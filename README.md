TimeBundle
==========

This project must be used to collect time of many people and perform some
statistic.

Time can be send with web ui or with rest api.

Someone create a project and invite others people to send time.
Those people received an email with URL to contribute to the project time.
This URL call in GET http method return project description, and with POST
http method is used to send time.

Each contributor has a unique URL.

Rest API
========

Account creation
----------------

curl -v --header "Content-Type: application/json"\
     --data '{"isPublic":true,"name":"codelutin","info":"Code Lutin SAS", "admin":\
           {"name":"admin", "email":"poussin@codelutin.com", "info":"root", "password":"toto"}}'\
     http://localhost:8080/api/v1/

Project creation
----------------

curl -v --header "Content-Type: application/json"\
      --data '{"isPublic":true,"name":"TimeBundle","info":"My Project",\
               "duration": 3600, "startDate":"2016-05-05T11:00:00+02:00",\
               "endDate": "2016-05-15T11:00:00+02:00"}'\
      http://admin:toto@localhost:8080/api/v1/codelutin/projects

Task creation
-------------

curl -v --header "Content-Type: application/json"\
     --data '{"name":"dev","info":"le dev",\
              "startDate":"2016-05-05T11:00:00+02:00", "endDate": "2016-05-15T11:00:00+02:00"}'\
     http://admin:toto@localhost:8080/api/v1/codelutin/projects/TimeBundle/tasks

Contributor creation
--------------------

curl -v --header "Content-Type: application/json"\
     --data '{"email":"bpoussin@free.fr","status":"ACTIVE"}'\
     http://admin:toto@localhost:8080/api/v1/cogitec/projects/ggene/contributors

Contribution creation
---------------------

Contribution for only one task
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

curl --header "Accept: application/json" --header "Content-Type: application/json"\
      --data '{"startDate":"2016-05-05T11:00:00+02:00","endDate":"2016-05-05T13:00:00+02:00",\
               "periods":[{"uid":"i01", "info":"1er test",\
                           "duration":"7200", "startDate":"2016-05-05T11:00:00+02:00", },\
                          {"uid":"i02", "info":"2eme test",\
                           "duration":"1800", "startDate":"2016-05-05T12:00:00+02:00"}]}'\
      http://localhost:8080/api/v1/codelutin/contribute/576fd55f-c0b5-4e0f-969c-36d5029635af/Dev

Contribution for multiple task
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

curl --header "Accept: application/json" --header "Content-Type: application/json"\
      --data '[{"url":"http://localhost:8080/api/v1/codelutin/contribute/576fd55f-c0b5-4e0f-969c-36d5029635af/Analyse"\
               "startDate":"2016-05-05T11:00:00+02:00","endDate":"2016-05-05T13:00:00+02:00",\
               "periods":[{"uid":"i01", "info":"1er anayse",\
                           "duration":"7200", "startDate":"2016-05-05T11:00:00+02:00", },\
                          {"uid":"i02", "info":"2eme analyse",\
                           "duration":"1800", "startDate":"2016-05-05T12:00:00+02:00"}]},\
               {"url":"http://localhost:8080/api/v1/codelutin/contribute/576fd55f-c0b5-4e0f-969c-36d5029635af/Dev"\
               "startDate":"2016-05-05T11:00:00+02:00","endDate":"2016-05-05T13:00:00+02:00",\
               "periods":[{"uid":"i03", "info":"1er test",\
                           "duration":"7200", "startDate":"2016-05-05T11:00:00+02:00", },\
                          {"uid":"i04", "info":"2eme test",\
                           "duration":"1800", "startDate":"2016-05-05T12:00:00+02:00"}]}]'\
      http://localhost:8080/api/v1/codelutin/contribute

Get information about how to contribute
---------------------------------------

curl -v http://localhost:8080/api/v1/codelutin/contribute/576fd55f-c0b5-4e0f-969c-36d5029635af

Get contibution
---------------

For project
~~~~~~~~~~~

url -v http://localhost:8080/api/v1/codelutin/TimeBundle/contributions

For one task
~~~~~~~~~~~~

url -v http://localhost:8080/api/v1/codelutin/TimeBundle/tasks/Dev/contributions

For one contributor
~~~~~~~~~~~~~~~~~~~

url -v http://localhost:8080/api/v1/codelutin/TimeBundle/contributors/poussin@codelutin.com/contributions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Project architecture
====================

~~~~~~~~~~~~~~~~~
orientDB oriented document Database
         /\
         ||
         ||
         \/
restesay / restJSON
         /\
         ||
         ||
         \/
JS / html / css api
~~~~~~~~~~~~~~~~~~~

Database modelisation
---------------------
![problem loading image](Diagrammedeclasses9.png "database schema")

Database use oriented document modelisation. All of data are document. We create first the database, one per account to improve security. In this database, we have some classes : 

1. **account** : (unique) this document is used for database creation.
2. **metadata** : (unique) this document stock some data about version, name and api.
3. **user** : There is one instance for each user plus some instances for administration (one for anonymous, one for alertEmail and one for each email to validate). There is an unique index to emails.
4. **project** : (extend class task). there is undefined number of project foreach database and the name is unique. There is an unique index with project name. 
5. **task** : For each project, there is one or more task. Name is not unique, the key is uuid. Each task having a link to direct "parentTask" who can be a task or a project and a link list with all parent tasks.
6. **Contributor** : For each project, there is contributors. A contributor can be created without any contribution and dont have password. he has a token name to be recognized. Actually, each contributor can contribute to only one project but he can contribute to an other project with same email and other instance of contributor. He has also a link to their project and can have a link to a user.
7. **Contribution view** : contribution are now separate in two classes to accept one contribtution to several task : 
    - **contribution** which store data about time, client id, informations and date.
    - **contributionstatus** which store status of contribtution and is linked to relative contributor, task and contribution. This schéma just exist in database, in application, we use contribution view.

Security
--------
The security of the application is done by database security system. Each user have some rights and the application can't override this rights. 


classes contribution, contributionStatus, contributor, task and project extend ORestrected class. This extention allow a besser rights management, we use CRUD system with 4 fields (_allow, _allowRead, _allowUpdate, _allowDelete).

<table>
    <tr>
        <td>user</td><td>all projects</td><td>specific project</td><td>contribution of specific project</td><td>contributor of specific project</td><td>public projects</td><td>contribution of open to contribution projects</td><td>alertEmail template(all projects)</td><td>alertemail</td>
    </tr>
    <tr>
        <td>super admin</td><td>CRUD</td><td>CRUD</td><td>CRUD</td><td>CRUD</td><td>CRUD</td><td>CRUD</td><td>CRUD</td><td>CRUD</td>
    </tr>
    <tr>
        <td>admin (for specific project)</td><td></td><td>RUD</td><td>CRUD</td><td>CRUD</td><td>R</td><td>C</td><td></td><td>CRUD</td>
    </tr>
    <tr>
        <td>reader (for specific project)</td><td></td><td>R</td><td>CR</td><td>R</td><td>R</td><td>CR</td><td></td><td></td>
    </tr>
    <tr>
        <td>contributor (for specific project)</td><td></td><td>R</td><td>CRUD</td><td>R</td><td></td><td>CRUD</td><td></td><td></td>
    </tr>
    <tr>
        <td>user</td><td></td><td></td><td></td><td>C (open projects)</td><td></td><td></td><td></td><td></td>
    </tr>
    <tr>
        <td>anonymous</td><td></td><td></td><td></td><td></td><td>R</td><td></td><td></td><td></td>
    </tr>
    
    
</table>



