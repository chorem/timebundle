package org.chorem.timebundle;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.time.ZonedDateTime;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ZonedDateTimeAdapter extends XmlAdapter<String, ZonedDateTime> {

    public ZonedDateTime unmarshal(String date) throws Exception {
        return ZonedDateTime.parse(date, ISO_OFFSET_DATE_TIME);
    }

    public String marshal(ZonedDateTime date) throws Exception {
        return date.format(ISO_OFFSET_DATE_TIME);
    }
}
