package org.chorem.timebundle;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.resteasy.util.Base64;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class AuthenticationParam {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(AuthenticationParam.class);

    protected String auth;
    protected String login;
    protected String password;

    public AuthenticationParam(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public AuthenticationParam(String auth) {
        try {
            this.auth = auth;
            if (StringUtils.startsWith(auth, "Basic ")) {
                String s = new String(Base64.decode(StringUtils.substringAfter(auth, "Basic ")));
                String[] lp = s.split(":");
                this.login = lp[0];
                this.password = lp[1];
            }
        } catch (Exception eee) {
            log.debug("Can't decode auth string: '" + auth + "'", eee);
            // do nothing
        }
    }

    public String getAuth() {
        return auth;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

}
