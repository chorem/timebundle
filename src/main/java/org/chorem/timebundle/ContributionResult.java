package org.chorem.timebundle;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.ObjectUtils;

/**
 * class to specify format of data from wid for rest api.
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Getter @Setter @ToString
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
public class ContributionResult {


    protected List<String> create = new ArrayList<String>();
    protected List<String> update = new ArrayList<String>();
    protected Map<String, String> error = new LinkedHashMap<String, String>();

    public void addCreate(String uid) {
        create.add(uid);
    }

    public void addUpdate(String uid) {
        update.add(uid);
    }

    public void addError(String uid, String msg) {
        error.put(ObjectUtils.defaultIfNull(uid, "null"), msg);
    }

    public int getCreateCount() {
        return create.size();
    }
    
    public int getUpdateCount() {
        return update.size();
    }

    public int getErrorCount() {
        return error.size();
    }
}
