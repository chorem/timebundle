package org.chorem.timebundle;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class TimeBundleConfig {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(TimeBundleConfig.class);

    protected static TimeBundleConfig instance;

    public static TimeBundleConfig get() {
        if (instance == null) {
             instance = new TimeBundleConfig();
        }
        return instance;
    }

    protected ApplicationConfig config;

    protected TimeBundleConfig() {
        config = new ApplicationConfig("timebundle.properties");
        try {
            config.parse();
        } catch (ArgumentsParserException eee) {
            throw new RuntimeException("Can't parse configuration", eee);
        }
    }

    public String getSmtpServer() {
        return this.config.getOption("tb.smtp.server");
    }

    public int getSmtpPort() {
        return this.config.getOptionAsInt("tb.smtp.port");
    }

    public String getSmtpUser() {
        return this.config.getOption("tb.smtp.user");
    }

    public String getSmtpPassword() {
        return this.config.getOption("tb.smtp.password");
    }

    public String getEmailSender() {
        return this.config.getOption("tb.email.sender");
    }

    public String getDatabaseUrl() {
        String result = this.config.getOption("tb.database.url");
        if (!StringUtils.endsWith(result, "/")) {
            result += "/";
        }
        return result;
    }
}
