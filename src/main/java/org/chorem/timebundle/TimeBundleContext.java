package org.chorem.timebundle;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.chorem.timebundle.entities.Account;
import org.chorem.timebundle.entities.Contributor;
import org.chorem.timebundle.entities.Project;
import org.chorem.timebundle.entities.Task;
import org.jboss.resteasy.annotations.Form;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

/**
 * class to manage globale informations about connection informations and url.
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Getter @Setter
public class TimeBundleContext {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(TimeBundleContext.class);

    @Context
    protected HttpServletRequest request;

    @DefaultValue("")
    @PathParam("account")
    protected String accountName;

    @DefaultValue("")
    @PathParam("contributorToken")
    protected String contributorToken;

    @DefaultValue("")
    @PathParam("project")
    protected String projectUuid;

    @DefaultValue("")
    @PathParam("task")
    protected String taskUuid;

    @Form
    protected Authentication auth;

    protected DBDocument db;

    public TimeBundleConfig getConfig() {
        return TimeBundleConfig.get();
    }

    public void init() {
        request.setAttribute("tbc", this);
    }

    public DBDocument getDB() {
        if(auth.getLogin().equals(DBDocument.ALERT_EMAIL) &&
                !request.getPathInfo().equals(String.format("/%s/alerts/all", accountName))) {
            String prefixUrl = getConfig().getDatabaseUrl();
            db = new DBDocument(prefixUrl, accountName, DBDocument.ROLE_ANONYMOUS, DBDocument.ROLE_ANONYMOUS);
            init();
        } // if alerter role try access to other function than checkAlert, we connect it like anonymous
        if (db == null) {    
            String prefixUrl = getConfig().getDatabaseUrl();
            db = new DBDocument(prefixUrl, accountName, auth.getLogin(), auth.getPassword());
            init();
        }
        return db;
    }

    public void closeDB(ODatabaseDocumentTx data) {
        getDB().shutdown(data);
    }

    public void setContributorToken(String contributorToken) {
        this.db = null;
        this.auth.setContributorToken(contributorToken);
        this.contributorToken = contributorToken;
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // D a t a b a s e   h e l p e r
    //
    ///////////////////////////////////////////////////////////////////////////
    /**
     * get all sub task with deep of 1
     * @param data
     * @param parentTaskUuid
     * @return 
     */
    public Collection<Task> getDirectSubTasks(ODatabaseDocumentTx data, String parentTaskUuid) {
        Collection<Task> result = new ArrayList<Task>();
         Iterable<ODocument> i = data.command(
                    new OCommandSQL("select *,"
                            + " parentTask.uuid as parentTaskUuid"
                            + " from Task"
                            + " where"
                            + " parentTask.uuid = ?"))
                    .execute(parentTaskUuid);
         
         for (ODocument doc : i ) {
             result.add(DBDocument.convertTo(doc, new Task()));
         }
        return result;
    }  
    
      ///////////////////////////////////////////////////////////////////////////
    //
    // E m a i l   h e l p e r
    //
    ///////////////////////////////////////////////////////////////////////////
    
    /**
     * configure email with paramether found in file timebundle.properties
     * @param subject 
     * @param message   
     * @param adressTo  
     * @param adressBcc
     * @return email in waiting to send
     */
    public SimpleEmail configEmail(
        String subject,
        String message,
        Set<String> adressTo,
        Set<String> adressBcc) {
        SimpleEmail email = new SimpleEmail();
        try {
            String smtpUser = getConfig().getSmtpUser();
            String smtpPassword = getConfig().getSmtpPassword();
            
            if (StringUtils.isNotBlank(smtpUser) && StringUtils.isNotBlank(smtpPassword)) {
                email.setAuthentication(smtpUser, smtpPassword);
            }
            email.setHostName(getConfig().getSmtpServer());
            email.setSmtpPort(getConfig().getSmtpPort());
            for(String to :adressTo) {
                email.addTo(to);
            }
            if(adressBcc != null) {
                for(String bcc :adressBcc) {
                    email.addBcc(bcc);
                }
            }
            email.setFrom(getConfig().getEmailSender());
            email.setSubject(subject);
            email.setMsg(message);
        } catch (EmailException ex) {
            log.error("email error", ex);
            email = null;
        }
        return email;
    }
    
    
    ///////////////////////////////////////////////////////////////////////////
    //
    // U R L   M A N A G E M E N T
    //
    ///////////////////////////////////////////////////////////////////////////

    public String getBaseUrl() {
        HttpRequest r = ResteasyProviderFactory.getContextData(HttpRequest.class);
        String result = r.getHttpHeaders().getHeaderString("X-tbHost");
        URI baseUri = r.getUri().getBaseUri();

        if(result != null) {
            // tbHost contains only protocole, host and port. We must add path (ex: /api/v1)
            result += baseUri.getPath();
        } else {
            result = baseUri.toString();
        }
        return result;
    }

    public String createUrl() {
        return getBaseUrl() + getAccountName();
    }

    public String createProjectUrl(String projectUuid) {
        return createUrl() + "/projects/" + projectUuid;
    }

    public String createTaskUrl(String taskUuid) {
        return createProjectUrl(getProjectUuid()) + "/tasks/" + taskUuid;
    }

    public String createContributortUrl(String contributorEmail) {
        return createProjectUrl(projectUuid) + "/contributors/" + contributorEmail;
    }

    public String createContributeUrl(String contributorName, String taskUuid) {
        
        return createUrl() + "/contribute/" + contributorName + "/" + taskUuid;
    }

    public String createContributeUrl(Contributor contributor, Task task) {
        return createContributeUrl(contributor.getName(), task == null ? "" : task.getUuid());
    }

    public String createContributionUrl(Contributor contributor) {
        return createProjectUrl(getProjectUuid()) + "/contribution/" + contributor.getName();
    }

    public String createUrl(Account account) {
        return getBaseUrl() + account.getName();
    }

    public String createUrl(Project project) {
        return createProjectUrl(project.getUuid());
    }

    public String createUrl(Task task) {
        return createTaskUrl(task.getUuid());
    }

    public String createUrl(Contributor contributor) {
        return createProjectUrl(getProjectUuid()) + "/contributors/" + contributor.getName();
    }
    
    public String createUrlValidation(String email, String userName, String login) {
        String url = getBaseUrl().replaceAll(request.getServletPath(),"")+"#"+getAccountName()
                +"/user/"+userName+"/login/"+login+"/email/"+email;
        log.info("url : "+url);
        return url;
    }

    /**
     * need to use in JSP, because EL doesn't support overloaded method :(
     * @param o
     * @return
     */
    public String createUrlFrom(Object o) {
        String result;
        if (o instanceof Account) {
            result = createUrl((Account)o);
        } else if (o instanceof Project) {
            result = createUrl((Project)o);
        } else if (o instanceof Task) {
            result = createUrl((Task)o);
        } else if (o instanceof Contributor) {
            result = createUrl((Contributor)o);
        } else {
            throw new RuntimeException("Can't create Url for " + o);
        }
        return result;
    }


}
