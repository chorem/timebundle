package org.chorem.timebundle;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.exception.OSecurityAccessException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Provider
public class OSecurityAccessExceptionMapper implements ExceptionMapper<OSecurityAccessException> {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(OSecurityAccessExceptionMapper.class);

    public @Context HttpHeaders headers;

    public Response toResponse(OSecurityAccessException eee) {
        log.info("Security error", eee);
        Response.ResponseBuilder result = Response.status(401);
        // use by JS to prevent browser login input (when JS want to use Form)
        if (!"true".equals(headers.getHeaderString("X-FetchRequest"))) {
            result.header("WWW-Authenticate", "Basic realm=\"TimeBundle\"");
        }
        return result.build();
    }

}
