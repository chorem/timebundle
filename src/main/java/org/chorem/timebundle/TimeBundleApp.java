package org.chorem.timebundle;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.chorem.timebundle.services.AccountService;
import org.chorem.timebundle.services.AlertService;
import org.chorem.timebundle.services.ContributionService;
import org.chorem.timebundle.services.ContributorService;
import org.chorem.timebundle.services.JSONService;
import org.chorem.timebundle.services.ProjectService;
import org.chorem.timebundle.services.TaskService;
import org.chorem.timebundle.services.UserService;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@ApplicationPath("/api/v1")
public class TimeBundleApp extends Application {

    HashSet<Object> singletons = new HashSet<Object>();
    
    public TimeBundleApp() {
        singletons.add(new TimeBundleFilter());
        singletons.add(new OSecurityAccessExceptionMapper());
        singletons.add(new TimeBundleNotFoundExceptionMapper());
        singletons.add(new AccountService());
        singletons.add(new ProjectService());
        singletons.add(new TaskService());
        singletons.add(new ContributorService());
        singletons.add(new ContributionService());
       // singletons.add(new InjectDataService());
        singletons.add(new JSONService());
        singletons.add(new UserService());
        singletons.add(new AlertService());
    }
    
    @Override
    public Set<Class<?>> getClasses() {
        HashSet<Class<?>> set = new HashSet<Class<?>>();
        return set;
    }
    
    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

}
