package org.chorem.timebundle.entities;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.record.impl.ODocument;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;
import org.chorem.timebundle.DBDocument;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Getter @Setter @ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Account extends TimeBundleEntity {
    
    //static constantes
    final static public String PUBLIC = "public";
    final static public String NAME = "name";
    final static public String INFO = "info";

    protected boolean isPublic;
    protected String name;
    protected String info;
    // admin only available during account creation send by client to server
    protected User admin;  


    @Override
    public Map getMap() {
        Map result = super.getMap();
        result.put(Account.PUBLIC, isPublic);
        result.put(Account.NAME, name);
        result.put(Account.INFO, StringUtils.defaultString(info));
        return result;
    }

    public String check(DBDocument db) {
        String result = null;
        if (StringUtils.isBlank(getName())) {
            result = "Bad account name";
        } else if (db.exists(getName())) {
            result = "This account already exist";
        } else if (!getName().matches("\\p{Alnum}*")) {
            result = "name not only contain alphanumeric charaters"; 
        }
        return result;
    }

    public void create(DBDocument db, User admin) {
        db.create(this, admin);
    }

    public void save(ODatabaseDocumentTx data) {
        ODocument result = null;
        for(ODocument account : data.browseClass("Account")){
            if(account.field("oid")==this.getOid()) {
                result = account;
                break;
            }
        }
        Map fields = getMap();
        fields.remove(Account.NAME); // name can't be change after creation
        result.fromMap(fields);
    }

    static public ODocument loadDocByName(ODatabaseDocumentTx data, String name) {
        ODocument result = DBDocument.loadOneBy(data, Account.class.getSimpleName(), Account.NAME, name);
        return result;
    }

    static public Account loadByName(ODatabaseDocumentTx data, String name, String userName) {
        Account result = DBDocument.loadOneBy(data, new Account(), NAME, name);
        User admin = DBDocument.convertTo(data.getUser().getDocument(), new User());//DBDocument.loadOneBy(data, new User(), NAME, userName);
        result.setAdmin(admin);
        return result;
    }

}
