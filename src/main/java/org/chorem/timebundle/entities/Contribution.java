package org.chorem.timebundle.entities;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.chorem.timebundle.ZonedDateTimeAdapter;

import static java.time.temporal.ChronoUnit.SECONDS;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;

/**
 * contribution interface that represent a contribution in database. This class 
 * contain all methods to modify and check integrity of contributions.
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Getter @Setter @ToString
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
@JsonIgnoreProperties(value = {"map"}, ignoreUnknown = true)
public class Contribution extends TimeBundleEntity {
/*
warning : contribution isn't stored like contribution object in base. there is a model with
 association class, contribution status which do link with task, contribution and contributor
 it has status, timezone, uuid and created date fields, contribution has just
 startdate, enddate, addtime, uid and info fields
  */  
    // static constantes
    final static public String ADD_TIME = "addTime";
    final static public String INFO = "info";
    final static public String VALIDATE = "status";
    final static public String START_DATE = "startDate";
    final static public String END_DATE = "endDate";
    final static public String C_UUID = "uuid";
    final static public String UID = "uid";
    final static public String NAME_CONTRIBUTOR = "contributorName";
    final static public String TASK_UUID = "taskUuid";
    final static public String TIMEZONE = "timezone";
    
    final static public String CONTRIBUTION = "contribution";
    final static public String CONTRIBUTOR = "contributor";
    final static public String CREATE_DATE = "createDate";
    final static public String TASK = "task";
    
    final static public String GET_CONTR_TO_DISPLAY = "select "
                + " createDate, status, uuid, timezone,"
                + " task.name as taskUuid," // to show name
                + " contributor.email as contributorName," // to show email
                + " contribution.startDate as startDate,"
                + " contribution.endDate as endDate,"
                + " contribution.addTime as addTime,"
                + " contribution.info as info,"
                + " contribution.uid as uid"
                + " from contributionStatus";

    static private Log log = LogFactory.getLog(Contribution.class);


    // unique id generated for each contribution
    protected String uuid;
    // user id send by client
    protected String uid;

    @XmlJavaTypeAdapter(value=ZonedDateTimeAdapter.class)
    protected ZonedDateTime startDate;

    @XmlJavaTypeAdapter(value=ZonedDateTimeAdapter.class)
    protected ZonedDateTime endDate;

    protected long addTime;
    protected String info;
    protected String status;


   // protected String projectUuid;
    
    protected String contributorName;
    protected String taskUuid;


    @Override
    public Map getMap() {
        Map result = super.getMap();
        result.putAll(MapContrib());
        result.putAll(MapStatus());
        return result;
    }
    
    public Map MapStatus() {
        Map result = super.getMap();
        result.put(C_UUID, uuid);
        result.put(TIMEZONE, startDate.getOffset().getTotalSeconds() / 3600);
        result.put(VALIDATE, status);
        return result;
    }
    
    public Map MapContrib() {
        Map result = super.getMap();
        result.put(UID, uid);
        result.put(START_DATE, Date.from(getStartDate().toInstant()));
        result.put(END_DATE, Date.from(getEndDate().toInstant()));
        result.put(ADD_TIME, addTime);
        result.put(INFO, StringUtils.defaultString(info));
        return result;
    }
    
    public String getId() {
        return uid;
    }

    public void setId(String id) {
        this.uid = id;
    }

    public void setDuration(long duration) {
        this.addTime = duration;
        if (startDate != null) {
            endDate = startDate.plusSeconds(duration);
        }
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
        if (startDate != null) {
            this.addTime = startDate.until(endDate, SECONDS);
        }
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
        if (endDate != null) {
            this.addTime = startDate.until(endDate, SECONDS);
        } else if (addTime > 0) {
            this.endDate = startDate.plusSeconds(addTime);
        }
    }

    @Override
    public void save(ODatabaseDocumentTx data) {
        ODocument contributionStatus = new ORecordId(getOid()).getRecord();
        setUuid(UUID.randomUUID().toString());
        contributionStatus.fromMap(MapStatus()).save();
        ODocument contribution = loadBaseContributionDoc(data, uid);
        contribution.fromMap(MapContrib()).save();
    }
    
    /**
     * check if contribution has already been sent
     * @param data
     * @param taskUuid
     * @param contributorName
     * @return 
     */
    public String allreadySent(ODatabaseDocumentTx data, String taskUuid, String contributorName) {
        Collection<ODocument> i = data.command(new OCommandSQL("select status"
                + " from ContributionStatus"
                + " where"
                + " contribution.uid = ? AND"
                + " task.uuid = ? AND"
                + " contributor.name = ?")).execute(uid, taskUuid, contributorName);
        for(ODocument doc: i) {
            return doc.field(VALIDATE);
        }
        return null;
    }
    
    @Override
    @JsonIgnore
    public void setFieldFrom(ODocument doc) {
        super.setFieldFrom(doc);
        Date sd = doc.field(Contribution.START_DATE);
        Date ed = doc.field(Contribution.END_DATE);
        int timezone = doc.field("timezone");

        setStartDate(ZonedDateTime.ofInstant(sd.toInstant(), ZoneOffset.ofHours(timezone)));
        if (ed != null) {
            setEndDate(ZonedDateTime.ofInstant(ed.toInstant(), ZoneOffset.ofHours(timezone)));
        }
    }

        /**
         *check some information about contribution to assure contribution integrity 
         * @param data database instance
         * @return string with error message, null if any
         */
    public String check(ODatabaseDocumentTx data) {
        StringBuilder sb = new StringBuilder();

        String sep = "";
        if (StringUtils.isBlank(uid)) {
            sb.append(sep).append("Bad uid '").append(uid).append("'");
            sep = "\n";
        }
        if (startDate == null) {
            sb.append(sep).append("Invalide start date '").append(startDate).append("'");
            sep = "\n";
        }

        if (endDate == null && addTime <= 0) {
            sb.append(sep).append("end date or duration must be set (endDate='")
                    .append(endDate).append("' duration='").append(addTime).append("'");
            sep = "\n";
        }
        if (startDate != null && endDate != null && startDate.isAfter(endDate)) {
            sb.append(sep).append("end date must be after start date (startDate='")
                    .append(startDate).append("' endDate='").append(endDate).append("'");
        }

        String result = null;
        if (sb.length() > 0) {
            result = sb.toString();
        }

        return result;
    }
    
    /**
     * create new contribution in database with current java contribution parameter.
     * @param data database instance
     * @param contributorName   name of the contributor
     * @param taskUuid  uuid of task
     */
    public void create(ODatabaseDocumentTx data, String contributorName, String taskUuid) {

      ODocument contributorDoc = Contributor.loadDocByName(data, contributorName);
        ODocument contribution = data.newInstance("contribution");
        contribution.fromMap(MapContrib());
        contribution.field("_allow", contributorDoc.getIdentity(), OType.LINK);
        contribution = contribution.save();
        Project project = Project.loadByUuid(data, contributorDoc.field("project.uuid").toString());
        DBDocument.addPermissions(data, project, contribution);
        addContributionLink(data, contribution, taskUuid, contributorName);
      
     }
    
    /**
     * add new contribution status. use this method to add new task or contributor
     * to an existing one.
     * @param data
     * @param contribution
     * @param taskUuid
     * @param contributorName
     */
    public void addContributionLink(ODatabaseDocumentTx data, ODocument contribution, String taskUuid, String contributorName) {
        ODocument taskDoc = Task.loadDocByUuid(data, taskUuid);
        ODocument contributorDoc = Contributor.loadDocByName(data, contributorName);
        
        ODocument contribStatus = data.newInstance("contributionStatus");
        setUuid(UUID.randomUUID().toString());
        contribStatus.fromMap(MapStatus());
        contribStatus.field(CREATE_DATE, new Date());
        contribStatus.field(TASK, taskDoc.getIdentity(), OType.LINK);
        contribStatus.field(CONTRIBUTION, contribution.getIdentity(), OType.LINK);
        contribStatus.field(CONTRIBUTOR, contributorDoc.getIdentity() , OType.LINK);
        contribStatus.save();
        Project project = Project.loadByUuid(data, contributorDoc.field("project.uuid").toString());
        DBDocument.addPermissions(data, project, contribStatus);
    }
    
    public static void remove(ODatabaseDocumentTx data, Map conditions) {
        
        Collection<ODocument> list = Contribution.loadDocWhere(data, conditions);
        for(ODocument doc : list) {
            doc.delete().save();
        }
        removeAllUnusued(data);
    }
    
    
    /**
     * remove contribution if any contributionstatus is linked to it
     * @param data 
     */
    static public void removeAllUnusued(ODatabaseDocumentTx data) {
        data.command(new OCommandSQL("delete"
                + " from contribution"
                + " where "
                + "not (uid in"
                + " (select distinct(contribution.uid)"
                + " from contributionStatus))")).execute();
    }
/*
    static public boolean removeByUid(ODatabaseDocumentTx data, String uid) {
        boolean result = false;
        for (ODocument doc : data.browseClass("Contribution")) {
            if(doc.field(Contribution.UID) == uid) {
                doc.delete();
                result = true;
                doc.save();
                break;
            }
        }
        return result;
    }
    */
    /**
     * @param data
     * @param uuid
     * @return ODocument contribution, null if any
     */
    static public ODocument loadDocByUuid(ODatabaseDocumentTx data, String uuid) {
        Map conditions = new HashMap();
        conditions.put("uuid", uuid);
        Collection<ODocument> result = loadDocWhere(data, conditions);
        for(ODocument contribution : result) {
            return contribution;
        }
        return null;
    }
    
    /**
     * load collection of contributions. conditions are linked with AND clause
     * @param data
     * @param conditions map with field as key and condition value as value
     * @return collection find
     */
    static public Collection<Contribution> loadWhere(ODatabaseDocumentTx data, Map conditions) {
        Collection<ODocument> resultDoc = loadDocWhere(data, conditions);
        Collection<Contribution> result = new ArrayList<Contribution>();
        for(ODocument doc : resultDoc) {
            result.add(DBDocument.convertTo(doc, new Contribution()));
        }
        return result;
    }
    
    /**
     * load collection of ODocuments. conditions are linked with AND clause
     * @param data
     * @param conditions map with field as key and condition value as value
     * @return collection find
     */
    static private Collection<ODocument> loadDocWhere(ODatabaseDocumentTx data, Map conditions) {
        Collection<ODocument> result;
        String where = getStringConditions(conditions);
        result = data.command(new OCommandSQL("select"
                + " @rid.asString(), createDate, status, uuid, timezone,"
                + " task.name as taskUuid,"
                + " contributor.email as contributorName," 
                + " contribution.startDate as startDate,"
                + " contribution.endDate as endDate,"
                + " contribution.addTime as addTime,"
                + " contribution.info as info,"
                + " contribution.uid as uid"
                + " from contributionStatus"
                + where
                + "unwind taskUuid, contributorName")).execute();
        return result;    
    } 
    
    
    
    
    /**
     * 
     * @param data
     * @param uuid
     * @return Contribution, null if any
     */
    static public Contribution loadByUuid(ODatabaseDocumentTx data, String uuid) {
        Contribution result = DBDocument.convertTo(loadDocByUuid(data, uuid), new Contribution());
        return result;
    }

    /**
     * get list of contributions for one uid (client id)
     * this contributions have task.name as taskUuid, contributor.email as contributorName
     * @param data
     * @param uid
     * @return 
     */
    static public Collection<ODocument> loadDocsByUid(ODatabaseDocumentTx data, String uid) {
        Map conditions = new HashMap();
        conditions.put("contribution.uid", uid);
        Collection<ODocument> result = loadDocWhere(data, conditions);
        
            return result;
    }

    static public ODocument loadContributionDoc(ODatabaseDocumentTx data, String uid, String taskUuid, String contributorName) {
       Map conditions = new HashMap();
        conditions.put("contribution.uid", uid);
        conditions.put("task.uuid", taskUuid);
        conditions.put("contributor.name", contributorName);
        Collection<ODocument> result = loadDocWhere(data, conditions);
        
        for(ODocument doc : result) {
            return doc;
        }
        return null;
    }
    
    
    static public ODocument loadBaseContributionDoc(ODatabaseDocumentTx data, String uid) {
        Iterable<ODocument> i = data.command(new OCommandSQL("select *"
                + " from contribution where uid = ?")).execute(uid);
        for (ODocument doc : i) {
            return doc;
        }
        return null;
    }
}


