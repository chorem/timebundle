package org.chorem.timebundle.entities;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.metadata.security.OUser;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;

/**
 * Contributor interface that represent a contributor in database. This class 
 * contain all methods to modify and check integrity of contributors.
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Getter @Setter @ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Contributor extends TimeBundleEntity {
    
    //static constantes
    final static public String PROJECT_UUID = "projectUuid";
    final static public String USER_NAME = "userName";
    final static public String CONTRIBUTE_URL = "contributeUrl";
    final static public String NAME = "name";
    final static public String EMAIL = "email";
    final static public String INFO = "info";
    final static public String STATUS = "status";
    final static public String CONTRIBUTION_VALIDATION = "contributionValidation";
    final static public String CREATE_DATE = "createDate";
    
    

        /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(Contributor.class);

    protected String projectUuid;
    protected String userName;
    
    protected String contributeUrl;

    protected String name;
    protected String email;
    protected String info;
    protected String status = "SUSPENDED";
    protected boolean contributionValidation;
    protected Date createDate;

    @Override
    public Map getMap() {
        Map result = super.getMap();
        result.put(Contributor.NAME, name);
        result.put(Contributor.EMAIL, email);
        result.put(Contributor.INFO, StringUtils.defaultString(info));
        result.put(Contributor.STATUS, status);
        result.put("password", name); // password is token UUID same as name
        result.put(Contributor.CONTRIBUTION_VALIDATION, contributionValidation);
        return result;
    }

    public String getPassword() {
        return name;
    }

    /**
     * check some informations about contributor
     * @param data
     * @return string with error, null if any
     */
    public String check(ODatabaseDocumentTx data) {
        String result = null;
        String emailContributor = getEmail();
        if (emailContributor.isEmpty()) {
            result = "Bad contributor email";
        }
        return result;
    }

    /**
     * create contributor instance in database with parameter of current java contributor
     * @param data  current database
     * @param projectUuid uuid for project
     */
    public void create(ODatabaseDocumentTx data, String projectUuid) {
        Project project = Project.loadByUuid(data, projectUuid);
        create(data, project);
    }

    public void create(ODatabaseDocumentTx data, Project project) {
        setName(UUID.randomUUID().toString());
        ODocument contributorDoc = data.newInstance("Contributor").fromMap(getMap());
        ODocument userDoc = User.loadDocByEmail(data, email);
        if(userDoc != null ) {
            contributorDoc.field("user", userDoc.getIdentity(), OType.LINK);
        }
        contributorDoc.field(Contributor.CREATE_DATE, new Date());
        log.info("project : "+ project);
        contributorDoc.field("project", project.getOid(), OType.LINK);
        contributorDoc.field(Contributor.STATUS, "ACTIVE");
        contributorDoc.save();
       // contributorDoc.reload();
        //contributorDoc.field("_allow", contributorDoc.getIdentity());
       // contributorDoc.save();
        OUser userContrib = new OUser(contributorDoc).addRole("contributor");
        userContrib.addRole(project.getReaderRoleName(data));
        DBDocument.addPermissions(data, project, contributorDoc);
        userContrib.getDocument().save();

    }

    /**
     * remove contributor only if he has no contribution
     * @param data database instance
     */
    public void remove(ODatabaseDocumentTx data) {
        ODocument doc = Contributor.loadDocByName(data, name);
        doc.delete().save();
    }



    static public ODocument loadDocByName(ODatabaseDocumentTx data, String name) {
        Iterable<ODocument> i = data.command(new OCommandSQL("select *,"
                + " project.uuid as projectUuid,"
                + " user.name as userName"
                + " from contributor"
                + " where name in ?")).execute(name);
        for (ODocument doc : i) {
            return doc;
        }
        return null;
    }

    static public Contributor loadByName(ODatabaseDocumentTx data, String name) {
        Contributor result = DBDocument.convertTo(loadDocByName(data,name), new Contributor());
        return result;
    }

    static public ODocument loadDocByEmail(ODatabaseDocumentTx data, String email, String projectUuid) {
        Iterable<ODocument> i = data.command(new OCommandSQL("select *,"
                + " project.uuid as projectUuid,"
                + " user.name as userName"
                + " from contributor where email in ? AND project.uuid in ?")).execute(email, projectUuid);
        for (ODocument doc : i ) {
            return doc ;
        }
        return null;
    }

    static public Contributor loadByEmail(ODatabaseDocumentTx data, String email, String projectUuid) {
        ODocument doc = loadDocByEmail(data, email, projectUuid);
        Contributor result = DBDocument.convertTo(doc, new Contributor());
        return result;
    }

}
