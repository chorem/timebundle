package org.chorem.timebundle.entities;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.metadata.security.ORestrictedOperation;
import com.orientechnologies.orient.core.metadata.security.OSecurity;
import com.orientechnologies.orient.core.metadata.security.OUser;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;

/**
 * project interface that represent a project in database. This class 
 * contain specific methods to modify and check integrity of projects ; others
 * methods are in task class
 * @author poussin
 * @version $Revision$
 *'
 * Last update: $Date$
 * by : $Author$
 */
@Getter @Setter @ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Project extends Task {

    //static constantes
    final static public String IS_PUBLIC = "isPublic";
    final static public String OPEN_TO_CONTRIBUTION = "openToContribution";
    final static public String LOGO = "logo";
    
    static private Log log = LogFactory.getLog(Project.class);
    
    public boolean isPublic;
    protected boolean openToContribution;

    // TODO
    protected byte[] logo = new byte[0];

    @Override
    public Map getMap() {
        Map result = super.getMap();
        result.put(Project.IS_PUBLIC, isPublic);
        result.put(Project.LOGO, logo);
        result.put(Project.OPEN_TO_CONTRIBUTION, openToContribution);
        return result;
    }


    
    @Override
    public String check(ODatabaseDocumentTx data) {
        String result = null;

        if (StringUtils.isBlank(getName())) {
            result = "Bad project name";
        } 

        return result;
    }

    /**
     * create a new project in database with parameter of current project
     * @param data database instance
     */
    public void create(ODatabaseDocumentTx data) {
        setUuid(UUID.randomUUID().toString());
        data.begin();
        try{
            ODocument projectDoc = data.newInstance("project").fromMap(getMap());
            projectDoc.field(Project.START_DATE, new Date());
            if (isPublic) {
                data.getMetadata().getSecurity().allowRole(projectDoc, ORestrictedOperation.ALLOW_READ, "anonymous");
            }
            data.save(projectDoc);
            
            data.save(data.newInstance("ORole").field("name", getReaderRoleName(data)).field("mode", 0));
            data.save(data.newInstance("ORole").field("name", getAdminRoleName(data)).field("mode", 0));
            //change effectued with .save method
            
            OUser user =new OUser(data.getUser().getDocument());
            String adminRole = getAdminRoleName(data);
            user.addRole(adminRole).save();
            OSecurity security = data.getMetadata().getSecurity();
            OUser alerter = security.getUser(DBDocument.ALERT_EMAIL);
            alerter.addRole(security.getRole(getReaderRoleName(data))).save();
            AlertEmail.addAlertsTask(data, uuid);
            
            DBDocument.addPermissions(data, this, projectDoc);
            projectDoc.save();
            data.commit();
            setOid(projectDoc.getIdentity().toString());
        } catch (Exception eee) {
            log.error("project not created", eee);
            data.rollback();
        }
    }
    

    static public ODocument loadDocByUuid(ODatabaseDocumentTx data, String uuid) {
        ODocument result = DBDocument.loadOneBy(data, Project.class.getSimpleName(), "uuid", uuid);
        return result;
    }

    static public Project loadByUuid(ODatabaseDocumentTx data, String uuid) {
        Project result = DBDocument.loadOneBy(data, new Project(), "uuid", uuid);
        return result;
    }

    static public Project loadByContributor(ODatabaseDocumentTx data, String contributorName) {
        Project result = DBDocument.convertTo(loadDocByContributor(data, contributorName), new Project());
        return result;
    }
    
    static public ODocument loadDocByContributor(ODatabaseDocumentTx data, String contributorName) {
        Iterable<ODocument> i = data.command(
                new OCommandSQL("select expand(project) from Contributor where name=?"))
                .execute(contributorName);
        for (ODocument doc : i) {
            return doc;
        }
        return null;
    }
    /**
     * remove a project and all their task, contributor and contributions 
     * @param data 
     */
    @Override
    public void remove(ODatabaseDocumentTx data) {
        super.remove(data);
        data.command(new OCommandSQL("delete"
                + " from Contributor"
                + " where project.uuid = ?")).execute(this.getUuid());
        Contribution.removeAllUnusued(data);
        Project.loadDocByUuid(data, uuid).delete().save();
        //warning commandsql must have only one space between all words
    }
    
    /**
     * this method save project and modify rights for all contributors, contributions
     * and task of the project if it's public or not
     * @param data 
     */
    @Override
    public void save(ODatabaseDocumentTx data) {
        Project proj = Project.loadByUuid(data, uuid);

        ODocument result = new ORecordId(oid).getRecord();
        result.fromMap(getMap()).save();

        if ( proj.isPublic() != isPublic()) {
            // if value isPublic change
            if (isPublic) {
                data.command(new OCommandSQL(String.format("update "
                        + " (select expand(referredBy)"
                        + " from (find references"
                        + " (select expand(referredBy)"
                        + " from (find references %s))))"
                        + " add _allowRead = "
                        + "(select from orole where name = 'anonymous')", oid))).execute();
                // allow anonymous to read all tasks and contributions of project
                data.command(new OCommandSQL(String.format("update "
                        + " (select expand(referredBy)"
                        + " from (find references %s [contributor]))"
                        + " add _allowRead = "
                        + " (select from orole where name = 'anonymous')", oid))).execute();
                // allow anonymous to read all contributors of projet
                data.command(new OCommandSQL(String.format("update %s "
                        + " add _allowRead = "
                        + " (select from orole where name = 'anonymous')", oid))).execute();
                // allow anonymous to read this project
            } else {
                data.command(new OCommandSQL(String.format("update "
                        + " (select expand(referredBy)"
                        + " from (find references"
                        + " (select expand(referredBy)"
                        + " from (find references %s))))"
                        + " remove _allowRead = "
                        + "(select from orole where name = 'anonymous')", oid))).execute();
                // disallow anonymous to read all tasks and contributions of project
                data.command(new OCommandSQL(String.format("update "
                        + " (select expand(referredBy)"
                        + " from (find references %s [contributor]))"
                        + " remove _allowRead = "
                        + "(select from orole where name = 'anonymous')", oid))).execute();
                // disallow anonymous to read all contributors of project
                data.command(new OCommandSQL(String.format("update %s "
                        + " remove _allowRead = "
                        + " (select from orole where name = 'anonymous')", oid))).execute();
                // disallow anonymous to read this project
            }
        }
    }

}


