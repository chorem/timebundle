package org.chorem.timebundle.entities;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.record.impl.ODocument;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;

/**
 * abstract class to implement all entities. This class can not be instancied.
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Getter @Setter @ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public abstract class TimeBundleEntity {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(TimeBundleEntity.class);

    protected String oid;

    /**
     * create a map with field and value of T extends TimeBundleEntity
     * @return the map with some field and value 
     */
    public Map getMap() {
        Map result = new HashMap();
        return result;
    }
    
    /**
     * fill all field of this with ODocument field
     * @param doc ODocument with data to set in this object
     */
    public void setFieldFrom(ODocument doc) {
        if (doc == null) {
            return;
        }
        
        for (String p : doc.fieldNames()) {
          //  try {
             /*   if(doc.fieldType(p)==OType.EMBEDDEDSET || doc.fieldType(p)==OType.EMBEDDEDLIST) {
                    log.info("E field : "+ p + " type : "+ doc.fieldType(p) + " embedded : ");
                    log.info(doc.fieldType(p).isEmbedded());
                    Set field = new HashSet();
                    for(String f :(Iterable<String>) doc.field(p)) {
                        field.add(f);
                    }
                    DBDocument.setField(this, p, field);
                } else {
                    log.info("field : "+ p + " type : "+ doc.fieldType(p) + " embedded : ");
                    log.info(doc.fieldType(p).isEmbedded());
                   */ 
                    DBDocument.setField(this, p, doc.field(p));
               // }
      //      } catch (NullPointerException eee) {
                //field is null so nothing to set into field
        //    }
        }
    }

    public boolean exists() {
        return StringUtils.isNotBlank(oid);
    }

    /**
     * Return True only if other entity exists in database with same value for field
     * @param data
     * @param field
     * @return
     */
    public boolean exists(ODatabaseDocumentTx data, String field) {
        if (data == null) {
            return false;
        }
        try {
            String value = BeanUtils.getProperty(this, field);
            ODocument doc = DBDocument.loadOneBy(data, this.getClass().getSimpleName(), field, value);
            return doc != null ;
        } catch (Exception eee) {
            log.error("Can't test existance", eee);
            throw new RuntimeException(eee);
        }
    }
    
    /**
     * save current TimeBundleEntity to equivalent ODocument in database
     * @param data database instance
     */
    public void save(ODatabaseDocumentTx data) {
        ODocument result = new ORecordId(getOid()).getRecord();
        result.fromMap(getMap()).save();
    }
    
    
    /**
     * create string to where condition with map from string object. 
     * @param conditions key as field, value as value condition. value must be 
     * string
     * @return 
     */
    protected static String getStringConditions(Map<String, Object> conditions) {
        String where = new String();
        if(!conditions.isEmpty()) {
            where = " where";
        }
        
        String sep = " ";
        for(String field :conditions.keySet()) {
            where += sep;
            where += field;
           // if(conditions.get(field).getClass()==String.class) {
                where += " = '"+ conditions.get(field)+ "'";
           /* } else {
                conditions.get("")
                where += " in ";
                for()
            }*/
          //  where += conditions.get(field).getClass()==String.class?"'":"";
            sep = " AND ";
        }
        return where;
    }

}
