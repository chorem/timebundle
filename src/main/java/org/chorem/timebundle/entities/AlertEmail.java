/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.chorem.timebundle.entities;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.metadata.security.ORestrictedOperation;
import com.orientechnologies.orient.core.metadata.security.OUser;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.chorem.timebundle.DBDocument;
import org.chorem.timebundle.TimeBundleContext;

/**
 * AlertsEmail interface that represent an alertsEmail in database. This class 
 * contain all methods to modify and check integrity of alertEmails.
 * @author tollive
 */
@Getter @Setter @ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class AlertEmail extends TimeBundleEntity {
    
    //static constantes
    final static public String FROM = "from";
    final static public String RULE_TO = "ruleTo";
    final static public String TO = "to";
    final static public String RULE_BCC = "ruleBcc";
    final static public String BCC = "bcc";
    final static public String SUBJECT = "subject";
    final static public String MESSAGE = "message";
    final static public String SENT = "sent";
    final static public String CONDITION = "condition";
    final static public String REPLY_TO = "replyTo";
    final static public String A_UUID = "uuid";
    final static public String ALL = "all";
    final static public String TASK_UUID = "taskUuid";
    
    
    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(AlertEmail.class);
    
    protected String        from;
    protected String        ruleTo;
    protected Set<String>   to;
    protected String        ruleBcc;
    protected Set<String>   bcc;
    protected String        subject;
    protected String        message;
    protected boolean       sent;
    protected long           condition;
    // rule to send this message
    protected String        replyTo;
    protected String        uuid;
    protected boolean       all;
    //if this alert is use like template for all new project (all => true, taskuuid => null)
    //, all new task of this project (all => true, taskUuid => task) or not (all => false, taskUuid => task)
    
    protected String        taskUuid;
    
    @Override
    public Map getMap() {
        Map result = super.getMap();
        result.put(FROM, from);
        result.put(TO, to);
        result.put(RULE_TO, ruleTo); 
        result.put(BCC, bcc);
        result.put(RULE_BCC, ruleBcc);
        result.put(SUBJECT, subject);
        result.put(MESSAGE, message);
        result.put(SENT, sent);
        result.put(CONDITION, condition);
        result.put(REPLY_TO, replyTo);
        result.put(A_UUID, uuid);
        result.put(ALL, all);
        return result;
    }

    /**
     * create alert and add permission for admin to edit them
     * if all is true, check taskuuid, if exist, it will be a template for all
     * subtasks of this task, else, it will be a template for all projects
     * 
     * @param data
     * @param taskUuid 
     * @return  
     */
    public boolean create(ODatabaseDocumentTx data, String taskUuid) {
        setUuid(UUID.randomUUID().toString());
        ODocument taskDoc = Task.loadDocByUuid(data, taskUuid);
        Task task = DBDocument.convertTo(taskDoc, new Task());
        ODocument alertDoc = data.newInstance("AlertEmail").fromMap(getMap());
        OUser alerter = data.getMetadata().getSecurity().getUser(DBDocument.ALERT_EMAIL);
        HashSet<OIdentifiable> alertSet = new HashSet<OIdentifiable>();
        alertSet.add(alerter.getDocument());
        alertDoc.field("_allowUpdate", alertSet, OType.LINKSET);
//        alertDoc.field("_allowRead", alertSet, OType.LINKSET);
        if(all==true && taskDoc==null){
            if(!data.getUser().hasRole("admin", true)) {
                return false;
            }// only super admin can manage template alert
        } else {
            alertDoc.field("task", taskDoc.getIdentity(), OType.LINK);
            data.getMetadata().getSecurity().allowRole(
                    alertDoc, ORestrictedOperation.ALLOW_ALL,task.getAdminRoleName(data));
            //allow admin project to modify alert
        }
        
        alertDoc.save();
        return true;
    }
    
    /**
     * remove specific alert
     * @param data 
     */
    public void remove(ODatabaseDocumentTx data) {
        ODocument alert = loadDocByUuid(data, uuid);
        alert.delete();
    }
    
    /**
     * add alert from template to task with taskuuid
     * @param data
     * @param taskUuid 
     */
    static public void addAlertsTask(ODatabaseDocumentTx data, String taskUuid) {
        ODocument entity = Task.loadDocByUuid(data, taskUuid);
        String className = entity.getClassName();
        OCommandSQL command;
        if ("Project".equals(className)) {
            command = new OCommandSQL("select"
                    + " from AlertEmail where all = 'true' AND task is null");
        } else {
            command = new OCommandSQL(
                    String.format("select"
                    + " from AlertEmail where"
                    + " all = true AND"
                    + " task.uuid = '%s'", Task.getProjectUuid(data, taskUuid)));
        }
        
        Iterable<ODocument> i =  data.command(command).execute();
        for(ODocument alert : i ) {
            Map m = alert.toMap();
            m.remove("@rid"); //to save alert as a new instance
            ODocument newAlert = new ODocument(m);
            
            newAlert.field("task", entity.getIdentity(), OType.LINK);
            newAlert.field(ALL, false);
            newAlert.field(A_UUID, UUID.randomUUID().toString());
            newAlert.save();
        }
    }
    
    /**
     * set adresse to and BCC with ruleTo and ruleBCC
     * @param data
     * @param alertDoc 
     */
    public void prepareAlert(ODatabaseDocumentTx data, ODocument alertDoc) {
        setTo(addAdresses(data, getTo(), getRuleTo(), alertDoc));
        setBcc(addAdresses(data, getBcc(), getRuleBcc(), alertDoc));
        setFrom(String.format("%s@%s.com",alertDoc.field("task.name"), data.getName()));
    }
    
    
    /**
     * add adress to 'adresses' in accort with ruleAdresse (admin or contributor)
     * @param data
     * @param adresses
     * @param ruleAdress
     * @param alertDoc
     * @return 
     */
    public Set addAdresses(
        ODatabaseDocumentTx data,
        Set adresses,
        String ruleAdress, 
        ODocument alertDoc) {
        Set result = new HashSet();
        if(adresses!=null) {
            result.addAll(adresses);
        }
        String projectUuid = Task.getProjectUuid(data, taskUuid);
            if("admin".equals(ruleAdress)){
                Collection<ODocument> AdminRole = alertDoc.field("_allow", OType.LINKSET);
                result.addAll(addAdminAdresses(data, AdminRole.iterator().next().getIdentity()));
            } else if ("contributor".equals(ruleAdress)) {
                Collection<ODocument> AdminRole = alertDoc.field("_allow", OType.LINKSET);
                result.addAll(addAdminAdresses(data, AdminRole.iterator().next().getIdentity()));
                result.addAll(addContributorsAdresses(data, projectUuid));
            }
            return result;
    } 
    
    /**
     * get admin adresses to specific project. To choose adress, we take first 
     * adresse added, the last in the set in db
     * @param data
     * @param AdminRoleId
     * @return 
     */
    public Collection<String> addAdminAdresses(
            ODatabaseDocumentTx data,
            ORID AdminRoleId) {
        Set result = new HashSet();
        Iterable<ODocument> i = data.command(new OCommandSQL(String.format("select"
                        + " last(emails) as emails"
                        + " from User"
                        + " where %s in roles"
                        + " OR 'admin' in roles", AdminRoleId))).execute();
        for(ODocument adresseDoc : i ) {
            result.add(adresseDoc.field("emails"));
        }
        return result;
    }
    
    /**
     * get contributors mail adresses for specific project 
     * @param data
     * @param projectUuid
     * @return 
     */
    public Collection<String> addContributorsAdresses(
        ODatabaseDocumentTx data,
        String projectUuid) {
        Set result = new HashSet();
        Iterable<ODocument> i = data.command(new OCommandSQL(
                "select from contributor where project.uuid = ?"))
                .execute(projectUuid);
        
        for(ODocument doc: i) {
            result.add(doc.field("email"));
        }
        return result;
    } 
    
    
    static public ODocument loadDocByUuid(ODatabaseDocumentTx data, String alertUuid) {
        ODocument result = null;
        Iterable<ODocument> i = data.command(
                new OCommandSQL(" select *,"
                        + " task.uuid as taskUuid"
                        + " from AlertEmail"
                        + " where uuid = ?")).execute(alertUuid);
        
        for (ODocument doc : i) {
            result = doc ;
        }
        return result;
    }
    
    static public AlertEmail loadByUuid(ODatabaseDocumentTx data, String alertUuid) {
        AlertEmail result;
        result = DBDocument.convertTo(loadDocByUuid(data, alertUuid), new AlertEmail());
        return result;
    }
    
    @Override
    public void save(ODatabaseDocumentTx data) {
        super.save(data);
        ODocument task = Task.loadDocByUuid(data, taskUuid);
        if(task!=null) {
            ODocument result = new ORecordId(oid).getRecord();
            result.field("task", task.getIdentity(), OType.LINK).save();
        }
    }
    

    public String send(TimeBundleContext tbc) {
        try {
            SimpleEmail email = tbc.configEmail(SUBJECT, MESSAGE, to, bcc);
             email.send();
             return AlertEmail.SENT;
        } catch (EmailException eee) {
            log.error("error during sending email", eee);
            return eee.toString();
        }
    }

    static public Collection<ODocument> getAlertsDocWhere(
            String where,
            boolean isTemplate,
            ODatabaseDocumentTx data) {
        Collection<ODocument> result = data.command(new OCommandSQL(
                    "select *,"
                    + " task.name as taskUuid"
                    + " from alertemail"
                    + " where"
                    + where
                    + " AND all = ?")).execute(isTemplate);
        return result;
    }
    
    static public Collection<AlertEmail> getProjectAlert(
            ORID projectOid,
            String taskUuid,
            boolean isTemplate, 
            ODatabaseDocumentTx data) {
                String where = String.format("(task.uuid in"
                    + " (select expand(referredBy.uuid)"
                    + " from (find references %s [task]))"
                    + " OR task.uuid = '%s')", projectOid, taskUuid);
           Collection<ODocument> i =  getAlertsDocWhere(where, isTemplate, data);
           Collection<AlertEmail> result = new ArrayList<AlertEmail>();
           for (ODocument doc :   i) {
                AlertEmail alert = DBDocument.convertTo(doc, new AlertEmail());
                result.add(alert);
            }
           return result;
    }
    
    static public Collection<AlertEmail> getDatabaseAlert(
          boolean isTemplate, 
            ODatabaseDocumentTx data) {
        Collection<ODocument> i = getAlertsDocWhere(" task is null", isTemplate, data);
        Collection<AlertEmail> result = new ArrayList<AlertEmail>();
           for (ODocument doc :   i) {
                AlertEmail alert = DBDocument.convertTo(doc, new AlertEmail());
                result.add(alert);
            }
           return result;
    }  
    
}
