package org.chorem.timebundle.entities;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.ws.rs.PathParam;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;
import org.chorem.timebundle.DateAdapter;

/**
 * task interface that represent a task in database. This class 
 * contain all methods to modify and check integrity of tasks. This class mustn't
 * be connected to client directly, we must use servce class.
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Getter @Setter @ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Task extends TimeBundleEntity {
    
    // static constantes
    final static public String T_UUID = "uuid";
    final static public String NAME = "name";
    final static public String INFO = "info";
    final static public String DURATION = "duration";
    final static public String START_DATE = "startDate";
    final static public String END_DATE = "endDate";
    final static public String CONTRIBUTION_VALIDATION = "contributionValidation";
    final static public String PARENT_TASK_UUID = "parentTaskUuid";
    final static public String PATH_TO_PROJECT_UUID = "pathToProjectUuid";
    
    
    

    static private Log log = LogFactory.getLog(Task.class);

    @PathParam("project")
    protected String parentTaskUuid;
    protected Set<String> pathToProjectUuid;

    // use to have unique id to create Role name (oid change after first save) for project
    // use as uid for task if name change
    protected String uuid;
    protected String name;
    protected String info;
    protected long duration;
    @XmlJavaTypeAdapter(DateAdapter.class)
    protected Date startDate;
    @XmlJavaTypeAdapter(DateAdapter.class)
    protected Date endDate;
    protected boolean contributionValidation;


    /**
     * return uuid of project in top of the stack of this task.
     * @param data  database instance
     * @param taskUuid 
     * @return 
     */
    static public String getProjectUuid(ODatabaseDocumentTx data, String taskUuid){
     
        Iterable<ODocument> i = data.command(new OCommandSQL("select"
                + " ifnull(first(pathToProject.uuid), uuid)"
                + " from task where uuid = ?")).execute(taskUuid);
        for(ODocument doc : i) {
            return doc.field("ifnull");
        }
        return null;
        
    }
    
    
    
    
    @Override
    public Map getMap() {
        Map result = super.getMap();
        result.put(Task.T_UUID, uuid);
        result.put(Task.NAME, name);
        result.put(Task.INFO, StringUtils.defaultString(info));
        if (duration > 0) {
            result.put(Task.DURATION, duration);
        }
        if (startDate != null) {
            result.put(Task.START_DATE, startDate);
        }
        if (endDate != null) {
            result.put(Task.END_DATE, endDate);
        }
        result.put(Task.CONTRIBUTION_VALIDATION, contributionValidation);
        return result;
        
    }
    
    /**
     * check some conditions du project intégrity
     * @param data database instance
     * @return string with error or null if any
     */
    public String check(ODatabaseDocumentTx data) {
        String result = null;

        if (StringUtils.isBlank(getName())) {
            result = "Bad project name";
        }

        return result;
    }
    
    /**
     * get name of reader role for this project
     * @param data database instance
     * @return 
     */
    public String getReaderRoleName(ODatabaseDocumentTx data) {
        
        String result = "projectReader-" + getProjectUuid(data, uuid);
        return result;
    }
    
    /**
     * get name of admin role for this project
     * @param data database instance
     * @return 
     */
    public String getAdminRoleName(ODatabaseDocumentTx data) {
        String result = "projectAdmin-" + getProjectUuid(data, uuid);
        return result;
    }
    
    /**
     * check task existence
     * @param data database instance
     * @return 
     */
    public boolean exists(ODatabaseDocumentTx data) {
        if (data == null) {
            return false;
        }
        ODocument doc = Task.loadDocByUuid(data, getUuid());
        return doc != null && !StringUtils.equals(oid, doc.field(Task.T_UUID).toString());
    }
    
    /**
     * create task instance in database with parameter of current task
     * @param data database instance
     * @param parentUuid uuid of parent task
     */
    public void create(ODatabaseDocumentTx data, String parentUuid) {
        setUuid(UUID.randomUUID().toString());       
        data.begin();
        ODocument taskDoc = data.newInstance("Task").fromMap(getMap());
        ODocument parentDoc = loadDocByUuid(data, parentUuid);        
        taskDoc.field("createDate", new Date());
        taskDoc.field("parentTask", parentDoc.getIdentity(), OType.LINK);
        taskDoc.save();
        data.commit();
        data.begin();
        taskDoc.reload();
        List<OIdentifiable> pathToProject = new ArrayList();  
        List<OIdentifiable> parentPath;
        if (parentDoc.containsField("pathToProject")) {
            parentPath = parentDoc.field("pathToProject") ;
        } else {
            parentPath = new ArrayList<OIdentifiable>();
        }
        if(parentPath.isEmpty()) {
            pathToProject.add(parentDoc.getIdentity());
        } else {
            pathToProject.addAll((Collection<ODocument>) parentDoc.field("pathToProject"));
        }
        pathToProject.add(taskDoc);
        taskDoc.field("pathToProject", pathToProject, OType.LINKSET);
        Project proj = Project.loadByUuid(data, getProjectUuid(data, uuid));
        DBDocument.addPermissions(data, proj, taskDoc);
        taskDoc.save();
        AlertEmail.addAlertsTask(data, uuid);
        data.commit();
    }
    
    
    
    /**
     * remove in cascade task, all subtasks and contributions
     * @param data database instance
     */
    public void remove(ODatabaseDocumentTx data){
        ODocument taskDoc = Task.loadDocByUuid(data, this.getUuid());
        data.command(new OCommandSQL(String.format("delete from"
                + " (select expand(distinct(@rid)) from" // some record are refereced more than one time
                + " (select expand(referredBy)"
                + " from (find references"
                + " (select expand(referredBy)"
                + " from (find references %s [task]"
                + ")))))", taskDoc.getIdentity())))
                .execute();   
        Contribution.removeAllUnusued(data);
    }// this method doesn't remove contributors
    
    static public ODocument loadDocByUuid(ODatabaseDocumentTx data, String taskUuid){
        ODocument result = null;
         Iterable<ODocument> i =  data.command(
                new OCommandSQL(" select *,"
                        + " parentTask.uuid as parentTaskUuid,"
                        + " pathToProject.uuid as pathToProjectUuid"
                        + " from Task"
                        + " where uuid = ?")).execute(taskUuid);
        for (ODocument doc : i ) {
            result = doc ;
        }
        return result;
    }
    
    

    static public Task loadByUuid(ODatabaseDocumentTx data, String taskUuid) {
        ODocument doc = loadDocByUuid(data, taskUuid);
        Task result = DBDocument.convertTo(doc, new Task());
        return result;
    }

    
    /**
     * get all subtask 
     * @param data database instance
     * @return subtask and current task if class = task, just subtask if class = project
     */
    public Collection<Task> getSubTasks (ODatabaseDocumentTx data) {
        Collection<Task> result = new ArrayList<Task>();
        Iterable<ODocument> i = data.command(new OCommandSQL(
                String.format(
                        "select expand(referredBy) from (find references %s [Task])", oid )))
                .execute();
        for(ODocument doc : i ) {
            result.add(DBDocument.convertTo(doc, new Task()));
        }
        return result;
    }
    
    /**
     * get informations about task
     * @param data
     * @return map with sum time of contributions, date of first contribution(min) and date of last contribution(max)
     */
    public Map getTaskInfos(ODatabaseDocumentTx data) {
        Map result = new HashMap();
        result.put("sum", 0);//if no contribution, map contain just field with sum=0
        ORID taskOid = Task.loadDocByUuid(data, uuid).getIdentity();
        Iterable<ODocument> i = data.command(
                    new OCommandSQL(String.format("select"
                            + " sum(contribution.addTime), min(contribution.startDate), max(contribution.endDate) "
                            + " from"
                            + " (select expand(referredBy)"
                            + " from (find references"
                            + " (select expand(referredBy)"
                            + " from (find references %s [task]))))"
                            + " where status = 'validate'", taskOid)))
                    .execute(); // get sum of time for contributions of all task and subtasks
            // and first start date / last endDate
            for(ODocument doc: i ) {
                result.put("sum", doc.field("sum"));
                result.put("min", doc.field("min"));
                result.put("max", doc.field("max"));
            }
        return result;
    }
    
  /*  public static Collection<Task> loadTaskWhere(ODatabaseDocumentTx data, Map<String, String> conditions) {
        Collection<Task> result = new ArrayList<Task>();
        String where = new String();
        if(!conditions.isEmpty()) {
            where = "where ";
        }
        for(String field :conditions.keySet()) {
         where += field + " = '" + conditions.get(field)+ "'";   
        }
        data.command(new OCommandSQL("select from task"
                + where)).execute();
        return result;
    }*/
}
