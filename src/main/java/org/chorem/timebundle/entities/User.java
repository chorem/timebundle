package org.chorem.timebundle.entities;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.metadata.security.OSecurity;
import com.orientechnologies.orient.core.metadata.security.OSecurityRole;
import com.orientechnologies.orient.core.metadata.security.OUser;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.chorem.timebundle.DBDocument;
import org.chorem.timebundle.TimeBundleContext;

/**
 * User interface that represent a user in database. This class 
 * contain all methods to modify and check integrity of users.
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Getter @Setter @ToString
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class User extends TimeBundleEntity {
    // constant
    final static public String NAME = "name";
    final static public String EMAILS = "emails";
    final static public String EMAILSTOVALIDATE = "emailsToValidate";
    final static public String INFO = "info";
    final static public String PASSWORD = "password";
    final static public String ROLES = "roles";
    final static public String STATUS = "status";
    final static public String USERTOVALID = "userToValid";
    
        /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(User.class);
    

    protected String name;
    protected Set emails;
    protected Set emailsToValidate ;
    protected String info;
    protected String password;
    protected Set roles;
    protected byte[] logo = new byte[0]; 
    protected String status;

    @Override
    public Map getMap() {
        Map result = super.getMap();
        result.put(User.NAME, name);
        result.put(User.EMAILS, emails);
        result.put(User.EMAILSTOVALIDATE, emailsToValidate);
        result.put(User.INFO, StringUtils.defaultString(info));
        result.put(User.PASSWORD, password);
        result.put(User.ROLES, roles.toArray());
        result.put(User.STATUS, status);
        return result;
    }

    public Set<String> getAllEmails() {
        Set<String> result = new LinkedHashSet<String>();
        result.addAll(this.getEmails());
        result.addAll(this.getEmailsToValidate());
        return result;
    }

    /**
     * save contributions modification
     * @param data
     * @param tbc
     * @return true, false if email exception
     */
    public boolean save(ODatabaseDocumentTx data, TimeBundleContext tbc) {
        
        boolean result = true;
        OSecurity security = data.getMetadata().getSecurity();
        ODocument userDoc = data.getRecord(new ORecordId(oid));
        Set<ODocument> userRoles = new HashSet();
        try {
            userRoles.addAll((Collection<ODocument>) userDoc.field(ROLES));
        } catch (NullPointerException eee) {
            //intercept exception if collection is null 
        }
        try {
            userRoles.add(security.getRole(DBDocument.ROLE_ANONYMOUS).getDocument()) ;
            userRoles.add(security.getRole(DBDocument.ROLE_WRITER).getDocument()) ;
            //if anonymous and writer was not present, adding them.
            for(Map<String, String> role: (Iterable<Map>) getRoles()) {
                String action = role.get("status");
                String id = role.get("id");
                if("add".equals(action)) {
                    userRoles.add(security.getRole(id).getDocument());
                } else if ("remove".equals(action)) {
                    userRoles.remove(security.getRole(id).getDocument());
                }
            }
        } catch (ClassCastException eee) {
            // if cast exception, roles is list<String> so, we put all roles in the user
            userRoles = new HashSet();
            userRoles.add(security.getRole(DBDocument.ROLE_ANONYMOUS).getDocument()) ;
            userRoles.add(security.getRole(DBDocument.ROLE_WRITER).getDocument()) ;
            for(String role: (Iterable<String>) getRoles()) {
                userRoles.add(security.getRole(role).getDocument());
            }
        } catch (NullPointerException eee) {
            // if null exception, roles is empty, so, there is no modification
        }
        setRoles(userRoles);
        
        Set<String> emails = new HashSet();
        try {
            emails.addAll((Collection<String>) userDoc.field(EMAILS));
        } catch (NullPointerException eee) {
            //intercept exception if collection is null 
        }
        try {   
           for(Map<String, String> emailMap: (Iterable<Map>) getEmails()) {
                String action = emailMap.get("status");
                String email = emailMap.get("id");
                if("add".equals(action)) {
                    emails.add(email);
                    findContributors(data, email, userDoc);
                } else if ("remove".equals(action)) {
                    emails.remove(email);
                }
            }
        } catch (ClassCastException eee) {
            // if cast exception, emails is list<String> so, we put all roles in the user
            emails = new HashSet();
            for(String email: (Iterable<String>) getEmails()) {
                emails.add(email);
            }
        } catch (NullPointerException eee) {
            // if null exception, emails is empty, so, there is no modification
        }
        setEmails(emails);
        
        Set<String> emailsToValidate = new HashSet<String>();
        try {
            emailsToValidate.addAll((Collection<String>) userDoc.field(EMAILSTOVALIDATE));
        } catch (NullPointerException eee) {
            //intercept exception if collection is null 
        }
        try {   
           for(Map<String, String> eToVMap: (Iterable<Map>) getEmailsToValidate()) {
                String action = eToVMap.get("status");
                String eToV = eToVMap.get("id");
                if("add".equals(action)) {
                    emailsToValidate.add(eToV);
                    String url = createUserValidation(data, eToV, tbc, userDoc.getIdentity());
                    try {
                        sendEmailValidation(eToV, tbc, url);
                    } catch (EmailException eee) {
                        log.error("mail not send", eee);
                        result = false;
                        emailsToValidate.remove(eToV);
                    }
                } else if ("remove".equals(action)) {
                    emailsToValidate.remove(eToVMap.get("id"));
                    data.command(new OCommandSQL(String.format("delete"
                        + " from user where userToValid = %s AND"
                        + " emails in ?", oid))).execute(eToV);
                    setStatus("ACTIVE");
                    // delete user created to valid email
                }
            }
        } catch (ClassCastException eee) {
            // if cast exception, emails is list<String> so, we put all roles in the user
            emails = new HashSet();
            for(String email: (Iterable<String>) getEmails()) {
                emails.add(email);
            }
        } catch (NullPointerException eee) {
            // if null exception, emails is empty, so, there is no modification
        } 
        setEmailsToValidate(emailsToValidate);
        userDoc.field(User.NAME, getName());
        userDoc.field(User.EMAILS, emails);
        userDoc.field(User.EMAILSTOVALIDATE, emailsToValidate);
        userDoc.field(User.INFO, getInfo());
        userDoc.field(User.ROLES, userRoles);

        userDoc.field(User.STATUS, getStatus());
        
        userDoc.save();
        
        return result; //true if no email error
    }
    
    
    public void delete(ODatabaseDocumentTx data, String userName) {
        ODocument userDoc = User.loadDocByName(data, userName);
        data.command(new OCommandSQL(String.format("update"
                + " (find references %s)" 
                + " set user = null", userDoc.getIdentity()))).execute();
        //delete link between user and contributors
        Iterable<String> eToV = userDoc.field("emailsToValidate");
        if(eToV != null) {
            for(String email: eToV) {
              User.loadDocByEmail(data, email).delete().save();
            }
        }
        userDoc.delete().save();
    }
    
    /**
     * check some informations about user integrity
     * @param data database instance
     * @return string error message, null if any
     */
    public String check(ODatabaseDocumentTx data) {
        StringBuilder sb = new StringBuilder();
        String sep = "";
        
        if (StringUtils.isBlank(getName())) {
            sb.append(sep).append("Blank name disallowed");
            sep = "\n";
        } else {
            if (exists(data, User.NAME)) {
                sb.append(sep).append("Name already used");
                sep = "\n";
            }
        }


        if (StringUtils.isBlank(getPassword())) {
            sb.append(sep).append("Blank password disallowed");
        }
        
        String result = null;
        if (sb.length() > 0) {
            result = sb.toString();
        }
        return result;
    }
    
    
    /**
     * create new user in database with current java user parameter.
     * @param data current database
     * @param tbc
     * @return true if no email error.
     */
    public boolean create(ODatabaseDocumentTx data, TimeBundleContext tbc) {
        data.begin();
        try {
            ODocument userDoc = new ODocument("User");
            OSecurityRole writer = DBDocument.getWriterRole(data);
            OSecurityRole anonymous = DBDocument.getAnonymousRole(data);
            userDoc.field(User.EMAILS, emails);
            userDoc.field(User.EMAILSTOVALIDATE, emailsToValidate);
            userDoc.field(User.INFO, info);
            userDoc.field(User.NAME, name);
            userDoc.field(User.PASSWORD, password);
            userDoc.field(User.STATUS, "SUSPENDED");
            userDoc.save();
            userDoc.reload();
            OUser user = new OUser(userDoc);
            user.addRole(writer);
            // all user have writer role to write or read some part of database
            user.addRole(anonymous);
            for (String roleName :(Set<String>) roles) {
                user.addRole(roleName);
            }
            user.getDocument().save();
            for (String emailToValidate :(Set<String>) emailsToValidate) {
                String url = createUserValidation(data, emailToValidate, tbc, userDoc.getIdentity());
                data.commit();
                sendEmailValidation(emailToValidate,tbc, url);
            }
        } catch (EmailException eee) {
            data.rollback();
            log.error("email not sent", eee);
            return false;
        }
        return true;
    }
    
    private String createUserValidation(
            ODatabaseDocumentTx data,
            String adressTo,
            TimeBundleContext tbc,
            ORID userOid) {
        ODocument userValidation = new ODocument("User");
        String login = UUID.randomUUID().toString();
        userValidation.field(User.NAME, login);
        userValidation.field(User.PASSWORD, login);
        userValidation.field(User.EMAILS, adressTo);
        userValidation.field(User.USERTOVALID, userOid, OType.LINK);
        userValidation.field(User.STATUS, "ACTIVE");
        userValidation.field(User.ROLES, DBDocument.getValidEmailRole(data).getIdentity());
        data.save(userValidation);
        return tbc.createUrlValidation(adressTo, getName(), login);
    }
    
    /**
     * send an email with a link to validate it
     * @param adressTo
     * @param tbc 
     * @param urlValidation 
     * @throws org.apache.commons.mail.EmailException 
     */
    public void sendEmailValidation(String adressTo, TimeBundleContext tbc, String urlValidation) throws EmailException {
        log.info("sending email");
        String subject = "email validation for TimeBundle";
        String message = "Please you need to validate the email "
                + adressTo
                + " before using it.\n"
                +"Follow this link : "
                + urlValidation
                + " .\n"
                + "\n"
                + "Regards.\n"
                + "Timebundle teams\n";
        Set to = new HashSet();
        to.add(adressTo);
        SimpleEmail email = tbc.configEmail(subject, message, to, null);
        email.send();

    }
    
    
    /**
//     * this method is call by user service to find contributors they have new valid email.
     * add link to user for all contributors with this email
     * @param data database instance
     * @param email email to validate
     * @param userDoc ODocument user with this email
     */
    public void findContributors(ODatabaseDocumentTx data, String email, ODocument userDoc) {
        
        log.info("findContributors : "+ email + " user "+userDoc.getIdentity());
        data.command(
                new OCommandSQL(String.format("update"
                        + " contributor"
                        + " set user = %s"
                        + " where email in ?", userDoc.getIdentity()))).execute(email);
    }
    
       /**
     * get all roles from active user
     * @param data
     * @param userName
     * @return map with project name+(admin/reader) as key and role name as value
     */
    static public Set<Map> getRoles(
        ODatabaseDocumentTx data,
        String userName
    ) {
        log.info("get roles from user");
        HashSet<Map> result = new HashSet<Map>();
            OUser user = data.getMetadata().getSecurity().getUser(userName);
            if(user.hasRole(DBDocument.ROLE_CREATOR, false)) {
                Map roleMap = new HashMap();
                roleMap.put("name", "superAdmin");
                roleMap.put("value", DBDocument.ROLE_CREATOR);
                result.add(roleMap);
            } else {
                Iterable<ODocument> list = new OCommandSQL("select"
                        + " name, _allow.name, _allowRead.name"
                        + " from"
                        + " (select name, _allow, _allowRead, uuid"
                        + " from project unwind _allow, _allowRead)"
                        + " where _allow.name <> 'admin'"
                        + " AND _allowRead.name <> 'anonymous'").execute();
                for (ODocument role : list) {
                    if(user.hasRole((String) role.field("_allow"), true)) {
                        Map roleMap = new HashMap();
                        roleMap.put("name", role.field("name"));
                        roleMap.put("role", "admin");
                        roleMap.put("value", role.field("_allow"));
                        result.add(roleMap);
                    }
                    if(user.hasRole((String) role.field("_allowRead"), true)) {
                        Map roleMap = new HashMap();
                        roleMap.put("name", role.field("name"));
                        roleMap.put("role", "reader");
                        roleMap.put("value", role.field("_allowRead"));
                        result.add(roleMap);
                    }

                }
            }
        return result;
    }
    
    
    
    
    static public ODocument loadDocByName(ODatabaseDocumentTx data, String userName) {
        ODocument result = null;
        Iterable<ODocument> i = data.command(
                new OCommandSQL("select * from User where name=?")).execute(userName);
        
        for (ODocument doc : i) {
            result = doc ;
        }
        return result;
    }
    
    static public User loadByName(ODatabaseDocumentTx data, String userName) {
        ODocument doc = loadDocByName(data, userName);
        User result = DBDocument.convertTo(doc, new User());
        return result;
    }

    static public ODocument loadDocByEmail(ODatabaseDocumentTx data, String email) {
        Iterable<ODocument> i = data.command(
                new OCommandSQL("select * from User where emails in ?")).execute(email); 
        for (ODocument doc : i ) {
            return doc;
        }
        return null;
    }
    
    static public ODocument loadDocByEmailToV(ODatabaseDocumentTx data, String email) {
        Iterable<ODocument> i = data.command(
                new OCommandSQL("select * from User where emailsToValidate in ?")).execute(email); 
        for (ODocument doc : i ) {
            return doc;
        }
        return null;
    }
    
    static public User loadByEmail(ODatabaseDocumentTx data, String email) {
        ODocument doc = loadDocByEmail(data, email);
        User result = DBDocument.convertTo(doc, new User());
        return result;
    }
}
