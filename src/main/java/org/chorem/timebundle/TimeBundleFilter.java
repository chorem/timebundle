package org.chorem.timebundle;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * class to add access autorizations for http request.
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$ by : $Author$
 */
@Provider
public class TimeBundleFilter implements ContainerRequestFilter, ContainerResponseFilter {

    /**
     * to use log facility, just put in your code: log.info(\"...\");
     */
    static private Log log = LogFactory.getLog(TimeBundleFilter.class);

    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {
        String origin = requestContext.getHeaderString("Origin");
        if (origin != null) {
            responseContext.getHeaders()
                    .putSingle("Access-Control-Allow-Origin", origin);
            responseContext.getHeaders()
                    .putSingle("Access-Control-Allow-Credentials",
                            "true");
            responseContext.getHeaders()
                    .putSingle("Access-Control-Allow-Headers",
                            "X-tbHost, X-FetchRequest, Content-Type, Accept, Authorization, Origin");
        }
    }

    public void filter(ContainerRequestContext requestContext) throws IOException {
        requestContext.setProperty("tbpath", "/api/v1");
    }

}
