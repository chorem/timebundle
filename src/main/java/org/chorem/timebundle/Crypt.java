package org.chorem.timebundle;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.security.Key;
import java.util.UUID;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class Crypt {

    protected Key aesKey = null;
    protected Cipher encipher = null;
    protected Cipher decipher = null;

    public Crypt() {
        this(null);
    }

    public Crypt(String keyStr) {
        try {
            if (keyStr == null) {
                keyStr = UUID.randomUUID().toString();
            }
            while (keyStr.length() < 16) {
                keyStr += keyStr;
            }

            keyStr = keyStr.substring(0, 16);

            aesKey = new SecretKeySpec(keyStr.getBytes(), "AES");
            encipher = Cipher.getInstance("AES");
            encipher.init(Cipher.ENCRYPT_MODE, aesKey);

            decipher = Cipher.getInstance("AES");
            decipher.init(Cipher.DECRYPT_MODE, aesKey);
        } catch(Exception eee) {
            throw new RuntimeException(eee);
        }
    }

    public String encrypt(String text) {
        try {
            return toHexString(encipher.doFinal(text.getBytes()));
        } catch(Exception eee) {
            return null;
        }
    }

    public String decrypt(String text) {
        try {
            return new String(decipher.doFinal(toByteArray(text)));
        } catch(Exception eee) {
            return null;
        }
    }

    protected String toHexString(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }

    protected byte[] toByteArray(String s) {
        return DatatypeConverter.parseHexBinary(s);
    }
}
