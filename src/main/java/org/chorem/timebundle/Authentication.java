package org.chorem.timebundle;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Getter @Setter
public class Authentication {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(Authentication.class);

    final static public Crypt crypt = new Crypt("TODO" /*TODO configure crypto password*/);
    final static public int COOKIE_MAX_AGE = 60*60*24*365; //Store cookie for 1 year

    @Context
    protected HttpServletResponse response;
    @HeaderParam("Authorization")
    protected AuthenticationParam basicAuth;
    @CookieParam("tb-auth")
    protected AuthenticationCookie cookieAuth;

    @DefaultValue("")
    @PathParam("contributorToken")
    protected String contributorToken;

    @DefaultValue("")
    @PathParam("account")
    protected String accountName;

    protected AuthenticationParam auth;

    public Authentication() {
    }

    public String getLogin() {
        return getAuth().getLogin();
    }

    public String getPassword() {
        return getAuth().getPassword();
    }

    public AuthenticationParam getAuth() {
        if (auth == null) {
            if (StringUtils.isNotBlank(contributorToken)) {
                auth = new AuthenticationParam(contributorToken, contributorToken);
            } else {
                if (basicAuth != null && StringUtils.isNotBlank(basicAuth.getLogin())) {
                    auth = basicAuth;
                } else if (cookieAuth != null && StringUtils.isNotBlank(cookieAuth.getLogin())) {
                    auth = cookieAuth;
                }
                if (auth != null) {
                    Cookie authCookie = new Cookie("tb-auth",
                            Authentication.crypt.encrypt(auth.getAuth()));
                    authCookie.setMaxAge(COOKIE_MAX_AGE);
                    authCookie.setPath("/");
                    response.addCookie(authCookie);

                    Cookie accountCookie = new Cookie("tb-account", accountName);
                    accountCookie.setPath("/");
                    accountCookie.setMaxAge(COOKIE_MAX_AGE);
                    response.addCookie(accountCookie);
                } else {
                    auth = new AuthenticationParam(DBDocument.ROLE_ANONYMOUS, DBDocument.ROLE_ANONYMOUS);
                }
            }
        }
        return auth;
    }

    public void setContributorToken(String contributorToken) {
        this.auth = null;
        this.contributorToken = contributorToken;
    }
}
