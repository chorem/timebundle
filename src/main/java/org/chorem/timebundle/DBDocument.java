package org.chorem.timebundle;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.orientechnologies.orient.core.Orient;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OSchemaProxy;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.metadata.security.ORestrictedOperation;
import com.orientechnologies.orient.core.metadata.security.ORole;
import com.orientechnologies.orient.core.metadata.security.ORule;
import com.orientechnologies.orient.core.metadata.security.OSecurity;
import com.orientechnologies.orient.core.metadata.security.OSecurityRole;
import com.orientechnologies.orient.core.metadata.security.OUser;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.core.storage.OStorage;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.entities.Account;
import org.chorem.timebundle.entities.Project;
import org.chorem.timebundle.entities.TimeBundleEntity;
import org.chorem.timebundle.entities.User;

/**
 * class to manage database and modify schema.
 * @author ollive
 */
@Getter @Setter @ToString 
public class DBDocument {
    
    
    /**
     * to use log facility, just put in your code: log.info(\"...\");
     */
    final static private Log log = LogFactory.getLog(DBDocument.class);

    final static public String ROLE_CREATOR = "admin";
    final static public String ROLE_ANONYMOUS = "anonymous";
    final static public String ROLE_WRITER = "writer";
    final static public String CONTRIBUTOR_ROLE = "contributor";
    final static public String EMAIL_ROLE = "validEmail";
    final static public String ALERT_EMAIL = "alerter";
    
  //  final static public String USER_ANONYMOUS = "$anonymous$";
    final static public double  DB_VERSION = 1 ;
    final static public double  DB_SUB_VERSION = 4 ;

    protected String prefixUrl;
    protected String account;
    protected String login;
    protected String password;
    
    /**
     * create a new instance of database to acced to database
     * @param prefixUrl prefix used for all account database (ex: "plocal:/tmp/db")
     * @param account the name of the database
     * @param login login of user  
     * @param password pasword of user
     */
    public DBDocument(String prefixUrl, String account, String login, String password) {
        this.prefixUrl = prefixUrl;
        this.account = account;
        this.login = login;
        this.password = password;
    }
    /**
     * return the url location of current database
     * @param account the name of the database
     * @return url location
     */
    public String getUrl(String account) {
        log.debug("Ask db url for: '" + account + "'");
        if (StringUtils.isBlank(account)) {
            throw new TimeBundleNotFoundException("database name can't be blank");
        }
        return prefixUrl + account;
    }
    
    static public OSecurityRole getWriterRole(ODatabaseDocumentTx data) {
        return data.getMetadata().getSecurity().getRole("writer");
    }
    
    static public OSecurityRole getAnonymousRole(ODatabaseDocumentTx data) {
        return data.getMetadata().getSecurity().getRole("anonymous");
    }
    
    static public OSecurityRole getValidEmailRole(ODatabaseDocumentTx data) {
        return data.getMetadata().getSecurity().getRole(EMAIL_ROLE);
    }
    
    /**
     * check if database exist
     * @param account
     * @return true if exist
     */
    public boolean exists(String account) {
        OStorage storage = Orient.instance().loadStorage(getUrl(account));
        return storage.exists();
    }
    /**
     * connect instance to database
     * @return the ODatabaseDocumentTx instance
     */
    public ODatabaseDocumentTx open() {
        if (account == null || !exists(account)) {
            throw new TimeBundleNotFoundException("Database doesn't exists");
        }
        ODatabaseDocumentTx result = new ODatabaseDocumentTx(getUrl(account)).open(login, password);
        return result;
    }
    
    /**
     * close current database
     * @param data 
     */
    public void shutdown (ODatabaseDocument data) {
        data.close();
    }
    
    /**
     * convert ODocument to T
     * @param <T> class name extends TimeBundleEntity we want to convert to
     * @param doc Odocument to convert
     * @param o an object with class destination
     * @return object with the class T
     */
    static public <T extends TimeBundleEntity> T convertTo(ODocument doc, T o) {
        if (doc == null) {
            return null;
        }
        if (doc.field("rid")!= null) {
            o.setOid(doc.field("rid").toString());
        } else {
            o.setOid(doc.field("@rid").toString());
        }
        o.setFieldFrom(doc);
        return o;
    }
    
    /**
     * set value to specific field of object do nothing if error
     * @param target the object to modify
     * @param name  name of field object
     * @param value new value of field object
     */
    static public void setField(Object target, String name, Object value) {
        try {
            BeanUtils.setProperty(target, name, value);
        } catch(Exception eee) {
            log.debug(eee);
            // do nothing
        }
    }
        
    /**
     * load Odocument from databse by specific field
     * @param data  the current database
     * @param type  ODocument class of target
     * @param field field to do selection 
     * @param value value to filter result
     * @return The ODocument instance researched null if any
     */
    static public ODocument loadOneBy(ODatabaseDocumentTx data, String type, String field, String value) {
        if( value != null) {
            for (ODocument doc : data.browseClass(type, false)) {
                if (doc.field(field).toString().contentEquals(value)) {
                    return doc;
                }
            }  
        }
        return null;
    }
    
    /**
     * load object class T from database by specific field
     * @param <T> class extends TimeBundleEntity for return
     * @param data  current database
     * @param result object of class for return
     * @param field field to do selection
     * @param value value to filter result
     * @return the object with T class, null if any
     */
    static public <T extends TimeBundleEntity> T loadOneBy(ODatabaseDocumentTx data, T result, String field, String value) {
        ODocument doc = loadOneBy(data, result.getClass().getSimpleName(), field, value);
        result = DBDocument.convertTo(doc, result);
        return result;
    }   
    
    /**
     * allow all to user "adminProject" and read to user "readerProject" for the current document
     * @param data  current database
     * @param project   project of users
     * @param doc document to add permissions
     */
    static public void addPermissions(ODatabaseDocumentTx data, Project project, ODocument doc) {    
            OSecurity sm = data.getMetadata().getSecurity();
            
            sm.allowRole(doc, ORestrictedOperation.ALLOW_READ,project.getReaderRoleName(data));
            sm.allowRole(doc, ORestrictedOperation.ALLOW_ALL,project.getAdminRoleName(data));
            if(project.isPublic()) {
                sm.allowRole(doc, ORestrictedOperation.ALLOW_READ, ROLE_ANONYMOUS);
            }
    }
        
    /**
     * create a new database with all classes, document and right for timebundle
     * @param account   name of the database
     * @param admin     user who will be superAdmin
     */
    public void create(Account account, User admin){
        log.info("database creation: start tx");
            
        // OPEN THE DATABASE
        ODatabaseDocumentTx db = new ODatabaseDocumentTx 
            (getUrl(account.getName())).create();
        try{
            OSchemaProxy SchemaDB = db.getMetadata().getSchema();
            
            log.info("database creation: create type User");
            OClass userClass = SchemaDB.createClass("User",SchemaDB.getClass("OUser"));
            userClass.createProperty("createDate", OType.DATETIME);
            userClass.createProperty("emails", OType.EMBEDDEDSET, OType.STRING); // all email, used to link with contributors
            userClass.createProperty("emailsToValidate", OType.EMBEDDEDSET, OType.STRING); // key : token, value : email
            userClass.createProperty("info", OType.STRING);
            userClass.createProperty("logo", OType.BINARY);
            userClass.createProperty("userToVald", OType.LINK);
            
            
            log.info("database creation: create type Account");
            OClass accountClass = SchemaDB.createClass("Account");
            accountClass.createProperty("createDate", OType.DATETIME);
            accountClass.createProperty("name", OType.STRING);
            accountClass.createProperty("info", OType.STRING);
            accountClass.createProperty("public", OType.BOOLEAN);
            
            log.info("database creation: create type Task");
            OClass taskClass = SchemaDB.createClass("Task");
            taskClass.createProperty("uuid", OType.STRING).setMandatory(true);
            taskClass.createProperty("createDate", OType.DATETIME);
            taskClass.createProperty("name", OType.STRING);
            taskClass.createProperty("info", OType.STRING);
            taskClass.createProperty("duration", OType.LONG);
            taskClass.createProperty("startDate", OType.DATETIME);
            taskClass.createProperty("endDate", OType.DATETIME);
            taskClass.createProperty("openToContribution", OType.BOOLEAN);
            taskClass.createProperty("contributionValidation", OType.BOOLEAN);
            taskClass.createProperty("parentTask", OType.LINK, taskClass);
            taskClass.createProperty("pathToProject", OType.LINKLIST, taskClass);
            
            log.info("database creation: create type Project");
            OClass projectClass = SchemaDB.createClass("Project", taskClass);
            projectClass.createProperty("logo", OType.BINARY);
            projectClass.createProperty("public", OType.BOOLEAN);
            
            log.info("database creation: create type Contributor");
            // Contributor can only show One Project and task where he can contribute on this project
            // If contributor has contributeToAll == true, then when new task is
            //   added to projet this contributor right must be set to read for this new task
            // Contributor can be linked with user
            // Contributor must be linked with Project            
            OClass contributorClass = SchemaDB.createClass("Contributor", SchemaDB.getClass("OUser"));
            contributorClass.createProperty("createDate", OType.DATETIME);
            contributorClass.createProperty("email", OType.STRING);
            // if true, all contribution for this contributor must be validate
            contributorClass.createProperty("contributionValidation", OType.BOOLEAN);
            // if true, can contribute to all task of project.
            // (when new task is added to project, this contributor must be added to reader)
            contributorClass.createProperty("contributeToAll", OType.BOOLEAN);
            contributorClass.createProperty("status", OType.STRING);
            contributorClass.createProperty("info", OType.STRING);
            // property name is use to store token with empty password
            contributorClass.createProperty("name", OType.STRING);
            contributorClass.createProperty("project", OType.LINK , projectClass);
            contributorClass.createProperty("user", OType.LINK, userClass) ;
            
            log.info("database creation: create type Contribution");
            // Contributor <- Contribution -> Task
            OClass contributionClass = SchemaDB.createClass("Contribution");
            //contributionClass.createProperty("uuid", OType.STRING);
            contributionClass.createProperty("uid", OType.STRING); // unique id send by client
            //contributionClass.createProperty("createDate", OType.DATETIME);
            //contributionClass.createProperty("timezone", OType.INTEGER);
            contributionClass.createProperty("startDate", OType.DATETIME);
            contributionClass.createProperty("endDate", OType.DATETIME);
            contributionClass.createProperty("addTime", OType.LONG);
            contributionClass.createProperty("info", OType.STRING);
            //contributionClass.createProperty("validate", OType.BOOLEAN);
            //contributionClass.createProperty("contributor", OType.LINK, contributorClass);
            
            log.info("database creation: create type contributionStatus");
            // association class to specified status of each contribution
            OClass contributionStatusClass = SchemaDB.createClass("contributionStatus");
            contributionStatusClass.createProperty("uuid", OType.STRING);
            contributionStatusClass.createProperty("timezone", OType.INTEGER);
            contributionStatusClass.createProperty("createDate", OType.DATETIME);
            contributionStatusClass.createProperty("status", OType.STRING); //[toValid, validate, blacklist]
            contributionStatusClass.createProperty("contribution", OType.LINK, contributionClass);
            contributionStatusClass.createProperty("contributor", OType.LINK, contributorClass);
            contributionStatusClass.createProperty("task", OType.LINK, taskClass);
            
            log.info("database creation: create type AlertEmail");
            OClass alertEmailClass = SchemaDB.createClass("AlertEmail");
            alertEmailClass.createProperty("from", OType.STRING);
            alertEmailClass.createProperty("ruleTo", OType.STRING);
            alertEmailClass.createProperty("to", OType.EMBEDDEDSET);
            alertEmailClass.createProperty("ruleBcc", OType.STRING);
            alertEmailClass.createProperty("bcc", OType.EMBEDDEDSET);
            alertEmailClass.createProperty("subject", OType.STRING);
            alertEmailClass.createProperty("message", OType.STRING);
            alertEmailClass.createProperty("sent", OType.BOOLEAN);
            alertEmailClass.createProperty("condition", OType.INTEGER);
            alertEmailClass.createProperty("task", OType.LINK);
            

            log.info("database creation: add ORestricted on Contribution, Task, Contributor and project");
            db.command(new OCommandSQL("alter class Contribution superclass +ORestricted")).execute();
            db.command(new OCommandSQL("alter class Task superclass +ORestricted")).execute();
            db.command(new OCommandSQL("alter class Contributor superclass +ORestricted")).execute();
            db.command(new OCommandSQL("alter class Project superclass +ORestricted")).execute();
            db.command(new OCommandSQL("alter class AlertEmail superclass +ORestricted")).execute();
            db.command(new OCommandSQL("alter class ContributionStatus superclass +ORestricted")).execute();
            
            
            log.info("database creation: create index");
            projectClass.createIndex("Project.name", OClass.INDEX_TYPE.UNIQUE, "name");
            userClass.getProperty("emails").createIndex(OClass.INDEX_TYPE.UNIQUE, new ODocument().field("ignoreNullValues",true));
            SchemaDB.createClass("metadata");
            SchemaDB.save();
        } finally {
            shutdown(db);
        }
        
        
         log.info("database creation: start to add initial data");
        
        db.open("admin","admin");//default orientDB admin user created with db creation
        try {
      
            log.info("database creation: create account");
            OSecurity sm = db.getMetadata().getSecurity();
            ODocument accountDoc = db.newInstance("account");
            accountDoc.field("createDate", new Date());
            accountDoc.fromMap(account.getMap());
            db.save(accountDoc);
            
            
            log.info("database creation: create roles");
            
            ORole anonymous = sm.createRole("anonymous", OSecurityRole.ALLOW_MODES.DENY_ALL_BUT);
            anonymous.addRule(ORule.ResourceGeneric.DATABASE, null, ORole.PERMISSION_READ);
            anonymous.addRule(ORule.ResourceGeneric.CLASS,null, ORole.PERMISSION_READ);
            anonymous.addRule(ORule.ResourceGeneric.CLUSTER,"internal",ORole.PERMISSION_READ);
            anonymous.addRule(ORule.ResourceGeneric.CLUSTER,null, ORole.PERMISSION_READ);
            anonymous.addRule(ORule.ResourceGeneric.COMMAND,null, ORole.PERMISSION_READ);
            anonymous.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS,null, ORole.PERMISSION_READ);
            anonymous.save();
            
            ORole alerter = sm.createRole("alerter", anonymous, OSecurityRole.ALLOW_MODES.DENY_ALL_BUT);
            alerter.addRule(ORule.ResourceGeneric.CLASS,"AlertEmail", ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE);
            alerter.addRule(ORule.ResourceGeneric.CLUSTER,"AlertEmail", ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE);
            alerter.save();
            //role for user who will send alert
            
            ORole writer = sm.getRole("writer");
            writer.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS, null, ORole.PERMISSION_READ);
            writer.addRule(ORule.ResourceGeneric.CLASS,"OUser", ORole.PERMISSION_READ);
            writer.addRule(ORule.ResourceGeneric.CLASS,"User", ORole.PERMISSION_READ+
                        ORole.PERMISSION_CREATE+ORole.PERMISSION_UPDATE);
            writer.revoke(ORule.ResourceGeneric.CLASS, "user", ORole.PERMISSION_DELETE);
            writer.addRule(ORule.ResourceGeneric.CLASS,"Contributor",ORole.PERMISSION_ALL);
            writer.addRule(ORule.ResourceGeneric.CLASS, "Project", ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE+ORole.PERMISSION_DELETE);
            writer.revoke(ORule.ResourceGeneric.CLASS, "Project", ORole.PERMISSION_CREATE);
            writer.addRule(ORule.ResourceGeneric.CLASS, "Task", ORole.PERMISSION_ALL);
            writer.addRule(ORule.ResourceGeneric.CLUSTER,"internal",ORole.PERMISSION_READ);
            writer.addRule(ORule.ResourceGeneric.CLASS,"Contribution",ORole.PERMISSION_ALL);
            writer.addRule(ORule.ResourceGeneric.CLUSTER,"ContributionStatus",ORole.PERMISSION_ALL);
            writer.addRule(ORule.ResourceGeneric.CLASS, "AlertEmail", ORole.PERMISSION_ALL);
            writer.revoke(ORule.ResourceGeneric.CLASS, "metadata", ORole.PERMISSION_ALL);
            //just admin can see metadata
            writer.save();

            ORole ContributorRole = sm.createRole("contributor", OSecurityRole.ALLOW_MODES.DENY_ALL_BUT);
            ContributorRole.addRule(ORule.ResourceGeneric.CLUSTER,null, ORole.PERMISSION_CREATE+
                    ORole.PERMISSION_DELETE+ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE);
            ContributorRole.addRule(ORule.ResourceGeneric.CLASS, "Contribution", ORole.PERMISSION_ALL);
            ContributorRole.addRule(ORule.ResourceGeneric.CLUSTER, "Contribution", ORole.PERMISSION_ALL);
            ContributorRole.addRule(ORule.ResourceGeneric.CLASS, "ContributionStatus", ORole.PERMISSION_ALL);
            ContributorRole.addRule(ORule.ResourceGeneric.CLUSTER, "ContributionStatus", ORole.PERMISSION_ALL);
            ContributorRole.addRule(ORule.ResourceGeneric.CLASS, "Task", ORole.PERMISSION_READ);
            ContributorRole.addRule(ORule.ResourceGeneric.CLASS, "Project", ORole.PERMISSION_READ);
            ContributorRole.addRule(ORule.ResourceGeneric.CLASS, "Contributor", ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE);
            ContributorRole.addRule(ORule.ResourceGeneric.DATABASE, null, ORole.PERMISSION_READ);
            ContributorRole.addRule(ORule.ResourceGeneric.COMMAND, null, ORole.PERMISSION_READ);
            ContributorRole.addRule(ORule.ResourceGeneric.SCHEMA, null, ORole.PERMISSION_READ);
            ContributorRole.save();

            ORole emailRole = sm.createRole(EMAIL_ROLE, OSecurityRole.ALLOW_MODES.DENY_ALL_BUT);
            emailRole.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS,null, 
                    ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE+ORole.PERMISSION_DELETE);
            emailRole.addRule(ORule.ResourceGeneric.DATABASE, null, ORole.PERMISSION_READ);
            emailRole.addRule(ORule.ResourceGeneric.COMMAND, null, ORole.PERMISSION_READ);
            emailRole.addRule(ORule.ResourceGeneric.CLASS, "user", 
                    ORole.PERMISSION_DELETE+ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE);
            emailRole.addRule(ORule.ResourceGeneric.CLASS, "Contributor",
                    ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE);
            emailRole.addRule(ORule.ResourceGeneric.CLUSTER, null,
                    ORole.PERMISSION_UPDATE+ORole.PERMISSION_DELETE);
            emailRole.addRule(ORule.ResourceGeneric.CLUSTER, "user",
                    ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE+ORole.PERMISSION_DELETE);
            emailRole.save();
            
            
            log.info("database creation: remove default users");
            sm.dropUser("admin");
            sm.dropUser("reader");
            sm.dropUser("writer");
            sm.createUser("alerter", "alerter", "alerter");
            //create alerter user;
            
            
            ODocument metadata = db.newInstance("metadata");
            metadata.field("name", account.getName());
            metadata.field("admin", accountDoc);
            metadata.field("version_db", DB_VERSION);
            metadata.field("sub_version_db", DB_SUB_VERSION);
            db.save(metadata);
            
            log.info("database creation: create admin user");
            ODocument adminDoc = db.newInstance("User");
            adminDoc.field("name", admin.getName());
            adminDoc.field("status", "ACTIVE");
            adminDoc.field("password", admin.getPassword());
            adminDoc.field("emails", admin.getEmails());
            adminDoc.field("emailsToValidate", admin.getEmailsToValidate());
            adminDoc.field("info", admin.getInfo());
            adminDoc.field("logo", admin.getLogo());
            OUser adminUser = new OUser(adminDoc);
            adminUser.addRole("admin");
            adminUser.getDocument().save();
            
            ODocument anonymousDoc = db.newInstance("User");
            anonymousDoc.field("name", "anonymous");
            anonymousDoc.field("status", "ACTIVE");
            anonymousDoc.field("password", "anonymous");
            OUser anonymousUser = new OUser(anonymousDoc);
            anonymousUser.addRole("anonymous");
            anonymousUser.getDocument().save();
            anonymousDoc.reload();
            anonymousDoc.field("userToValid", anonymousDoc.getIdentity());
            anonymousDoc.save();
            sm.close(false);
        } finally {
            shutdown(db);
            log.info("database created");
        }

    }
}
