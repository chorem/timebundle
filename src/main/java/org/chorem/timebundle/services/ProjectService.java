package org.chorem.timebundle.services;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.exception.OSecurityException;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.storage.ORecordDuplicatedException;
import java.util.ArrayList;
import java.util.Collection;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;
import org.chorem.timebundle.TimeBundleContext;
import org.chorem.timebundle.entities.Project;
import org.jboss.resteasy.annotations.Form;

/**
 * interface to interact with projects from web. this class can't modify directly 
 * database, she must use project class.
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Path("")
public class ProjectService extends TimeBundleService {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(ProjectService.class);

    /**
     * get all projects of database
     * @param tbc 
     * @return collection of projects in json format
     */
    @GET
    @Path("/{account}/projects")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Project> getProjects(
            @Form TimeBundleContext tbc
    ) {
        log.info("get projects");
        Collection<Project> result = new ArrayList<Project>();

        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            
            for (ODocument doc : data.browseClass("Project")) {
                result.add(DBDocument.convertTo(doc, new Project()));
            }
        } finally {
            tbc.closeDB(data);
        }
        
        return result;
    }

    /**
     * get specific project from database
     * @param tbc
     * @param projectUuid uuid of project
     * @return specific project in json format
     */
    @GET
    @Path("/{account}/projects/{project}")
    @Produces(MediaType.APPLICATION_JSON)
    public Project getProject(
            @Form TimeBundleContext tbc,
            @PathParam("project") String projectUuid
    ) {
        log.info("get specific project");
        Project project;
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            project = Project.loadByUuid(data, projectUuid);
        } finally {
            tbc.closeDB(data);
        }

        return project;
    }
    
    /**
     * save specific project in database
     * @param tbc
     * @param project project to save
     * @return http status : 200, 201, 400, 301 or 500
     */
    @POST
    @Path("/{account}/projects")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object saveProjects(
            @Form TimeBundleContext tbc,
            Project project
    ) {
        log.info("saving project");
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            String errorMessage = project.check(data);

            if (errorMessage != null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
            } else {
                try {
                    if (project.exists()) {
                        project.save(data);
                        return Response.status(Response.Status.ACCEPTED).build();
                    } else {
                        project.create(data);
                        return Response.status(Response.Status.CREATED).build();
                    }
                } catch (OSecurityException e) {
                    return Response.status(Response.Status.FORBIDDEN).build();
                }
                
            }
        } catch (ORecordDuplicatedException eee) {
            log.info("project not saved, name already used", eee) ;
          return Response.status(Response.Status.CONFLICT).entity("project already used").build();
        }catch(Exception eee) {
            log.info("Can't save project", eee);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } finally {
            tbc.closeDB(data);
        }
    }
    
        /**
     * remove specific project
     * @param tbc
     * @param projectUuid 
     */
    @DELETE
    @Path("/{account}/projects/{project}")
    public void removeProject(
            @Form TimeBundleContext tbc,
            @PathParam("project") String projectUuid
        ) {
        log.info("remove project");
        ODatabaseDocumentTx data = tbc.getDB().open();
        Project project = Project.loadByUuid(data, projectUuid);
        project.remove(data);
    }

}
