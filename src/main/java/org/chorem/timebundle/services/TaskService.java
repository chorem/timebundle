package org.chorem.timebundle.services;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.exception.OSecurityAccessException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.TimeBundleContext;
import org.chorem.timebundle.entities.Task;
import org.jboss.resteasy.annotations.Form;

/**
 * interface to interact with tasks from web. this class can't modify directly 
 * database, she must use task class.
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Path("")
public class TaskService extends TimeBundleService {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(TaskService.class);

    /**
     * get subtasks, direct if all is false and all if all is true
     * @param tbc
     * @param projectUuid
     * @param all
     * @return 
     */
    @GET
    @Path("/{account}/parents/{project}/tasks/all/{all}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Task> getSubTasks(
            @Form TimeBundleContext tbc,
            @PathParam("project") String projectUuid,
            @DefaultValue("false") @PathParam("all") boolean all
    ) {
        log.info("get sub tasks");
        ODatabaseDocumentTx data = tbc.getDB().open();                
         Collection<Task> result = new ArrayList<Task>();
     try {
         if(all) {
            Task task = Task.loadByUuid(data, projectUuid);
            result = task.getSubTasks(data);
         } else {
             result = tbc.getDirectSubTasks(data, projectUuid);
         }
        } catch(Exception eee) {
            log.debug("Can't get tasks", eee);
        } finally {
            tbc.closeDB(data);
        }
        return result;
    }
    
    
    /**
     * get specific task
     * this méthod replace pathToProjectUuid value by pathToProject name
     * @param tbc
     * @param taskUuid
     * @return 
     */
    @GET
    @Path("/{account}/tasks/{task}")
    @Produces(MediaType.APPLICATION_JSON)
    public Task getTask(
            @Form TimeBundleContext tbc,
            @PathParam("task") String taskUuid
    ) {
        log.info("get task");
        Task task = new Task();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
          task = Task.loadByUuid(data, taskUuid);
        } finally {
            tbc.closeDB(data);
        }
        return task;
    }
    
    /**
     * save specific task
     * @param tbc
     * @param projectUuid
     * @param task
     * @return http status : 200, 201, 400, or 500
     */
    @POST
    @Path("/{account}/tasks/projects/{project}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object saveTask(
            @Form TimeBundleContext tbc,
            @PathParam("project") String projectUuid,
            Task task
    ) {
        log.info("saving task");
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            String errorMessage = task.check(data);
            if (errorMessage != null) {
                return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
            } else {
                try {
                    if (task.exists()) {
                        task.save(data);
                        
                        return Response.status(Response.Status.ACCEPTED).build();
                    } else {
                        task.create(data, projectUuid);
                        return Response.status(Response.Status.CREATED).build();
                    }
                } catch (OSecurityAccessException e) {
                    return Response.status(Response.Status.BAD_REQUEST).entity("you are not allowd to modified this task").build();
                }
            }
        } catch(Exception eee) {
            log.info("Can't save task", eee);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } finally {
            tbc.closeDB(data);
        }
    }
    

//***This services are using by task and project because of they are similar***

    /**
     * get time of task and subtask, first start date and last endDate
     * @param tbc
     * @param taskUuid
     * @return json object with three filds, json object with field sum = 0 if any 
     */
    @GET
    @Path("/{account}/tasks/{task}/infos")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getTaskinfos(
            @Form TimeBundleContext tbc,
            @PathParam("task") String taskUuid
    ) {
        log.info("get time from task");
        ODatabaseDocumentTx data = tbc.getDB().open();
        Task task = Task.loadByUuid(data, taskUuid);
        Map result = new HashMap();
        try {
             result = task.getTaskInfos(data);
        } catch (Exception eee) {
            log.error("can't get task infos", eee);
        }finally {
            tbc.closeDB(data);
        }
        return result ;
    }
    
    /**
     * remove specific task
     * @param tbc
     * @param taskUuid 
     */
    @DELETE
    @Path("/{account}/tasks/{task}")
    public void removeTask(
            @Form TimeBundleContext tbc,
            @PathParam("task") String taskUuid
        ) {
        log.info("remove task");
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            Task task = Task.loadByUuid(data, taskUuid);
            task.remove(data);
        } finally {
            data.close();
        }
    }
}


