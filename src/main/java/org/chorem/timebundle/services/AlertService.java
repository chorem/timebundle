/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.chorem.timebundle.services;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.record.impl.ODocument;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;
import org.chorem.timebundle.TimeBundleContext;
import org.chorem.timebundle.entities.AlertEmail;
import org.chorem.timebundle.entities.Project;
import org.chorem.timebundle.entities.Task;
import org.jboss.resteasy.annotations.Form;

/**
 * To active automatic alertEmail, please add a new cron task in your server 
 * like this : 0 6 * * * 
 * @author tollive 
 */
@Path("")
public class AlertService extends TimeBundleService {
    
     /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(AlertService.class);
    
    /**
     * get all alert from spécifique project
     * replace taskUuid value by taskName value
     * @param tbc
     * @param taskUuid 
     * @param isTemplate get template alert if true, alert if false
     * @return 
     */
    @GET
    @Path("/{account}/alerts/task/{task}/template/{all}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<AlertEmail> getAlerts(
            @Form TimeBundleContext tbc,
            @PathParam("task") String taskUuid,
            @PathParam("all") boolean isTemplate 
    ) {
        Collection<AlertEmail> result = new ArrayList<AlertEmail>();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            if("false".equals(taskUuid)) {
                result = AlertEmail.getDatabaseAlert(isTemplate, data);
            //we are looking for where alert are not attach to task or project
            } else {
                Project project = Project.loadByUuid(data, taskUuid);
               result = AlertEmail.getProjectAlert(
                       new ORecordId(project.getOid()), taskUuid, isTemplate, data);
            }//we are looking for alert with entity subtask uuid or entity uuid 
        } finally {
            data.close();
        }
        return result;
    }
    
    
    /**
     * get specific alert
     * @param tbc
     * @param alertUuid
     * @return 
     */
    @GET
    @Path("/{account}/alerts/{alert}/task")
    @Produces(MediaType.APPLICATION_JSON)
    public AlertEmail getAlert(
        @Form TimeBundleContext tbc,
        @PathParam("alert") String alertUuid
    ) {
        ODatabaseDocumentTx data = tbc.getDB().open();
        AlertEmail result = AlertEmail.loadByUuid(data, alertUuid);
        return result;
    }
    
    /**
     * save alert or create if not exist
     * @param tbc
     * @param taskUuid
     * @param alert
     * @return http status : 200, 201, 403 or 500 
     */
    @POST
    @Path("/{account}/alerts/task/{task}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object saveAlert(
            @Form TimeBundleContext tbc,
            @PathParam("task") String taskUuid,
            AlertEmail alert
    ) {
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            if (alert.exists()) {
                alert.save(data);
                return Response.status(Response.Status.ACCEPTED).build();
            } else {
                if (alert.create(data, taskUuid)) {
                    return Response.status(Response.Status.CREATED).build();
                } else {
                    return Response.status(Response.Status.FORBIDDEN).build();
                }
            }
        } catch(Exception eee) {
            log.info("Can't save alert", eee);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } finally {
            tbc.closeDB(data);
        }
    }
    
    /**
     * remove specific alert
     * @param tbc
     * @param alertUuid 
     */
    @DELETE
    @Path("/{account}/alerts/{alert}")
    public void removeAlert(
            @Form TimeBundleContext tbc,
            @PathParam("alert") String alertUuid
        ) {
        
        ODatabaseDocumentTx data = tbc.getDB().open();
        AlertEmail alert = AlertEmail.loadByUuid(data, alertUuid);
        alert.remove(data);
    }
    
    /**
     * check all not sent alert mail to see if we need to send it
     * @param tbc 
     * @return  map with number of alert not sent, sent and eventual error
     */
    
    /* FIXME comment gerer la connection et les droit des alertes ?*/
    @GET
    @Path("/{account}/alerts/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Map checkAlert(
        @Form TimeBundleContext tbc
    ) {
        log.info("check emails alerts and send it if needed");
        Map result = new HashMap();
        int alerts = 0, sent = 0;
        Collection<Map> fails = new ArrayList<Map>();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            String where = " sent = 'false'";
            Collection<ODocument> i = AlertEmail.getAlertsDocWhere(where, false, data);
                    
            for(ODocument alertDoc : i ) {
                alerts++;
                AlertEmail alert = DBDocument.convertTo(alertDoc, new AlertEmail());
                ODocument taskDoc = alertDoc.field("task");
                Task task = DBDocument.convertTo(taskDoc, new Task());
                Integer timeSpent =new Integer(task.getTaskInfos(data).get("sum").toString());
                if(timeSpent>= task.getDuration()*alert.getCondition()/100) {
                alert.prepareAlert(data, alertDoc);
                String value = alert.send(tbc);
                if(AlertEmail.SENT.equals(value)) {
                 sent++;   
                } else {
                    Map fail = new HashMap();
                    fail.put(alert.getSubject(), value);
                }
                alertDoc.reload();
                alertDoc.field(AlertEmail.SENT, true).save();
                }
            }
        } finally {
            data.close();
        }              
        result.put("alerts", alerts);
        result.put("sent", sent);
        result.put("fails", fails);
        return result;
    }
    
    
    @GET
    @Path("/{account}/alerts/{alert}/activate")
    public Object activeAlert(
        @Form TimeBundleContext tbc,
        @PathParam("alert") String alertUuid
    ) {
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            
            ODocument alertDoc = AlertEmail.loadDocByUuid(data, alertUuid);
            AlertEmail alert = DBDocument.convertTo(alertDoc, new AlertEmail());
            if(!alert.exists()) {
                return Response.status(Response.Status.NO_CONTENT);
            }
            alert.prepareAlert(data, alertDoc);
            alert.send(tbc);
        } catch (Exception eee) {
            log.error("error during alert activation", eee);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
            finally {
            data.close();
        }
        return Response.status(Response.Status.ACCEPTED);
    }
}
