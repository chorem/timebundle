package org.chorem.timebundle.services;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.ContributionResult;
import org.chorem.timebundle.TimeBundleContext;
import org.chorem.timebundle.entities.Contribution;
import org.chorem.timebundle.entities.Contributor;
import org.chorem.timebundle.entities.Project;
import org.chorem.timebundle.entities.Task;
import org.chorem.timebundle.entities.TaskContributions;
import org.jboss.resteasy.annotations.Form;

/**
 *  this class is the interface between contribution and web. This class is not allowed to 
 * to modify contributions directly and must only have method to get or set informations
 * about contributions
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Path("")
public class ContributionService extends TimeBundleService {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(ContributionService.class);

    /**
     * get specific contribution
     * @param tbc
     * @param uuid uuid of the contribution
     * @return contribution
     */
    @GET
    @Path("/{account}/contributions/{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Contribution getContribution(
            @Form TimeBundleContext tbc,
            @PathParam("uuid") String uuid
    ) {
        log.info("get contribution");
        Contribution result = null;
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            result = Contribution.loadByUuid(data, uuid);
        } finally {
            tbc.closeDB(data);
        }

        return result;
    }

    
    /**
     * save specific contribution
     * @param tbc
     * @param contribution
     * @return http status : 200, 401 or 500
     */
    @POST
    @Path("/{account}/contributions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object saveContribution(
            @Form TimeBundleContext tbc,
            Contribution contribution
    ) {
        log.info("save contribution");
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
                if (contribution.exists()) {
                    contribution.save(data);
                    } else {
                    contribution.setUid("tb-"+UUID.randomUUID().toString());
                    String errorMessage = contribution.check(data);
                    if (errorMessage==null) {
                        contribution.create(data, contribution.getContributorName(), contribution.getTaskUuid());
                    } else {
                        return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
                    }
                }
  //          }
        } catch(Exception eee) {
            log.info("Can't save contribution", eee);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } finally {
            tbc.closeDB(data);
        }
        return Response.status(Response.Status.ACCEPTED).build();
                
    }
    



    // FIXME permettre de parametre avec 2 dates start-end
    /**
     * get all validate or not validate contributions from database
     * @param tbc
     * @param status
     * @return 
     */
    @GET
    @Path("/{account}/contributions/validate/{status}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Contribution> getContributions(
            @Form TimeBundleContext tbc,
            @PathParam("status") String status
    ) {
        log.info("get contributions");
        Collection<Contribution> result = new ArrayList<Contribution>();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            Map conditions = new HashMap();
            conditions.put("status", status);
            result = Contribution.loadWhere(data, conditions);
        } finally {
            tbc.closeDB(data);
        }

        return result;
    }

    
    /**
     * get contribution with specified status from specific project 
     * this function replace contributorName value by emailContributor value 
     *          and TaskUuid value by taskName value
     * @param tbc
     * @param projectUuid
     * @param status 
     * @return 
     */
    @GET
    @Path("/{account}/contributions/projects/{project}/status/{status}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Contribution> getProjectContributions(
            @Form TimeBundleContext tbc,
            @PathParam("project") String projectUuid,
            @PathParam("status") String status
    ) {
        log.info("get project contributions");
        Collection<Contribution> result = new ArrayList<Contribution>();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            
            Map conditions = new HashMap();
            conditions.put("contributor.project.uuid", projectUuid);
            conditions.put("status", status);
            result = Contribution.loadWhere(data, conditions);
        } finally {
            tbc.closeDB(data);
        }
        return result;
    }
    
    /**
     * get contributions from specific task and sepecific status
     * this function replace contributorName value by emailContributor value
     * and TaskUuid value by taskName value
     * @param tbc
     * @param taskUuid
     * @param status
     * @return
     */
    @GET
    @Path("/{account}/contributions/tasks/{task}/validate/{status}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Contribution> getTaskContributions(
            @Form TimeBundleContext tbc,
            @PathParam("task") String taskUuid,
            @PathParam("status") String status
    ) {
        log.info("get task contributions");
        Collection<Contribution> result = new ArrayList<Contribution>();
        ODatabaseDocumentTx data = tbc.getDB().open();
        ODatabaseRecordThreadLocal.INSTANCE.set(data);
        try {
            Map conditions = new HashMap();
            conditions.put("task.uuid", taskUuid);
            conditions.put("status", status);
            result = Contribution.loadWhere(data, conditions);
        } finally {
            tbc.closeDB(data);
        }
        return result;
    }

    /**
     * get number of contributions with status 'toValid' for specific project
     * @param tbc
     * @param projectUuid
     * @return 
     */
    @GET
    @Path("/{account}/contributions/projects/{project}/tovalidate")
    @Produces(MediaType.APPLICATION_JSON)
    public Object CountContributionToValidate(
        @Form TimeBundleContext tbc,
            @PathParam("project") String projectUuid
    ) {
        log.info("get number of contributions to validate");
        Map result = new HashMap();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            Map conditions = new HashMap();
            conditions.put("contributor.project.uuid", projectUuid);
            conditions.put("status", "toValid");
            int count = Contribution.loadWhere(data, conditions).size();
            result.put("count", count);
        } finally {
            data.close();
        }
        return result;
    }
    
    
    /**
     * get validate or not validate contributions from specific contributor
     * @param tbc
     * @param contributorName
     * @param status
     * @return 
     */
    @GET
    @Path("/{account}/contributions/projects/{project}/contributors/{contributor}/validate/{status}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Contribution> getContributorContributions(
            @Form TimeBundleContext tbc,
            @PathParam("contributor") String contributorName,
            @PathParam("status") String status
    ) {
        log.info("get contributor contributions");
        Collection<Contribution> result = new ArrayList<Contribution>();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            Map conditions = new HashMap();
            conditions.put("contributor.name", contributorName);
            conditions.put("status", status);
            result = Contribution.loadWhere(data, conditions);
        } finally {
            tbc.closeDB(data);
        }
        return result;
    }

    /**
     * get validate or not validate contributions from specific contributor and specific task
     * @param tbc
     * @param taskUuid
     * @param contributorName
     * @param status
     * @return 
     */
    @GET
    @Path("/{account}/contributions/task/{task}/contributors/{contributor}/validate/{status}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Contribution> getContributorTaskContributions(
            @Form TimeBundleContext tbc,
            @PathParam("task") String taskUuid,
            @PathParam("contributor") String contributorName,
            @PathParam("status") String status
    ) {
        log.info("get contributor task contributions");
        Collection<Contribution> result = new ArrayList<Contribution>();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            Map conditions = new HashMap();
            conditions.put("contributor.name", contributorName);
            conditions.put("status", status);
            conditions.put("task.uuid", taskUuid);
            result = Contribution.loadWhere(data, conditions);
        } finally {
            tbc.closeDB(data);
        }
        return result;
    }
    
    
 
    @GET
    @Path("/{account}/contribute/{contributorToken}")
    @Produces(MediaType.APPLICATION_JSON)
    public Map getContributeInfo(
            @Form TimeBundleContext tbc,
            @PathParam("contributorToken") String contributorName
    ) {
        log.info("get contribute infos for wid");
        Map result = new HashMap();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            Contributor contributor = Contributor.loadByName(data, contributorName);
            Project project = Project.loadByContributor(data, contributorName);
            Map<String, Task> taskMap = new HashMap<String, Task>();
            if (project != null) {
                for (Task task : project.getSubTasks(data)) {
                    // use uuid task and not name to prevent error if task is renamed
                    if (!project.getUuid().equals(task.getUuid())){
                        taskMap.put(tbc.createContributeUrl(
                                contributorName, task.getUuid()), task);
                    }
                }
            }

            result.put("email", contributor.getEmail());
            result.put("project", project);
            result.put("tasks", taskMap);
            
        } finally {
            tbc.closeDB(data);
        }
        log.info("data send : "+ result);
        return result;
    }

    /**
     * Add support to send multiple contribution for multiple project and task
     * result is array of ContributionResult.
     * Client must check errorCount of each item if result is BAD_REQUEST (400)
     * in this case on or more ContributionResult have error.
     *
     * @param tbc
     * @param contributions
     * @return
     */
    @POST
    @Path("/{account}/contribute")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object saveContribute(
            @Form TimeBundleContext tbc,
            List<TaskContributions> contributions
    ) {
        log.info("save contributions from wid");
        List<ContributionResult> resultList =
                new ArrayList<ContributionResult>(contributions.size());
        Response.Status status = Response.Status.ACCEPTED;

        for (TaskContributions tc : contributions) {
            String [] url = tc.getUrl().split("/");
            String contributorName = url[url.length - 2];
            String taskUuid = url[url.length - 1];

            tbc.setContributorToken(contributorName);
        }
        return Response.status(status).entity(resultList).build();
    }



/**
     * Add contribution for task. 
     *
     * @param tbc
     * @param contributorName
     * @param taskUuid
     * @param contributions
     * @return
     */
    @POST
    @Path("/{account}/contribute/{contributorToken}/{taskName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object saveContributeTask(
            @Form TimeBundleContext tbc,
            @PathParam("contributorToken") String contributorName,
            @PathParam("taskName") String taskUuid,
            TaskContributions contributions
    ) {
        log.info("get contributions from wid for specific task");
        ContributionResult result = saveContributionForTask(tbc, contributorName, taskUuid, contributions);
        Response.Status status;
        if (result.getErrorCount() == 0) {
            status = Response.Status.ACCEPTED;
        } else {
            status = Response.Status.BAD_REQUEST;
        }

        return Response.status(status).entity(result).build();
    }

    protected ContributionResult saveContributionForTask(
            TimeBundleContext tbc,
            String contributorName,
            String taskUuid,
            TaskContributions contributions
    ) {
        ContributionResult result = new ContributionResult();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            data.begin();
            // remove old between startDate and endDate
            data.command(
                    new OCommandSQL(" delete from"
                            + " contributionStatus"
                            + "  where"
                            + " contributor.name = ? AND"
                            + " task.uuid = ? AND"
                            + " contribution.startDate >= ? AND"
                            + " contribution.endDate <= ? AND"
                            + " status <> 'blacklist'"))
                    .execute(contributorName,
                            taskUuid,
                            contributions.getStartDate(),
                            contributions.getEndDate());
            Contribution.removeAllUnusued(data);

            
            
//            FIXME: if task or contributor not loaded, send error with correct message
            Task task = Task.loadByUuid(data, taskUuid);
            Contributor contributor = Contributor.loadByName(data, contributorName);
            log.info(String.format("contribution from %s to task %s",
                    contributor.getEmail(),task.getName()));
            String mustValidate;
            if(contributor.isContributionValidation() || task.isContributionValidation()) {
                mustValidate = "toValid";
            } else {
                mustValidate = "validate";
            }
            
            // add contribution
            for (Contribution contribution : contributions.getPeriods()) {
                String uid = contribution.getUid();
                try {
                    String errorMsg = contribution.check(data);
                    if (errorMsg != null) {
                        result.addError(uid, errorMsg);
                    } else {
                        contribution.setStatus(mustValidate);
                        if (contribution.exists(data,"uid")) {
                            String status = contribution.allreadySent(data, taskUuid, contributorName);
                            if (status != null) {
                                if("blacklist".equals(status)) {
                                    // do nothing, contribution is not accepted
                                } else {
                                    ODocument contribDoc = Contribution.loadContributionDoc(data, uid, taskUuid, contributorName);
                                    contribution.setOid(contribDoc.field("rid").toString());
                                    // contribDoc is not really document, identity is bad so use rid
                                    contribution.save(data);
                                    result.addUpdate(uid);
                                }
                            } else { 
                                ODocument contribDoc = Contribution.
                                        loadBaseContributionDoc(data, uid);
                                contribution.addContributionLink(data, contribDoc,
                                        taskUuid, contributorName);
                                result.addUpdate(uid);
                            }
                        } else { 
                                contribution.create(data, contributorName, taskUuid);
                                result.addCreate(uid);
                        }
                    }
                } catch (Exception eee) {
                    log.warn("Can't store contribution", eee);
                    result.addError(uid, "Exception: " + eee.getMessage());
                }
            }
            if (result.getErrorCount() == 0) {
                data.commit();
            } else {
                data.rollback();
            }
        } finally {
            tbc.closeDB(data);
        }
        return result;
    }
/* *******************valid and remove contributions méthods*******************/
    
    @POST
    @Path("/{account}/contributions/status/{status}")
    public void ChangeStatusContributions(
        @Form TimeBundleContext tbc,
        @PathParam("status") String status,
        ArrayList<String> listUuid
    ) {
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            data.command(new OCommandSQL("update"
                    + " contributionStatus set status = ?"
                    + " where uuid in ?")).execute(status, listUuid);
        } finally {
            data.close();
        }
    }
    
    @DELETE
    @Path("/{account}/contributions")
    public void removeContributions(
            @Form TimeBundleContext tbc,
            ArrayList<String> uuids
    ) {
        log.info("remove contributions");
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
           /* Map conditions = new HashMap();
            conditions.put("uuid", uuids);
            Contribution.remove(data, conditions);
            */
            data.command(new OCommandSQL("delete"
                    + " from contributionStatus where uuid in ?")).execute(uuids);
            Contribution.removeAllUnusued(data);
        }  catch(Exception eee) {
            log.debug("error during deleting contributions", eee);
        } finally {
            data.close();
        }
    }
    
}
