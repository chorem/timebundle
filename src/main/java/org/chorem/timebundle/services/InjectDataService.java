package org.chorem.timebundle.services;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import java.net.URI;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;
import org.chorem.timebundle.TimeBundleContext;
import org.chorem.timebundle.entities.Account;
import org.chorem.timebundle.entities.Contribution;
import org.chorem.timebundle.entities.Contributor;
import org.chorem.timebundle.entities.Project;
import org.chorem.timebundle.entities.Task;
import org.chorem.timebundle.entities.User;
import org.jboss.resteasy.annotations.Form;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Path("")
public class InjectDataService extends TimeBundleService {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(InjectDataService.class);


    @GET
    @Path("/inject/{account:.*}")
    @Produces(MediaType.TEXT_HTML)
    public Object injectData(
            @Form TimeBundleContext tbc,
            @DefaultValue("codelutin") @PathParam("account") String accountName
    ) {
        log.info("Start injection for: " + accountName);
        DBDocument db = tbc.getDB();

        Account account = new Account();
        account.setName(accountName);
        account.setInfo("The account " + accountName);

        Set emails = new HashSet();
        emails.add("poussin@codelutin.com");
        User admin = new User();
        admin.setEmails(emails);
        admin.setName("admin");
        admin.setPassword("toto");
        admin.setInfo(accountName);

        account.create(db, admin);
        log.info("Database created, start to create information");
        db.setLogin(admin.getName());
        db.setPassword(admin.getPassword());
        ODatabaseDocumentTx data = db.open();
        try {
            data.begin();
            Project project = new Project();
            project.setName("TimeBundle");
            project.create(data);
            
            Task task = new Task();
            task.setName("Analyse");
            task.setInfo("some analyse");
            task.create(data, project.getName());
            log.info("first task created");

            task = new Task();
            task.setName("Dev");
            task.setInfo("some dev");
            task.create(data, project.getUuid());
            
            Contributor contributor = new Contributor();
            contributor.setStatus("ACTIVE");
            contributor.setEmail("poussin@codelutin.com");
            contributor.create(data, project);
            

            contributor = new Contributor();
            contributor.setStatus("ACTIVE");
            contributor.setEmail("bpoussin@free.fr");
            contributor.create(data, project);

            Contribution contribution = new Contribution();
            contribution.setUid("001");
            contribution.setAddTime(3600);
            ZonedDateTime date = ZonedDateTime.now();
            contribution.setStartDate(date.minusSeconds(contribution.getAddTime()));
            contribution.setEndDate(date);
            contribution.create(data, contributor.getName(), task.getName());
        
        } finally {
            
            db.shutdown(data);
        }
        log.info("Injection done");
        return Response.seeOther(URI.create(tbc.createUrl(account))).build();
    }
}
