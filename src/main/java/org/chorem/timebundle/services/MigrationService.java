/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.chorem.timebundle.services;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OSchemaProxy;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.metadata.security.ORole;
import com.orientechnologies.orient.core.metadata.security.ORule;
import com.orientechnologies.orient.core.metadata.security.OSecurity;
import com.orientechnologies.orient.core.metadata.security.OSecurityRole;
import com.orientechnologies.orient.core.metadata.security.OUser;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.core.storage.ORecordDuplicatedException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;
import static org.chorem.timebundle.DBDocument.EMAIL_ROLE;
import static org.chorem.timebundle.DBDocument.ROLE_ANONYMOUS;
import org.chorem.timebundle.TimeBundleContext;
import org.chorem.timebundle.entities.Contribution;
import org.chorem.timebundle.entities.Project;

/**
 * class used for database migration between versions. for all minor modifications,
 * update sub_version database and create new function migration.
 * @author ollive
 */
public class MigrationService extends TimeBundleService {

        /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(MigrationService.class);
    
    /**
     * 
     * @param tbc
     * @param data
     * @return 
     */
    static public int migrationValidation(
        TimeBundleContext tbc,
        ODatabaseDocumentTx data
    ) {
            try{
                if(data.getUser().hasRole(DBDocument.ROLE_CREATOR, true)) {
                    //only admin can update database
                    for (ODocument d :data.browseClass("metadata")){
                        int dbSubVersion;
                        try {
                            dbSubVersion = d.field("sub_version_db", OType.INTEGER);
                        } catch (NullPointerException eee){
                            dbSubVersion = 0;//if not exist, is version 0
                        }
                        if(DBDocument.DB_SUB_VERSION==dbSubVersion) {
                            return -1;
                        } else {
                            return dbSubVersion;
                        }
                    }
                }
            } catch (IllegalArgumentException e){
                return -2;
            }
        return -3;
    }
     
    /**
     * check version from database and update if needed.
     * @param data
     * @param tbc
     * @param version
     * @return 
     */
    static public Object migration(ODatabaseDocumentTx data, TimeBundleContext tbc, int version) {
        Map result;
            switch (version) {
                case 0: result =  migrationFromSV0(data);
                if(testMigration(result, data)) {
                    data.command(new OCommandSQL("update metadata set sub_version_db = ?")).execute(++version);
                } else {
                    return "migration not done with success"+result;
                }
                
                
                case 1: result = migrationFromSV1(data);
                if(testMigration(result, data)) {
                    data.command(new OCommandSQL("update metadata set sub_version_db = ?")).execute(++version);
                } else {
                    return "migration not done with success"+result;
                }
                
                case 2: result = migrationFromSV2(data);
                if(testMigration(result, data)) {
                    data.command(new OCommandSQL("update metadata set sub_version_db = ?")).execute(++version);
                } else {
                    return "migration not done with success"+result;
                }
                
                
                case 3: result = migrationFromSV3(data);
                if(testMigration(result, data)) {
                    data.command(new OCommandSQL("update metadata set sub_version_db = ?")).execute(++version);
                } else {
                    return "migration not done with success"+result;
                }
                
                default:
            }
        return null;
    }
    
    private static boolean testMigration(Map migration, ODatabaseDocumentTx data) {
        Set success = (Set) migration.get("success");
        if (success.size() !=(Integer) migration.get("actions")) {
                    log.error(migration);
                    return false;
                }
        return true;
    }

    
    /**
     * function do update database from 1.0 to 1.1
     * @param data
     * @return map with success and error
     */
    private static Map migrationFromSV0(ODatabaseDocumentTx data) {
        log.info("migration from sv0");
        Map result = new HashMap();
        result.put("actions", 6 ); // add number of actions to execute in migration service
        Set success = new HashSet();
        Set error = new HashSet();
        OSecurity sm = data.getMetadata().getSecurity();
        try {
            log.info("alerter role and user added");
            data.begin();
            sm.createRole("alerter", OSecurityRole.ALLOW_MODES.DENY_ALL_BUT);
            ORole alerter = sm.getRole("alerter");
            alerter.addRule(ORule.ResourceGeneric.DATABASE, null, ORole.PERMISSION_READ);
            alerter.addRule(ORule.ResourceGeneric.COMMAND, null, ORole.PERMISSION_READ);
            alerter.addRule(ORule.ResourceGeneric.CLASS,null, ORole.PERMISSION_READ);
            alerter.addRule(ORule.ResourceGeneric.CLASS,"AlertEmail", ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE);
            alerter.save();  
            
            data.commit();
            sm.createUser("alerter", "alerter", "alerter");
            success.add("alerter role added");
        } catch (ORecordDuplicatedException eee) {
            success.add("alerter role added");
            //alerter already exist, is success
        } catch (Exception eee) {
            data.rollback();
            log.error("can't create alerter role", eee);
            error.add("adding alerter role");
        }
        
        try {
            data.begin();
            log.info("validEmail role added");
            sm.createRole(EMAIL_ROLE, OSecurityRole.ALLOW_MODES.DENY_ALL_BUT);
            ORole validEmail = sm.getRole(EMAIL_ROLE);
            validEmail.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS,null, 
                    ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE+ORole.PERMISSION_DELETE);
            validEmail.addRule(ORule.ResourceGeneric.DATABASE, null, ORole.PERMISSION_READ);
            validEmail.addRule(ORule.ResourceGeneric.COMMAND, null, ORole.PERMISSION_READ);
            validEmail.addRule(ORule.ResourceGeneric.CLASS, "user", 
                    ORole.PERMISSION_DELETE+ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE);
            validEmail.addRule(ORule.ResourceGeneric.CLASS, "Contributor",
                    ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE);
            validEmail.save();
            data.commit();
            success.add("valid Email added");
        } catch (ORecordDuplicatedException eee) {
            success.add("valid Email added");
            //valid Email already exist, is success
        } catch (Exception eee) {
            data.rollback();
            log.error("can't create validEmail role", eee);
            error.add("adding validEmail role");
        }
        
        if(sm.getUser("anonymous").hasRole("writer", true)) {
            try{
                data.begin();
                log.info("correct rights anonymous");
                ORole anonymous = sm.getRole(ROLE_ANONYMOUS);
                anonymous.addRule(ORule.ResourceGeneric.DATABASE, null, ORole.PERMISSION_READ);
                anonymous.addRule(ORule.ResourceGeneric.CLASS,null, ORole.PERMISSION_READ);
                anonymous.addRule(ORule.ResourceGeneric.CLUSTER,"internal",ORole.PERMISSION_READ);
                anonymous.addRule(ORule.ResourceGeneric.CLUSTER,null, ORole.PERMISSION_READ);
                anonymous.addRule(ORule.ResourceGeneric.COMMAND,null, ORole.PERMISSION_READ);
                anonymous.addRule(ORule.ResourceGeneric.SYSTEM_CLUSTERS,null, ORole.PERMISSION_READ);
                anonymous.save();
                sm.getUser(ROLE_ANONYMOUS).removeRole("writer");
                data.commit();
                success.add("anonymous rights updated");
            } catch (Exception eee) {
                data.rollback();
                log.error("can't update anonymous role", eee);
                error.add("updating anonymous role");
            }
        } else {
            success.add("anonymous rights updated");
        }
        
        OSchemaProxy schema = data.getMetadata().getSchema();
        if(schema.getClass("Task").getProperty("pathToProject").getType()==OType.LINKSET) {
            try {
                log.info("changing pathtoproject from set to list");
                data.command(new OCommandSQL("alter property task.pathToProject type 'LINKLIST'")).execute();
                data.begin();
                for(ODocument doc : data.browseClass("Task")) {
                    ODocument parentDoc = doc.field("parentTask");
                    ArrayList<ORID> pathToProject = new ArrayList<ORID>();
                    while(parentDoc!=null) {
                        ArrayList<ORID> list = new ArrayList<ORID>();
                        list.add(parentDoc.getIdentity());
                        list.addAll(pathToProject);
                        pathToProject.addAll(list);
                        parentDoc = parentDoc.field("parentTask");
                    }
                    if(!"Project".equals(doc.getClassName())) {
                        pathToProject.add(doc.getIdentity());
                    }
                    doc.field("pathToProject", pathToProject).save();
                }
                success.add("pathToProject from set to list updated");
                data.commit();
            } catch (Exception eee) {
                data.rollback();
                log.error("can't change pathtoproject from linkset to linklist", eee);
                error.add("modifying pathtoproject type");
            } 
        } else {
            success.add("pathToProject from set to list updated");
        }
        
        if(!schema.existsClass("contributionStatus")) {
            try{
                log.info("model to store contribution changed");
                OClass contributionClass = schema.getClass(Contribution.CONTRIBUTION);
                OClass contributionStatusClass = schema.createClass("contributionStatus");
                contributionStatusClass.createProperty("uuid", OType.STRING);
                contributionStatusClass.createProperty("timezone", OType.INTEGER);
                contributionStatusClass.createProperty("createDate", OType.DATETIME);
                contributionStatusClass.createProperty("status", OType.STRING); //[toValid, validate, blacklist]
                contributionStatusClass.createProperty("contribution",
                        OType.LINK, schema.getClass(Contribution.CONTRIBUTION));
                contributionStatusClass.createProperty("contributor",
                        OType.LINK, schema.getClass("Contributor"));
                contributionStatusClass.createProperty("task",
                        OType.LINK, schema.getClass("Task"));
                data.command(new OCommandSQL("alter class ContributionStatus superclass +ORestricted")).execute();
                data.begin();
                for(ODocument doc : data.browseClass("Contribution")) {
                    Boolean status = doc.field("validate");
                    ODocument contribStatus = data.newInstance("contributionStatus");
                    contribStatus.field("timezone", (OType) doc.field("timezone"));
                    contribStatus.field("createDate", (OType) doc.field("createDate"));
                    contribStatus.field("status", status==true?"validate":"toValid");
                    contribStatus.field("contributor", (OType) doc.field("contributor"));
                    contribStatus.field("task", (OType) doc.field("task"));
                    contribStatus.field("contribution", doc);
                    contribStatus.field("uuid", (OType) doc.field("uuid"));
                    contribStatus.field("_allowRead", (OType) doc.field("_allowRead"));
                    contribStatus.field("_allow", (OType) doc.field("_allow"));
                    contribStatus.save();
                }
                data.command(new OCommandSQL("UPDATE Contribution REMOVE contributor")).execute();
                data.command(new OCommandSQL("UPDATE Contribution REMOVE uuid")).execute();
                data.command(new OCommandSQL("UPDATE Contribution REMOVE timezone")).execute();
                data.command(new OCommandSQL("UPDATE Contribution REMOVE task")).execute();
                data.command(new OCommandSQL("UPDATE Contribution REMOVE createDate")).execute();
                data.command(new OCommandSQL("UPDATE Contribution REMOVE validate")).execute();
                //remove all fields they now are in contribtuionStatus
                data.commit();
                contributionClass.dropProperty("contributor");
                contributionClass.dropProperty("uuid");
                contributionClass.dropProperty("timezone");
                contributionClass.dropProperty("task");
                contributionClass.dropProperty("createDate");
                contributionClass.dropProperty("validate");
                success.add("storage contribution updated");
            } catch (Exception eee) {
                data.rollback();
                error.add("updating contribution storage");
                log.fatal("can't update contribution storage", eee);
            }
        } else {
            success.add("storage contribution updated");
        }
        
        if(!schema.getClass("user").areIndexed("emails")) {
            try {
                schema.getClass("user").getProperty("emails").setLinkedType(OType.STRING);
                schema.getClass("user").getProperty("emailsToValidate").setLinkedType(OType.STRING);
                schema.getClass("user").getProperty("emails").createIndex(
                    OClass.INDEX_TYPE.UNIQUE, new ODocument().field("ignoreNullValues",true));
                success.add("new user.emails index added");
            } catch (Exception eee) {
                log.error("cant create index email for users", eee);
                error.add("adding index user.emails");
            }
            log.info("create new email index");
        } else {
             success.add("new user.emails index added");
        }
        
        sm.close(false);
        if(!error.isEmpty()) {
            result.put("error", error);
        }
        if(!success.isEmpty()) {
            result.put("success", success);
        }
        return result;
        
    }

     private static Map migrationFromSV1(ODatabaseDocumentTx data) {
         log.info("migration from sv1");
        Map result = new HashMap();
        result.put("actions", 2 ); // add number of actions to execute in migration service
        Set success = new HashSet();
        Set error = new HashSet();
        
        data.begin();
        try {
            ORole alertEmail = data.getMetadata().getSecurity().getRole(DBDocument.EMAIL_ROLE);
            alertEmail.addRule(ORule.ResourceGeneric.CLUSTER, null,
                    ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE+ORole.PERMISSION_DELETE);
            alertEmail.save();
            data.commit();
        } catch (Exception eee) {
            data.rollback();
           log.error("cant update valid Email role", eee);
           error.add("updating valid email role"); 
        }
        success.add("valid Email role updated");
        
        data.begin();
        try {
            ORole contribRole = data.getMetadata().getSecurity().getRole(DBDocument.CONTRIBUTOR_ROLE);
            contribRole.addRule(ORule.ResourceGeneric.CLASS, "Contribution",ORole.PERMISSION_ALL);
            contribRole.addRule(ORule.ResourceGeneric.CLASS, "ContributionStatus",ORole.PERMISSION_ALL);
            contribRole.save();
                        ORole writerRole = data.getMetadata().getSecurity().getRole(DBDocument.ROLE_WRITER);
            writerRole.addRule(ORule.ResourceGeneric.CLASS, "Contribution",ORole.PERMISSION_ALL);
            writerRole.addRule(ORule.ResourceGeneric.CLASS, "ContributionStatus",ORole.PERMISSION_ALL);
            writerRole.save();
        } catch (Exception eee) {
           log.error("cant update contributor and writer role", eee);
           error.add("updating contributor and writer role"); 
        }
        success.add("contributor and writer roles updated");
        
        
        
        if(!error.isEmpty()) {
            result.put("error", error);
        }
        if(!success.isEmpty()) {
            result.put("success", success);
        }
        return result;
        
    }   
     
     
        private static Map migrationFromSV2(ODatabaseDocumentTx data) {
         log.info("migration from sv2");
        Map result = new HashMap();
        result.put("actions", 2 ); // add number of actions to execute in migration service
        Set success = new HashSet();
        Set error = new HashSet();
        
        OSecurity security = data.getMetadata().getSecurity();
        data.begin();
        try {
             OUser alerter = security.getUser(DBDocument.ALERT_EMAIL);
            for(ODocument projectDoc : data.browseClass("Project")) {
                Project project = DBDocument.convertTo(projectDoc, new Project());
                alerter.addRole(project.getReaderRoleName(data));
            }
            alerter.save();
            data.commit();
        } catch (Exception eee) {
            data.rollback();
           log.error("cant add reader role to alerter", eee);
           error.add("adding reader roles to alerter"); 
        }
        success.add("reader role to alerter added");
        
        data.begin();
        try {
            ORole alerter = security.getRole(DBDocument.ALERT_EMAIL);
            alerter.addRule(ORule.ResourceGeneric.CLUSTER, "alertEmail", 
                    ORole.PERMISSION_READ+ORole.PERMISSION_UPDATE);
            ORole validEmail = security.getRole(EMAIL_ROLE);
            validEmail.addRule(ORule.ResourceGeneric.CLUSTER, "alertEmail", ORole.PERMISSION_DELETE);
            validEmail.addRule(ORule.ResourceGeneric.CLUSTER, null, ORole.PERMISSION_DELETE+ORole.PERMISSION_UPDATE);
        } catch (Exception eee) {
           log.error("cant update alerter and validEmail role", eee);
           error.add("updating alerter and validEmail role"); 
        }
        success.add("validEmail and alerter roles updated");
        
        
        
        if(!error.isEmpty()) {
            result.put("error", error);
        }
        if(!success.isEmpty()) {
            result.put("success", success);
        }
        return result;
        
    }   
     

        
    private static Map migrationFromSV3(ODatabaseDocumentTx data) {
       log.info("migration from sv3");
        Map result = new HashMap();
        result.put("actions", 1 ); // add number of actions to execute in migration service
        Set success = new HashSet();
        Set error = new HashSet();
        
        data.begin();
        try {
             for(ODocument doc :data.browseClass("Task", false)) {
                 ODocument parent = doc.field("parentTask");
                 Collection path = new ArrayList();
                 while(parent != null) {
                     Collection list = new ArrayList();
                     list.add(parent);
                     list.addAll(path);
                     path = list;
                     parent = parent.field("parentTask");
                 }
                 path.add(doc);
                 doc.field("pathToProject", path, OType.LINKSET);
                 doc.save();
             }
            data.commit();
        } catch (Exception eee) {
            data.rollback();
           log.error("can't correct pathToProject for some tasks", eee);
           error.add("correcting PathToProject for some tasks"); 
        }
        success.add("PathToProject for some tasks corrected");
        
        if(!error.isEmpty()) {
            result.put("error", error);
        }
        if(!success.isEmpty()) {
            result.put("success", success);
        }
        return result;
        
    }
    
}
