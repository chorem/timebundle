package org.chorem.timebundle.services;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.metadata.security.ORole;
import com.orientechnologies.orient.core.metadata.security.OSecurity;
import com.orientechnologies.orient.core.metadata.security.OSecurityRole;
import com.orientechnologies.orient.core.metadata.security.OUser;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;
import org.chorem.timebundle.TimeBundleContext;
import org.chorem.timebundle.TimeBundleNotFoundException;
import org.chorem.timebundle.entities.Account;
import org.chorem.timebundle.entities.Contribution;
import org.chorem.timebundle.entities.Contributor;
import org.chorem.timebundle.entities.User;
import org.jboss.resteasy.annotations.Form;


/**
 *  this class export and import JSON files to/from orientDB database
 * @author ollive
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Path("")
public class JSONService extends TimeBundleService {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(JSONService.class);


    static public Map export(
        @Form TimeBundleContext tbc,
        @PathParam("account") String accountName
    ) {
        Map result = null;
        DBDocument dbDoc = tbc.getDB();
        ODatabaseDocumentTx data = dbDoc.open();
        if (StringUtils.isNotBlank(accountName) && dbDoc.exists(accountName)) {

            try{
                for (ODocument d :data.browseClass("metadata")){
                    int dbVersion = d.field("version_db", OType.INTEGER);
                    switch(dbVersion){
                        case 1 :
                            result = exportFromV1(tbc,accountName/*, data*/);
                            break;
                        default :
                            throw new TimeBundleNotFoundException(String.format(
                                    "database version '%d' not supported", dbVersion));
                    }
                }
            } catch (IllegalArgumentException e){
                OrientGraph datagraph =new OrientGraph(data);
                result = exportFromV0(tbc,accountName);//, datagraph);
            }finally{
                dbDoc.shutdown(data);
            }

        } else {
            throw new TimeBundleNotFoundException(String.format("database %s does not exist", accountName));
        }
        return result;
    }


    /**
     * This class export from the first database version (graph version) to JSON file
     * @param tbc
     * @param accountName
     * @return the object generated with json modelisation
     */

    static public  Map exportFromV0(
        @Form TimeBundleContext tbc,
        @PathParam("account") String accountName//,
      //  OrientGraph data
    )
    {
        log.info("export begin");
        Map result = new LinkedHashMap(); // map to stock all the database
        DBDocument db = tbc.getDB();
        OrientGraph data = new OrientGraph(db.getUrl(accountName), db.getLogin(), db.getPassword());
        data.setUseLightweightEdges(true);
            try {

                Map metadata = new HashMap();
                metadata.put("name", accountName);
                metadata.put("version_db", 0);
                metadata.put("version_json", 1);
                result.put("metadata", metadata);

                Object r =  data.command(new OCommandSQL
                        ("select * from ouser where roles.name in 'creator'")).execute();
                log.info("export admin");
                for (Vertex v : (Iterable<Vertex>) r) {
                    result.put("admins", toMap(v,"name","emails", "emailsToValidate",
                            "info", "password", "logo"));
                }

                ArrayList<Map> projects = new ArrayList<Map>();

                OCommandSQL command = new OCommandSQL(
                                "select * from Project "
                                        );
                for (Vertex v : (Iterable<Vertex>) data.command(command).execute()) {
                    Map mapProject = toMap
                        (v, "logo", "public", "uuid", "name", "info",
                        "duration", "startDate", "endDate", "openToContribution", "contributionValidation");
                    //stock each project in a new map
                    log.info("export projects"+mapProject.get("name"));
                    projects.add(mapProject); //add all projects in an arraylist

                    ArrayList<Map> contributors = new ArrayList<Map>();
                    command = new OCommandSQL(
                                "select *"
                                        + " from Contributor "
                                        + " where"
                                        + " out('ContributeTo').name in ?"
                                        );


                    for (Vertex w : (Iterable<Vertex>) data.command(command).execute
                            (v.getProperty("name"))) {


                        Map mapContributor = toMap(w, "name",
                                "email", "info", "status", "contributionValidation",
                                "createDate");
                        contributors.add(mapContributor);
                        log.info("export projects"+mapContributor.get("name"));
                    }

                    mapProject.put("contributors", contributors.toArray());

                    ArrayList<Map> tasks = new ArrayList<Map>();
                    command = new OCommandSQL(
                                "select *"
                                        + " from Task "
                                        + " where"
                                        + " out('Parent').name in ?"
                                        );
                    for (Vertex x : (Iterable<Vertex>) data.command(command).execute(v.getProperty("name"))) {
                        Map mapTask = toMap(x,  "uuid", "name", "info",
                        "duration", "createDate", "endDate", "contributionValidation");
                        tasks.add(mapTask);

                        ArrayList<Map> contributions = new ArrayList<Map>();
                        command = new OCommandSQL(
                                "select *"
                                        + ", out('ContributionFrom').name as contributorName "
                                        + " from Contribution "
                                        + " where "
                                        + " out('ContributeTo').uuid in ? "
                                        + " unwind contributorName"
                        );
                       for (Vertex y : (Iterable<Vertex>) data.command(command).execute(x.getProperty("uuid"))) {
                            Map mapContribution = toMap
                                (y, "uuid", "uid", "timezone", "startDate", "endDate",
                                        "duration", "info", "validate", "contributorName");
                            contributions.add(mapContribution);
                            log.info("export projects"+mapContribution.get("name"));
                        }
                        mapTask.put("contributions", contributions.toArray());
                    }
                    mapProject.put("tasks", tasks.toArray());

                }
                result.put("projects", projects.toArray()); //add arraylist to first map
            } finally {
                data.shutdown();
            }

        log.info("export success");
        return result;

    }
    
/* **********************functions to export database from V1***************** */
    @GET
    @Path("/{account}/export")
    @Produces(MediaType.APPLICATION_JSON)
    static public  Map exportFromV1(
            @Form TimeBundleContext tbc,
            @PathParam("account") String accountName//,
        //    ODatabaseDocumentTx data
    )
    {
        log.info("export begin");
        DBDocument dbDoc = tbc.getDB();
        ODatabaseDocumentTx data = dbDoc.open();
       Map result = new LinkedHashMap(); // map to stock all the database
       try{
            for(ODocument metadata : data.browseClass("metadata")) {
                metadata.field("version_json", 1);
                result.put("metadata",toMap(metadata, "version_db", "name", "version_json", "sub_version_db"));

            }
            result.put("admins", toMap(data.getUser().getDocument(),"name","emails",
                    "emailsToValidate", "info", "password", "logo"));
            
            result.put("users", exportUsers(data)); // add users array to global map of export
            
            result.put("template_alerts", exportAlerts(null, data));
            
            result.put("contributions", exportContributions(data));
            
            ArrayList<Map> projects = new ArrayList<Map>();
            for(ODocument project: data.browseClass("Project")){
                Map mapProject = new HashMap();

                mapProject.putAll(toMap(project, "logo", "public", "uuid", "name", "info",
                            "duration", "startDate", "endDate", "openToContribution", "contributionValidation"));
                
                String projectUuid = project.field("uuid").toString();
                mapProject.put("contributors", exportContributors(projectUuid, data));
                mapProject.put("alerts", exportAlerts(projectUuid, data));
                mapProject.put("tasks", exportTasks(projectUuid, data));
                
                projects.add(mapProject);
            }
            result.put("projects", projects);
            log.info("export ended");

       } catch (Exception eee) {
         log.error("probleme during export", eee);
         result.put("error", eee);
       } finally {
           data.close();
       }
       return result;
}
    
    
    static protected Collection<Map> exportUsers(ODatabaseDocumentTx data) {
        ArrayList<Map> result = new ArrayList<Map>(); // create an array for users
        ODocument userAdmin = data.getUser().getDocument();
            for(ODocument user : data.browseClass("user")) { // get all users in ODocument
                if(user != userAdmin && !"anonymous".equals(user.field("name").toString())) {
                    //check if it's not admin or anonymous
                    Map mapUser = new HashMap(); // create a map for user
                    mapUser.putAll(toMap(user, "name", "emails", "emailsToValidate", "info", "password", "status"));
                    //put user simple fields in map
                    Collection<String> roles = new ArrayList(); // create an array for users's roles
                    try {
                        for(ODocument role :(Iterable<ODocument>) user.field("roles")) {
                            roles.add(role.field("name").toString()); // add all roles name in array
                        }
                        roles.remove("writer");
                        roles.remove("anonymous");
                        
                    } catch (NullPointerException eee) {
                        //if user have no role, intercept null pointer exception
                    }
                    mapUser.put("roles", roles); // add roles array to user map
                    result.add(mapUser); // add user to users list
                }
            }
        return result;
    }
    
    
    static protected Collection<Map> exportAlerts(String taskUuid, ODatabaseDocumentTx data) {
        String where;
        if(taskUuid == null) {
            where = " task is null ";
        } else {
            where = String.format("task.uuid = '%s'", taskUuid);
        }
        ArrayList<Map> result = new ArrayList<Map>();
        
        Iterable<ODocument> i = data.command(new OCommandSQL("select from AlertEmail where "
                + where))
                .execute(taskUuid); 
        for(ODocument alert: i ) {
            result.add(toMap(alert, "from", "to", "ruleTo", "bcc", "all",
                    "ruleBcc", "subject", "message", "sent", "condition", "replyTo", "uuid"));
        }
        return result;
    }
    
    static protected Collection<Map> exportContributors(String projectUuid, ODatabaseDocumentTx data) {
        ArrayList<Map> result = new ArrayList<Map>();
        Iterable<ODocument> i = data.command(new OCommandSQL("select *"
                     + " from contributor "
                     + " where"
                     + " project.uuid in ?")).execute(projectUuid);
        
        for(ODocument contr: i) {
        result.add(toMap(contr, "name",
                "email", "info", "status", "contributionValidation","createDate"));
        }
        return result;        
    }
    
    
    static protected Collection<Map> exportTasks(String projectUuid, ODatabaseDocumentTx data) {
        Iterable<ODocument> i = data.command(new OCommandSQL("select *"
                + " from task "
                + " where"
                + " parentTask.uuid in ?")).execute(projectUuid);
                
        ArrayList<Map> result = new ArrayList<Map>();
        for(ODocument task: i ){
            result.add(TaskExportV1(data,task));
        }
        return result;
    }
    
    static protected Collection<Map> exportContributions(ODatabaseDocumentTx data) {
        Iterable<ODocument> i = data.command(new OCommandSQL("select *, distinct(uid)"
                + " from contribution")).execute();
        ArrayList<Map> result = new ArrayList<Map>();
        for(ODocument contrib : i ) {
            result.add(toMap(contrib, "uid", "startDate", "endDate" ,"info", "addTime"));
        }
        return result;
    }
    
    static protected Collection<Map> exportContributionsStatus(String taskUuid, ODatabaseDocumentTx data) {
        Iterable<ODocument> i;
        if(data.getMetadata().getSchema().existsClass("contributionStatus")) {
            i = data.command(new OCommandSQL("select *,"
                    + " contributor.name as contributorName,"
                    + " contribution.uid as uid,"
                    + " validate as status"
                    + " from contributionStatus"
                    + " where"
                    + " task.uuid in ?")).execute(taskUuid);
        } else {
            //compatibility with version before 1.1
            i = data.command(new OCommandSQL("select *,"
                    + " contributor.name as contributorName"
                    + " from contribution"
                    + " where"
                    + " task.uuid in ?")).execute(taskUuid);
            
        }
        ArrayList<Map> result = new ArrayList<Map>();
        for(ODocument contribStatus: i )
        {
            result.add(toMap(contribStatus, "uuid", "uid", "timezone", "createDate",
                    "status", "contributorName"));
        }
        return result;
    }

    static protected Map TaskExportV1(
            ODatabaseDocumentTx data,
            ODocument task
    ) {
        Map mapTask = new HashMap();
        mapTask.putAll(toMap(task, "uuid", "name", "info",
            "duration", "startDate", "endDate", "contributionValidation"));
        
        String taskUuid = task.field("uuid");
        
        mapTask.put("contributions", exportContributionsStatus(taskUuid, data));
        
        mapTask.put("alerts", exportAlerts(taskUuid, data));
        
        Iterable<ODocument> i = data.command(new OCommandSQL("select *"
                + " from task "
                + " where"
                + " parentTask.uuid in ?")).execute(task.field("uuid"));
        
        ArrayList<Map> subtasks = new ArrayList<Map>();
        for(ODocument subtask: i ) {
            subtasks.add(TaskExportV1(data,subtask));
        }
        mapTask.put("tasks", subtasks);
        return mapTask;
    }
    
 /* *************************************************************************/

    static protected String getString(Map m, String s) {
        return (String)m.get(s);
    }

    static protected Map getMap(Map m, String s) {
        return (Map)m.get(s);
    }

    static protected List getList(Map m, String s) {
        return (List)m.get(s);
    }

    static protected Boolean getBoolean(Map m, String s) {
        return (Boolean)m.get(s);
    }
    
    static protected Set getSet(Map m, String s) {
        try{
            return new HashSet<Object>((Collection) m.get(s));
        } catch (NullPointerException ee) {
            return (Set) m.get(s);
        }
    }

    /**
     * import database from JSON String
     * @param tbc
     * @param source the json to import into the new database
     * @return
     */
    @POST
    @Path("/{account}/import")
    @Consumes(MediaType.APPLICATION_JSON)
    static public Object importToV1(
        @Form TimeBundleContext tbc,
         Map source)//,
    {
        DBDocument dbDoc = tbc.getDB();
        String accountName = getString(getMap(source, "metadata"), "name");
        if(dbDoc.exists(accountName)) {
            return Response.status(Response.Status.CONFLICT).entity("database already exist\n").build();
        }
        log.info("import begin");
        User admin = new User();

            Map adminMap = getMap(source,"admins"); // extract admin account
            log.info(adminMap);
            DBDocument db = tbc.getDB();
            admin.setName(getString(adminMap, "name"));
            admin.setPassword("password");// we temporarely use an other passord to can connect to database
            admin.setEmails(getSet(adminMap, "emails"));
            admin.setEmailsToValidate(getSet(adminMap, "emailsToValidate"));
            admin.setInfo(getString(adminMap, "info"));
            log.info("admin user created");

            
            Account account = new Account();
            account.setName(accountName);
            account.setInfo(getString(adminMap, "info"));
            Boolean isPublic = getBoolean(adminMap, "isPublic");
            account.setPublic( isPublic==null?false:isPublic);
            account.setAdmin(admin);

            if (!db.exists(account.getName())) {
                account.create(db, admin);
            } else {
                throw new TimeBundleNotFoundException("database already exist, please retry export");
            }

            db.setLogin(admin.getName());
            db.setPassword("password");
            ODatabaseDocumentTx data = dbDoc.open();

            log.info("db open : " + data);
            admin.setPassword(getString(adminMap,"password")); // change password to real password
            ODocument adminDoc = data.getUser().getDocument();
            adminDoc.fromMap(adminMap);
            adminDoc.save();
            OSecurity sm = data.getMetadata().getSecurity();
            
            List contributions = getList(source, "contributions");
            if(contributions!=null) {
                importContributions(data, contributions);
            }
            

            for(Object p :getList(source, "projects")){

                Map proj = (Map) p;
                ODocument projectDoc = data.newInstance("Project");
                
                List contributors = (List) proj.remove("contributors");
                // contributors, contributions, global alerts and task are not
                //save into projects, we remove them from the map
                List tasks = (List) proj.remove("tasks");
                List alerts = (List) proj.remove("alerts");
                
                ORole readerProject = sm.createRole("ProjectReader-"+proj.get("uuid"), OSecurityRole.ALLOW_MODES.DENY_ALL_BUT);
                ORole adminProject = sm.createRole("ProjectAdmin-"+proj.get("uuid"), OSecurityRole.ALLOW_MODES.DENY_ALL_BUT);
                sm.getUser(DBDocument.ALERT_EMAIL).addRole(readerProject);
                //add allow read projet for lalerter to can test condition_
                
                Set<OIdentifiable> readerSet = new HashSet();
                Set<OIdentifiable> adminSet = new HashSet();
                readerSet.add(readerProject.getIdentity());
                adminSet.add(adminProject.getIdentity());
                if(getBoolean(proj, "public")) {
                    readerSet.add(sm.getRole("anonymous").getIdentity());
                }
                projectDoc.fromMap(proj);
                projectDoc.field("_allow", adminSet,OType.LINKSET);
                projectDoc.field("_allowRead", readerSet,OType.LINKSET);

                projectDoc.save();
                Set<OIdentifiable> contributorSet = new HashSet();
                contributorSet.add(sm.getRole("contributor").getIdentity());
                contributorSet.add(readerProject.getIdentity());
                
                importContributors(data, contributors, adminSet, readerSet, contributorSet, projectDoc);
                
                if(source.containsKey("template_alerts")) {
                   importAlerts(data, alerts, projectDoc, adminSet);
                }
                taskImportV1(data, projectDoc,tasks, adminSet, readerSet);
            }
            
         if(source.containsKey("template_alerts")) {
             importAlerts(data,getList(source, "template_alerts"), null, null);
         }
        
        if(source.containsKey("users")) {
            for(Object u : getList(source, "users")) {
                importUsers(data, (Map) u);
            }
        }

        log.info("import success");
        dbDoc.shutdown(data);
        return Response.status(Response.Status.CREATED).build();
    }
    
    static protected void importUsers(
        ODatabaseDocumentTx data,
        Map user) {
            ODocument userDoc = data.newInstance("User");
            List<String> roles =(List) user.remove("roles");
            userDoc.fromMap(user);
            userDoc.field("status", "ACTIVE");
            userDoc.save();
            userDoc.reload();
            OUser userUser = new OUser(userDoc);
            userUser.addRole("writer");
            userUser.addRole("anonymous");
            for(String role : roles) {
                userUser.addRole(role);
            }
            for (String email :(Iterable<String>) userDoc.field("emails", OType.LINKSET)) {
                new OCommandSQL(String.format("update "
                    + " Contributor"
                    + " set user = %s"
                    + " where email = ?", userDoc.getIdentity())).execute(email);
                Iterable<ODocument> i = data.command(
                        new OCommandSQL("select"
                                + " expand(project)"
                                + " from contributor"
                                + " where email = ?")).execute(email);
                for (ODocument doc : i ) {
                    userUser.addRole("ProjectReader-"+doc.field("uuid").toString());
                }
            }
            userUser.save();
    }
    
    static protected void importContributors(
        ODatabaseDocumentTx data,
        List<Map> contributors,
        Set adminSet,
        Set readerSet,
        Set contributorSet,
        ODocument projectDoc) {
        
        for (Map contr : contributors) {

            ODocument contributorDoc = data.newInstance("contributor");
            
            contributorDoc.fromMap(contr);
            contributorDoc.field("password",contr.get("name"));// name and password are same for contributor
            contributorDoc.save();
            contributorDoc.reload();
            adminSet.add(contributorDoc.getIdentity());
            contributorDoc.field("_allow", adminSet,OType.LINKSET);
            contributorDoc.field("_allowRead", readerSet,OType.LINKSET);
            contributorDoc.field("project", projectDoc.getIdentity(), OType.LINK);
            contributorDoc.field("roles", contributorSet, OType.LINKSET);
            contributorDoc.save();
        }
    }
    
    static protected  void importAlerts(
            ODatabaseDocumentTx data,
            List<Map> alerts,
            ODocument taskDoc,
            Set adminSet ) {
        for (Map alert : alerts) {
                    
          ODocument alertDoc = data.newInstance("AlertEmail");
          OUser alerter = data.getMetadata().getSecurity().getUser(DBDocument.ALERT_EMAIL);
          HashSet<OIdentifiable> alertSet = new HashSet<OIdentifiable>();
          alertSet.add(alerter.getDocument());
          alertDoc.fromMap(alert);
           if(taskDoc!=null) {
               alertDoc.field("task", taskDoc.getIdentity(), OType.LINK);
           }
            alertDoc.field("_allow", adminSet,OType.LINKSET);
            alertDoc.field("_allowRead", alertSet,OType.LINKSET);
            alertDoc.field("_allowUpdate", alertSet,OType.LINKSET);
            
            alertDoc.save();
        }
    }
    
    static protected void importContributionsStatus(
        ODatabaseDocumentTx data,
        Set adminSet,
        Set readerSet,
        List<Map> contributionsStatus,
        ODocument taskDoc
    ) {
        for(Map contrib : contributionsStatus) {
            contrib.putIfAbsent("addTime", contrib.remove("duration"));
            contrib.putIfAbsent("contributorName",contrib.remove("nameContributor"));
            contrib.putIfAbsent("status",contrib.remove("validate"));
            contrib.replace("status", true, "validate");
            contrib.replace("status", false, "toValid");
            // to have compatibility with older version.
            
            ODocument contribDoc = data.newInstance("ContributionStatus");
            
            ODocument contributor = Contributor.loadDocByName(data,(String) contrib.get("contributorName"));
            contrib.remove("contributorName");
            OIdentifiable contributorRole = new OUser(contributor).getIdentity();
            adminSet.add(contributorRole);
          
            ODocument contribution = Contribution.loadBaseContributionDoc(data,(String) contrib.get("uid"));
            if(contribution!=null){
                contrib.remove("uid");
            //[version with contribution and contributionStatus] add link instead of uid
            } else {
                contribution = data.newInstance("contribution");
                contribution.field("uid", contrib.remove("uid"));
                contribution.field("startDate", contrib.remove("startDate"));
                contribution.field("endDate", contrib.remove("endDate"));
                contribution.field("info", contrib.remove("info"));
                contribution.field("addTime", contrib.remove("addTime"));
            }   //[version with only contribution] create contribution and remove informations from map
            
            contribution.field("_allow", adminSet, OType.LINKSET);
            contribution.field("_allowRead", readerSet,OType.LINKSET);
            contribution.save();
            
            contribDoc.fromMap(contrib);
            contribDoc.field("contribution", contribution, OType.LINK);
            contribDoc.field("_allow", adminSet,OType.LINKSET);
            contribDoc.field("_allowRead", readerSet,OType.LINKSET);
            contribDoc.field("task",taskDoc.getIdentity(), OType.LINK);
            contribDoc.field("contributor", contributor.getIdentity(), OType.LINK);
            contribDoc.save();
            adminSet.remove(contributorRole);
        }
    }
    
    static protected void importContributions(
        ODatabaseDocumentTx data, 
        List<Map> contributions
            ) {
        for(Map contrib : contributions) {
            ODocument contribDoc = data.newInstance("Contribution");
            contribDoc.fromMap(contrib).save();
        }
    }
    
    static protected void taskImportV1(
            ODatabaseDocumentTx data,
            ODocument parentDoc,
            List tasks,
            Set adminSet,
            Set readerSet
    )
    {
        for (Object t : tasks)
        {
            Map task = (Map) t;
            ODocument taskDoc = data.newInstance("Task");
            List subTasks =(List) task.remove("tasks");
            List contributions = (List) task.remove("contributions");
            List alerts = (List) task.remove("alerts");
                    
            
            taskDoc.fromMap(task);
            taskDoc.field("_allowRead", readerSet,OType.LINKSET);
            taskDoc.field("_allow", adminSet,OType.LINKSET);
            taskDoc.field("parentTask", parentDoc.getIdentity(), OType.LINK);
            taskDoc.save();
            taskDoc.reload();
            List<OIdentifiable> pathToProject = new ArrayList();  
            
             if(parentDoc.field("pathToProject")==null) {
                 pathToProject.add(parentDoc);
             } else {
                 pathToProject.addAll((Collection<ODocument>) parentDoc.field("pathToProject"));
             } 
            pathToProject.add(taskDoc);
            taskDoc.field("pathToProject", pathToProject, OType.LINKSET);
            taskDoc.save();
            if(contributions!=null) {
                importContributionsStatus(data, adminSet, readerSet, contributions, taskDoc);
            }
            if(alerts!=null) {
                importAlerts(data, alerts, taskDoc, adminSet);
            }
            if(subTasks!=null){
                taskImportV1(data, taskDoc,subTasks, adminSet, readerSet);
            }
        }
    }

    /**
     * intern function to get a map from a document. Map and document have same fields
     * @param d
     * @param fields
     * @return 
     */
    static protected Map toMap(ODocument d, String ... fields)
    {
        Map result = new HashMap();
        for (String field:fields){
            result.put(field,d.field(field));
        }
        return result;
    }


    /**
    * intern function to get a map from a vertex. Map and vertex have same fields
    * @param v
    * @param fields
    * @return map from vertex
    */
    static protected Map toMap(Vertex v, String ... fields)
    {
        Map result = new HashMap();
        for (String field:fields){
            result.put(field,v.getProperty(field));
        }
        return result;
    }
}
