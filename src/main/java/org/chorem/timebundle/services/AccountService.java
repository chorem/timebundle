package org.chorem.timebundle.services;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.exception.OSecurityAccessException;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;
import org.chorem.timebundle.TimeBundleContext;
import org.chorem.timebundle.TimeBundleNotFoundException;
import org.chorem.timebundle.entities.Account;
import org.chorem.timebundle.entities.User;
import org.jboss.resteasy.annotations.Form;

/**
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Path("")
public class AccountService extends TimeBundleService {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(AccountService.class);

    /**
     * get account and connect to database
     * @param tbc
     * @param accountName
     * @return 
     */
    @GET
    @Path("/{account}")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getAccount(
            @Form TimeBundleContext tbc,
            @PathParam("account") String accountName
    ) {


        DBDocument db = tbc.getDB();
        Account account;
        if (StringUtils.isNotBlank(accountName) && db.exists(accountName)) {
            ODatabaseDocumentTx data;
            try {
                data = db.open();
                
            } catch (OSecurityAccessException e) {
                if(e.getMessage().contains("User or password not valid for database")) {
                    return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
                } else {
                    return Response.status(Response.Status.FORBIDDEN).entity(e.getMessage()).build();
                }
            }

            int version = MigrationService.migrationValidation(tbc, data);
        switch(version) {
            case -3 : log.info("only admin can update database");
            break;
                
            case -2 : throw new TimeBundleNotFoundException("problem with database version");

            case -1 : break; // database has good version

            default :
                log.info("migration begin");
                Object result = MigrationService.migration(data, tbc, version);
                if (result == null) {
                    data.command(new OCommandSQL("update metadata set sub_version_db = ?")).execute(DBDocument.DB_SUB_VERSION);
                } else {
                    return Response.status(Response.Status.CONFLICT).entity("migration not done with success"+result).build();
                    
                }
            }

            try {
                account = Account.loadByName(data, accountName, tbc.getAuth().getLogin());
            } finally {
                tbc.closeDB(data);
            }
        } else {
            //account = new Account();
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.status(Response.Status.ACCEPTED).build();
    }
    
    /**
     * save contributor
     * @param tbc
     * @param accountName
     * @param account
     * @return http status : 200, 400 or 500
     */
    @POST
    @Path("/{account}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object saveAccount(
            @Form TimeBundleContext tbc,
            @PathParam("account") String accountName,
            Account account
    ) {
        if (account.exists()) {
            DBDocument db = tbc.getDB();
            ODatabaseDocumentTx data = db.open();
            try {
                account.save(data);
                return Response.status(Response.Status.ACCEPTED).build();
            } catch (Exception eee) {
                log.debug("Can't save Account", eee);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } finally {
                tbc.closeDB(data);
            }

        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    /**
     * create new account, creating new database.
     * @param tbc
     * @param account
     * @return http status 400, 500 or 202
     */
    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object createAccount(
            @Form TimeBundleContext tbc,
            Account account
    ) {
        DBDocument db = tbc.getDB();
        String errorMessage = account.check(db);

        User admin = account.getAdmin();
            if (errorMessage == null) {
                errorMessage = admin.check(null); // null because database don't exists
            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
            }
            if (errorMessage != null) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorMessage).build();
            } else {
                    account.create(db, admin);
                    return Response.status(Response.Status.CREATED).build();
            }
        }

}
