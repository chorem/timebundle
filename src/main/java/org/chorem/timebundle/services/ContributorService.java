package org.chorem.timebundle.services;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.exception.OSecurityAccessException;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import java.util.ArrayList;
import java.util.Collection;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.timebundle.DBDocument;
import org.chorem.timebundle.TimeBundleContext;
import org.chorem.timebundle.entities.Contribution;
import org.chorem.timebundle.entities.Contributor;
import org.chorem.timebundle.entities.Project;
import org.chorem.timebundle.entities.Task;
import org.jboss.resteasy.annotations.Form;

/**
 * interface to interact with contributor from web. thisclass can't modify directly 
 * database, she must use contributor class.
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Path("")
public class ContributorService extends TimeBundleService {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(ContributorService.class);

    /**
     * get all contributor for specific project
     * @param tbc
     * @param projectUuid
     * @return 
     */
    @GET
    @Path("/{account}/contributors/projects/{project}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Contributor> getContributors(
            @Form TimeBundleContext tbc,
            @PathParam("project") String projectUuid
    ) {
        log.info("get contributors");
        Collection<Contributor> result = new ArrayList<Contributor>();

        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            Iterable<ODocument> i = data.command(new OCommandSQL
                    ("select *,"
                            + " project.name as projectName,"
                            + " project.uuid as projectUuid"
                            + " from Contributor where project.uuid in ?"))
                    .execute(projectUuid); 
            for (ODocument doc : i ) {
                Contributor contributor = DBDocument.convertTo(doc, new Contributor());
                contributor.setContributeUrl(tbc.createContributeUrl(contributor, null)
                        + "#"+doc.field("projectName"));
                result.add(contributor);
            }
        } finally {
            tbc.closeDB(data);
        }

        return result;

    }
    
    
    

    /**
     * get specific contributor
     * @param tbc
     * @param contributorName
     * @return 
     */
    @GET
    @Path("/{account}/contributors/{contributor}/projects/{project}")
    @Produces(MediaType.APPLICATION_JSON)
    public Contributor getContributor(
            @Form TimeBundleContext tbc,
            @PathParam("contributor") String contributorName
    ) {
        log.info("get specific contributor");
        ODocument contribDoc ;
        Contributor contributor = new Contributor();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            contribDoc = Contributor.loadDocByName(data, contributorName);
            contributor = DBDocument.convertTo(contribDoc, new Contributor());
            if (contributor!=null) {
            contributor.setContributeUrl(tbc.createContributeUrl(contributor, null)
                + "#" + contribDoc.field("project.name"));
            }
        } finally {
            tbc.closeDB(data);
        }

        return contributor;
    }
    
    /**
     * get time contribution, first and last date of contribution for specific contributor
     * @param tbc
     * @param taskUuid
     * @param contributorName
     * @param validate
     * @return 
     */
    @GET
    @Path("/{account}/contributors/{contributor}/tasks/{task}/validate/{bool}/infos")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getContributorInfos(
            @Form TimeBundleContext tbc,
            @PathParam("task") String taskUuid,
            @PathParam("contributor") String contributorName,
            @PathParam("bool") String validate
    ) {
        log.info("get time from contributor");
        ODatabaseDocumentTx data = tbc.getDB().open();
        ORID taskOid = Task.loadDocByUuid(data, taskUuid).getIdentity();
        try{
            Iterable<ODocument> i = data.command(
                    new OCommandSQL(String.format("select"
                            + " sum(contribution.addTime), min(contribution.startDate), max(contribution.endDate), count(uuid)"
                            + " from"
                            + " (select expand(referredBy)"
                            + " from (find references"
                            + " (select expand(referredBy)"
                            + " from (find references %s [task]))))"
                            + " where contributor.name in ? AND"
                            + " status = ?"
                            , taskOid)))
                    .execute(contributorName, validate);
            for(ODocument obj : i ) {
                return obj.toJSON();
            }
        } finally {
            tbc.closeDB(data);
        }
        return null;
        // count return all time something
    }
    
    /**
     * get number of contributors who have already contributed
     * @param tbc
     * @param projectUuid
     * @return 
     */
    @GET
    @Path("/{account}/contributors/projects/{project}/active")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getActiveContributorsNumber(
            @Form TimeBundleContext tbc,
            @PathParam("project") String projectUuid
    ) {
        log.info("get actives contributors");
        ODatabaseDocumentTx data = tbc.getDB().open();
        try{
            ORID projectOid = Project.loadDocByUuid(data, projectUuid).getIdentity();
            Iterable<ODocument> i = data.command( 
                new OCommandSQL(String.format("select"
                        + " count(distinct(contributor))"
                        + " from contributionStatus where"
                        + " task in (select expand(referredBy)"
                        + " from (find references %s [task]))", projectOid)))
            .execute();
            
            for(ODocument obj : (Iterable<ODocument>) i) {
                return obj.field("count");
            }
        } finally {
            tbc.closeDB(data);
        }
                
       return 0;
    }
    
    /**
     * get contributor from specific task
     * @param tbc
     * @param taskUuid
     * @return 
     */
    @GET
    @Path("/{account}/contributors/tasks/{task}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Contributor> getTaskContributors(
            @Form TimeBundleContext tbc,
            @PathParam("task") String taskUuid
    ) {
        log.info("get task contributors");
        ODatabaseDocumentTx data =tbc.getDB().open();
        Collection<Contributor> result = new ArrayList<Contributor>();
        ORID taskOid = Task.loadDocByUuid(data, taskUuid).getIdentity();
        try{
            Iterable<ODocument> i = data.command(
                    new OCommandSQL(String.format("select"
                            + " expand(distinct(contributor))"
                            + " from contributionStatus"
                            + " where task in"
                            + " (select referredBy from"
                            + " (find references %s [task]))", taskOid)))
                    .execute();
            
            for(ODocument doc:(Iterable<ODocument>) i) {
                Contributor contributor = DBDocument.convertTo(doc, new Contributor());
            contributor.setContributeUrl(tbc.createContributeUrl(contributor, null)
                + "#" + doc.field("project.name"));
            result.add(contributor);
            }
            
        } finally {
            tbc.closeDB(data);
        }
        return result;
    }
    
    @DELETE
    @Path("/{account}/contributor/{contributor}/contributions/{bool}")
    public void removeContributor(
        @Form TimeBundleContext tbc,
        @PathParam("contributor") String contributorName,
        @PathParam("bool") boolean contributions
    ) {
        log.info("remove contributor");
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            Contributor contributor = Contributor.loadByName(data, contributorName);
            if(contributions) {
                data.command(new OCommandSQL("delete"
                        + " from contributionStatus"
                        + " where contributor.name = ?")).execute(contributorName);
            } else {
               data.command(new OCommandSQL("update"
                        + " contributionStatus"
                       + " set contributor = null"
                        + " where contributor.name = ?")).execute(contributorName); 
            }
            Contribution.removeAllUnusued(data);
            contributor.remove(data);
        } finally {
            data.close();
        }
    }
    
    /**
     * save specific contributor
     * @param tbc
     * @param projectUuid
     * @param contributor
     * @return http status : 200, 201, 400, 301 or 500
     */
    @POST
    @Path("/{account}/contributors/projects/{project}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object saveContributorHTML(
            @Form TimeBundleContext tbc,
            @PathParam("project") String projectUuid,
            Contributor contributor
    ) {
        log.info("save contributor");
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            String errorMessage = contributor.check(data);
            if (errorMessage != null) {
                log.info(errorMessage);
                return Response.status(Response.Status.BAD_REQUEST).encoding(errorMessage).build();
            } else {
                try {
                    if (contributor.exists()) {
                        contributor.save(data);

                        return Response.status(Response.Status.ACCEPTED).build();
                    } else {
                        // get project to create url with #<project name>
                        Project project = Project.loadByUuid(data, projectUuid);

                        contributor.create(data, project);
                        String url = tbc.createContributeUrl(contributor, null) + "#" + project.getName();
                        contributor.setContributeUrl(url);
                        return Response.status(Response.Status.CREATED).entity(contributor).build();
                    }
                } catch (OSecurityAccessException e) {
                    return Response.status(Response.Status.UNAUTHORIZED).entity("you are not allowd to modified this contributor").build();
                }
            }
        } catch(Exception eee) {
            log.info("Can't save contributor", eee);
            return Response.status(Response.Status.BAD_REQUEST).build();
        } finally {
            tbc.closeDB(data);
        }
    }

}
