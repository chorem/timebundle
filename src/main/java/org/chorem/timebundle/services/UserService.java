/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.chorem.timebundle.services;

/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.metadata.security.OSecurityUser;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.core.storage.ORecordDuplicatedException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.Cookie;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.chorem.timebundle.Authentication;
import static org.chorem.timebundle.Authentication.COOKIE_MAX_AGE;
import org.chorem.timebundle.AuthenticationParam;
import org.chorem.timebundle.DBDocument;
import org.chorem.timebundle.TimeBundleContext;
import org.chorem.timebundle.entities.Contributor;
import org.chorem.timebundle.entities.Project;
import org.chorem.timebundle.entities.User;
import org.jboss.resteasy.annotations.Form;

/**
 * interface to interact with users from web.
 * this class has only method to get informations about user or to modify user.
 * this class can't modify directly database, she must use user class.
 * @author ollive
 */
@Path("")
public class UserService extends TimeBundleService {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(UserService.class);

    /**
     * save user into database
     * @param tbc
     * @param user
     * @return 
     */
    @POST
    @Path("/{account}/users")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object saveUser(
            @Form TimeBundleContext tbc,
            User user
    ) {

        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            if (user.exists()) {
                log.info("saving user");
                if(user.save(data, tbc)) {
                    return Response.status(Response.Status.ACCEPTED).build();
                } else {
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("email not sent").build();
                }
            } else {
                log.info("creating user");
                String errorMessage = user.check(data);
                if (errorMessage != null) {
                    log.info("errormessage : "+ errorMessage);
                    return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage).build();
                } else {
                    if(user.create(data, tbc)) {
                        return Response.status(Response.Status.CREATED).entity("user created").build();   
                    } else {
                      return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("email not sent").build();
                    }
                }
            }
    } catch (ORecordDuplicatedException eee) {
            data.rollback();
            log.info("user not saved, email already used", eee) ;
          return Response.status(Response.Status.CONFLICT).entity("email already used").build();
        } catch (Exception eee) {
        log.error("Cant save user", eee);
            data.rollback();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } finally {
            tbc.closeDB(data);
        }
    }
    
    /**
     * get url to valid email from user
     * @param tbc
     * @param userName
     * @param email
     * @return 
     */
    @GET
    @Path("/{account}/users/{user}/email/{email}")
    public Object validEmailUser(
        @Form TimeBundleContext tbc,
        @PathParam("user") String userName,
        @PathParam("email") String email
    ) {
        log.info("valid email user");
        ODatabaseDocumentTx data = tbc.getDB().open();
        try{
            data.begin();
            ODocument userValidation = data.getUser().getDocument();
            ODocument userDoc = User.loadDocByEmailToV(data, email);
            userValidation.field("emails", "").save();
            //to disable conflict with duplicate email adress in index
            User user = DBDocument.convertTo(userDoc,new User());
            Set emails = user.getEmails()==null?new HashSet():user.getEmails();
            emails.add(email);
            userDoc.field(User.EMAILS, emails);
            Set emailsToValidate = user.getEmailsToValidate();
            emailsToValidate.remove(email);
            userDoc.field(User.EMAILSTOVALIDATE, emailsToValidate);
            userDoc.field(User.STATUS, "ACTIVE");
            userDoc.save();
            user.findContributors(data, email, userDoc);
            userValidation.delete().save();
            data.commit();
        } catch (Exception eee) {
            log.error("can't valid email", eee);
            data.rollback();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }finally {
            data.close();
        }
        return Response.status(Response.Status.ACCEPTED);
    }
    
   
    @DELETE
    @Path("/{account}/users/{user}")
    public void removeUser(
        @Form TimeBundleContext tbc,
        @PathParam("user") String userName
    ) {
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            User user = User.loadByName(data, userName);
            user.delete(data, userName);
        } finally {
            data.close();
        }
    }
    
    @GET
    @Path("/{account}/users")
    public Collection<User> getUsers(
        @Form TimeBundleContext tbc
    ) {
        log.info("get users");
        Collection<User> result = new ArrayList<User>();
        ODatabaseDocumentTx data = tbc.getDB().open();
        Iterable<ODocument> i = data.command(
                new OCommandSQL("select *"
                        + " from user where userToValid is null"))
                .execute(); 
        for(ODocument doc : i ) {
            if(doc.field("name")!=data.getUser().getName()) {
                doc.removeField("password");
            }
            User user = DBDocument.convertTo(doc, new User());
            user.setRoles(User.getRoles(data, user.getName()));
            result.add(user);
        }
        return result;
    }
    
    /**
     * create a new contributor attached to user
     * @param tbc
     * @param userName
     * @param projectUuid
     * @param email
     * @return 
     */
    @GET
    @Path("/{account}/users/{user}/project/{project}/contributor/{email}")
    @Produces(MediaType.APPLICATION_JSON)
    public Object addContributor(
        @Form TimeBundleContext tbc,
        @PathParam("user") String userName,
        @PathParam("project") String projectUuid,
        @PathParam("email") String email
    ) {
        log.info("add contributor to user");

        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            User user = User.loadByName(data, userName);
            Project project = Project.loadByUuid(data, projectUuid);

                Contributor contributor = new Contributor();
                contributor.setEmail(email);
                contributor.setUserName(user.getName());
                contributor.setContributionValidation(true);
                contributor.create(data, project);

                String url = tbc.createContributeUrl(contributor, null) + "#" + project.getName();
                contributor.setContributeUrl(url);

                HashMap result = new HashMap();
                result.put("urlContribute", url);
                return Response.status(Response.Status.CREATED).entity(result).build();
        } finally {
            data.close();
        }
    }        
    
    /**
     * check if user is contributor of the project. We search is there is a linked contributor in this project
     * @param tbc
     * @param userName
     * @param projectUuid
     * @return map with url to contribute to this project else map with emails of contributor
     */
    @GET
    @Path("/{account}/users/{user}/project/{project}")
    @Produces(MediaType.APPLICATION_JSON)
    public Object isContributor(
        @Form TimeBundleContext tbc,
        @PathParam("user") String userName,
        @PathParam("project") String projectUuid    
            ) {
        log.info("check if user is contributor");
        HashMap result = new HashMap();
        ODatabaseDocumentTx data = tbc.getDB().open();
        Iterable<ODocument> contributor = (Iterable<ODocument>) data.command(new OCommandSQL(
            "select name, project.name from Contributor where user.name = ? AND project.uuid in ?"))
                .execute(userName, projectUuid);
        for (ODocument doc : contributor) {
            String contribUuid = doc.field("name").toString();
            String projectName = doc.field("project").toString();
            String contributeUrl = tbc.createContributeUrl(contribUuid, "") + "#" + projectName;
            result.put("urlContribute" ,contributeUrl);
            return result; // if user is already contributor
        }
        //else, set list with emails adress
        Collection emails = User.loadDocByName(data, userName).field("emails");
        result.put("emails", emails);
        
        return result;
    }
    
    /**
     * get project with admin role for this user
     * @param tbc
     * @return map with project name as key and role name as value
     */
    @GET
    @Path("/{account}/users/admin")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Map> getAdminProjects(
            @Form TimeBundleContext tbc
    ) {
        log.info("get admin project for user");
        Map<String, Map> result = new HashMap();
        ODatabaseDocumentTx data = tbc.getDB().open();
        try {
            Iterable<ODocument> list = new OCommandSQL("select"
                    + " name, uuid, _allow.name"
                    + " from"
                    + " (select"
                    + " name, _allow, uuid"
                    + " from project"
                    + " unwind _allow)"
                    + " where _allow.name <> 'admin'").execute();
            if(data.getUser().hasRole(DBDocument.ROLE_CREATOR, false)) {
                Map roles = new HashMap();
                    roles.put("admin", DBDocument.ROLE_CREATOR);
                    roles.put("reader", DBDocument.ROLE_CREATOR);
                    result.put("superAdmin", roles );
                for (ODocument doc : list) {
                    Project project = DBDocument.convertTo(doc, new Project());
                    roles = new HashMap();
                    roles.put("admin", project.getAdminRoleName(data));
                    roles.put("reader", project.getReaderRoleName(data));
                    result.put((String) doc.field("name"),roles);
                }
            } else {
                OSecurityUser user = data.getUser();
                for (ODocument doc : list) {
                    if(user.hasRole((String) doc.field("_allow"), true)) {
                        Project project = DBDocument.convertTo(doc, new Project());
                        Map roles = new HashMap();
                        roles.put("admin", project.getAdminRoleName(data));
                        roles.put("reader", project.getReaderRoleName(data));
                        result.put((String) doc.field("name"),roles);
                    }                    
                }
            }
        }finally {
            tbc.closeDB(data);
        }
        return result;
    } 
    
    /**
     * check if user has admin role for his project
     * @param tbc
     * @param ProjectUuid
     * @return 
     */
    @GET
    @Path("/{account}/users/project/{project}/admin")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean isAdmin(
        @Form TimeBundleContext tbc,
        @PathParam("project") String ProjectUuid
    ) {
        log.info("check if user is admin");
        ODatabaseDocumentTx data = tbc.getDB().open();
        try{
            OSecurityUser user = data.getUser();
            if(user.hasRole(DBDocument.ROLE_CREATOR, true)) {
                return true;
            }
            Collection<ODocument> i = data.command(
                    new OCommandSQL(" select *"
                            + " from project"
                            + " where"
                            + " _allow in (select roles from user where name in ?)"
                            + " AND uuid = ?"))
                    .execute(user.getName(), ProjectUuid); 
            
            return !i.isEmpty();
        } finally {
            data.close();
        }   
    }
    
    @POST
    @Path("/{account}/users/change")
    public Object changePassword(
            @Form TimeBundleContext tbc,
            List<String> newPassword) {
        
        log.info("change user password");
        ODatabaseDocumentTx data = tbc.getDB().open();
        try{
            data.begin();
            ODocument userDoc = data.getUser().getDocument();
            String userName = userDoc.field("name");
            String password = newPassword.get(0);
            userDoc.field("password", password).save();
            data.commit();
            //----------------------------------------------
            tbc.getAuth().setAuth(null);
            tbc.getAuth().setBasicAuth(new AuthenticationParam(userName, password));
            Cookie authCookie = new Cookie("tb-auth",Authentication.crypt
                    .encrypt("Basic "+Base64.getEncoder().encodeToString((userName+":"+password).getBytes())));
            authCookie.setMaxAge(COOKIE_MAX_AGE);
            authCookie.setPath("/");
            tbc.getAuth().getResponse().addCookie(authCookie);
            //change cookie to keep connexion FIXME change patch to intercept all 
            //exit request and create cookie at this time
        } catch(Exception eee) {
            log.error("can't change password", eee);
            data.rollback();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } finally {
            data.close();
        }

                   
        return Response.status(Response.Status.ACCEPTED).build();
    }
    
            
    /**
     * method to reinit password of specific user
     * @param tbc
     * @param emailAdress email to send email with new password
     * @param userName user to change password
     * @return 
     */
    @GET
    @Path("/{account}/users/{user}/reinit/{email}")
    public Object reinitPassword(
        @Form TimeBundleContext tbc,
        @PathParam("email") String emailAdress,
        @PathParam("user") String userName) {
        log.info("reinit password for user with email "+ emailAdress);
        
        ODatabaseDocumentTx data = tbc.getDB().open();
        OSecurityUser MyUser = data.getUser();
        OSecurityUser userDoc = data.getMetadata().getSecurity().getUser(userName);
        if(MyUser.hasRole(DBDocument.ROLE_CREATOR, true)) {
            try {
                String password = RandomStringUtils.random(
                        10, 0, 0, true, true, null, new SecureRandom());
                String subject = "password reinitialisation";
                String message = "your password have been modified.\n"
                        + "new password : "
                        + password
                        + "\n"
                        + "Please connect you with this password and modify it quickly";
                
                Set to = new HashSet();
                to.add(emailAdress);
                SimpleEmail email = tbc.configEmail(subject, message, to, null);
                email.send();
                log.info("new password : "+password);
                userDoc.setPassword(password);
                //userDoc.getDocument().save();
            } catch (EmailException ex) {
                log.error("can't send email"+ex);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity("can't send email").build();
            }
        } else {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        return Response.status(Response.Status.ACCEPTED).build();
    }

}


