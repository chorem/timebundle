/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


define("js/tb-utils", () => {
  class TBUtils {
    createCookie(account, login, passwd) {
      var days = 356;
      var date = new Date(Date.now() + (days*24*60*60*1000));
      var expires = "; expires=" + date.toGMTString();
      document.cookie = "tb-account" + "=" + account + expires;
      document.cookie = "tb-login" + "=" + login + expires;
      let test = "tb-auth" + "=" + "Basic " + btoa(login + ":" + passwd) + expires + "; path=/api/";
      document.cookie = test;
    }
    doLogout() {
      document.cookie = "tb-auth=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
    }
    uid() {
      this._uid = this._uid || Date.now();
      return this._uid++;
    }
    sortBy(x,a,b) {
      return a[x] < b[x]===true?-1:1 ;
    }
    /**
    * remove element from collection select by field
    * @param col : collection to remove
    * @param field : name of field to find
    * @param value : value of field to find
    * @returns collection without element
    */
    rmFromCollection(col, field, value) {
      col.splice(col.findIndex((el) => {
        return el[field] === value;
      }), 1);
    }


    /**
    * convert time
    * @param {type} time in second
    * @returns time in days
    */
    stod(time) {
      return (time / 3600 / 24).toFixed(2);
    }
    /**
    * convert time
    * @param {type} time in second
    * @returns time in hours
    */
    stoh(time) {
      if (time) {
        return (time / 3600).toFixed(2);
      }
      return 0;
    }
    /**
    * convert time
    * @param {type} time in hours
    * @returns time in second
    */
    htos(time) {
      if (time) {
        return (time * 3600);
      }
      return 0;
    }

    ISOtos(date) {
      if (date) {
        return new Date(date).toLocaleDateString();
      }
      return null;
    }
    toISOTZString(date) {
      let pad = function(num) {
        let norm = Math.abs(Math.floor(num));
        return (norm < 10 ? '0' : '') + norm;
      };
      let d = new Date(date);
      let tz = -d.getTimezoneOffset();
      let result = d.toISOString().replace(/\.\d\d\dZ?$/, function() {
        return (tz > 0 ? "+" : "-") + pad(tz/60) + ":" + pad(tz%60);
      });
      return result;
    }
    fromISOTZString(date) {
      if (date !=null){
        let d = new Date(date);
        d = new Date(d.getTime()-d.getTimezoneOffset()*60000);
        let result = d.toISOString().replace(/(T.*Z$)/, "");
        return result;
      }
      return null;
    };
  };
  const tbu = new TBUtils();
  return tbu;
});
