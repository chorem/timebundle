/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
define("js/tb-log", () => {

    class Log {

        log() {
            console.log.apply(console, arguments);
            return Promise.reject(Array.prototype.join.call(arguments, ", "));
        }

        err() {
            console.error.apply(console, arguments);
            return Promise.reject(Array.prototype.join.call(arguments, ", "));
        }
    }

    return new Log();
});
