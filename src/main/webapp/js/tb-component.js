/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
define("js/tb-component", ["js/tb-utils"], (utils) => {

  return {
    init: function() {
      this.loaded = true;
      // update must be done at end, but mixin must be load at begin
      setTimeout(() => {
        this.update();
      }, 4);
    },

    shouldUpdate: function() {
      return this.isMounted;
    },

    formToMap: function(form) {
      let result = {};
      Array.prototype.forEach.call(form.elements, (e) => {
        if (e.name) {
          let storeValue;
          if(e.parentElement.type == "fieldset") {
            storeValue = result[e.parentElement.name] || result;
          } else {
            storeValue = result;
          }
          if (e.name.endsWith("[]") ){
            if (e.type !== "checkbox" || (e.checked !== false && e.type === "checkbox")) {
            storeValue[e.name.slice(0,-2)] = result[e.name.slice(0,-2)] || [];
            try {
              storeValue[e.name.slice(0,-2)].push(JSON.parse(e.value));
            } catch (eee) {
              storeValue[e.name.slice(0,-2)].push(e.value);
            };
          }
        } else if (e.type === "checkbox") {
            if (e.checked) {
              storeValue[e.name] = e.value || true;
            }
          } else if (e.type === "fieldset") {
              storeValue[e.name] = new Object();
          } else if (e.type === "date") {
            if (e.value!=="") {
              storeValue[e.name] = utils.toISOTZString(e.value+"T12:00");
            }
          } else if (e.type === "datetime-local") {
            if (e.value!=="") {
              storeValue[e.name] = utils.toISOTZString(e.value);
            }
          } else if (e.type === "radio") {
            Array.prototype.forEach.call(form.elements[e.name], function(r) {
              if (r.checked) {
                storeValue[e.name] = r.value;
              }
            });
          } else if (e.tagName === "SELECT") {
            if (e.multiple) {
              var list = storeValue[e.name] = [];
              Array.prototype.forEach.call(e.selectedOptions, function(o) {
                list.push(o.value);
              });
            } else {
              storeValue[e.name] = e.value;
            }
          } else if (e.value) {
            storeValue[e.name] = e.value;
          }
        }
      });
      return  result;
    },

    fillForm: function(form, context, defaults = {}) {
      let formData = context._formData || context;

      Array.prototype.forEach.call(form.elements, (e) => {
        if (e.name) {
          let value =
          formData[e.name] || formData["_" + e.name] ||
          context[e.name] || context["_" + e.name] ||
          defaults[e.name] || "";
          if (e.type === "checkbox") {
            e.checked = !!value;
          } else if (e.type === "datetime-local") {
            let d = new Date(value);
            e.value = new Date(+d-d.getTimezoneOffset()*60*1000).toISOString().replace("Z", "");
          } else if (e.type === "radio") {
            Array.prototype.forEach.call(form.elements[e.name], function(r) {
              if (r.value === value) {
                r.checked = true;
              }
            });
          } else if (e.tagName === "SELECT") {
            if (e.multiple) {
              let values = new Set(Array.isArray(value) ? value : [value]);
              Array.prototype.forEach.call(e.options, function(o) {
                o.selected = values.has(o.value);
              });
            } else {
              e.value = value;
            }
          } else {
            e.value = value;
          }
        }
      });
    }

  }
});
