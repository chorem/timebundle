/*-
 * #%L
 * TimeBundle
 * %%
 * Copyright (C) 2016 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
/**
* event:
* - loaded
* - loading
*/
define("js/tb-storage", ["js/tb-log", "js/tb-utils", "js/tb-bus"], (log, utils, bus) => {

    class TBStorage {

        getAccountName() {
            let key = "tb-account";
            let result = decodeURIComponent(document.cookie.replace(
                    new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"),
            "$1")) || "";
            return result;
        }

        getAccountLogin() {
            let key = "tb-login";
            let result = decodeURIComponent(document.cookie.replace(
                    new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"),
            "$1")) || "";

            return result;
        }

        delete(path, data, account, login, passwd) {
          let result = this._remoteData(path, data, {account, login, passwd}, "DELETE");
          return result;
        }

        postData(path, data, account, login, passwd) {
          let result = this._remoteData(path, data, {account, login, passwd})
            return result;
        }

        load(path = "", account, login, passwd) {
            let result = this._remoteData(path, null, {account, login, passwd});
            return result;
        }

        _remoteData(path = "", data, auth = {}, protocol) {
            let loadId = utils.uid();
            bus.trigger("loading", {id: loadId});
            var account = auth.account;
            if (account == null) {
                account = this.getAccountName();
            }
            if (account == null) {
                return Promise.reject();
            }

            var init = {
                "credentials": "include", // to send cookies
                headers: {
                    // used to prevent browser login input popup
                    // in conjonction with server side implementation
                    "X-FetchRequest": true,
                    "X-tbHost": location.origin,
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Request-Headers": "X-FetchRequest, Content-Type, Accept, Authorization, Origin"
                }
            };
            if (auth.login && auth.passwd) {
                init.headers.Authorization = "Basic " + btoa(auth.login + ":" + auth.passwd);
            }
            init.method = protocol || "GET";
            if (data) {
                init.method = protocol || "POST";
                init.body = JSON.stringify(data);
            }

            if (path && path.charAt(0) !== "/") {
                path = "/" + path;
            }
            var prefix = localStorage.getItem("timebundleServer") || window.location.pathname;
            if (prefix.slice(-1) !== "/") {
                prefix += "/";
            }
            var p = prefix + "api/v1/";
            if( path || init.method !== "POST") {
              p += account + path;
            }
            let result = fetch(p, init).catch(() => {
                return log.err("Network error", path);
            }).then(response => {
                if(response.ok) {
                    // s'il n'y a pas de json en reponse on retourne null
                    return response.json().catch(() => null);
                } else if (response.status === 401) {
                    route("signin");
                    return log.err("user or passord not exist");
                } else if (response.status === 409) {
                  return log.err("conflict data already exist");
                } else {
                    return log.err(response.status);
                }
            });

            result.then((json) => {
                bus.trigger("loaded", {id: loadId, json});
            }, (error) => {
                bus.trigger("loaded", {id: loadId, error});
            });
            return result;
        }
//**********************functions to discuss with back************************

      createDatabase(data, account, login, password) {
        return this.postData("", data, account, login, password);
      }

      connect(account, login, password) {
        return this.load("", account, login, password);
      }

      loadProjects() {
        return this.load("/projects");
      }

      loadProject(projectUuid) {
        return this.load(`/projects/${encodeURIComponent(projectUuid)}`);
      }

      saveProject(data) {
        return this.postData('/projects', data) ;
      }

      removeProject(projectUuid) {
        return this.delete(`projects/${encodeURIComponent(projectUuid)}`);
      }

      saveTask(parentTaskUuid, data) {
          return this.postData(`tasks/projects/${parentTaskUuid}`, data);
      }

      removeTask(taskUuid) {
        return this.delete(`tasks/${encodeURIComponent(taskUuid)}`);
      }

      loadTask(taskUuid) {
        return this.load(`tasks/${encodeURIComponent(taskUuid)}`);
      }

      loadUsers() {
        return this.load(`users`);
      }

      removeUser(userName) {
        return this.delete(`/users/${encodeURIComponent(userName)}`);
      }

      saveUser(data) {
        return this.postData('/users', data) ;
      }

//---------------------functions used by project And task-----------------------

      loadTaskInfos(taskUuid) {
        return this.load(`tasks/${encodeURIComponent(taskUuid)}/infos`);
      }

      loadTasks(taskUuid, all = false) {
        return this.load(`parents/${encodeURIComponent(taskUuid)}/tasks/all/${all}`);
      }

//---------------------functions to manage contributors-------------------------
      loadContributorTaskContributions(taskUuid, contributorName, validate = "validate") {
        return this.load(`/task/${encodeURIComponent(taskUuid)}/contributors/${encodeURIComponent(contributorName)}/validate/${validate}/contributions`)
      }

      loadContributorInfos(taskUuid, contributorName, validate = "validate") {
        return this.load(`contributors/${encodeURIComponent(contributorName)}/tasks/${encodeURIComponent(taskUuid)}/validate/${validate}/infos`);
      }

      loadTaskContributors(taskUuid) {
        return this.load(`contributors/tasks/${encodeURIComponent(taskUuid)}`);
      }

      isContributor(login, projectUuid) {
        return this.load(`users/${encodeURIComponent(login)}/project/${encodeURIComponent(projectUuid)}`);
      }

      loadProjectContributors(projectUuid) {
        return this.load(`contributors/projects/${encodeURIComponent(projectUuid)}`);
      }

      loadActiveContributor(projectUuid) {
        return this.load(`contributors/projects/${encodeURIComponent(projectUuid)}/active`);
      }
      rmContributor(contributorName, all) {
        return this.delete(`/contributor/${encodeURIComponent(contributorName)}/contributions/${all}`);
      }
      saveContributor(projectUuid, data) {
        return this.postData(`contributors/projects/${encodeURIComponent(projectUuid)}`, data);
      }
      loadContributor(projectUuid, contributorName) {
        return this.load(`contributors/${encodeURIComponent(contributorName)}/projects/${encodeURIComponent(projectUuid)}`);
      }

//------------------------functions to manage contributions---------------------

      removeContributions(uuids) {
        return this.delete('/contributions', uuids );
      }

      updateContributionStatus(status, listUuid) {
        return this.postData(`/contributions/status/${encodeURIComponent(status)}`, listUuid);
      }

      loadContributionsToValidate(projectUuid) {
        return this.load(`contributions/projects/${encodeURIComponent(projectUuid)}/tovalidate`);
      }

      saveContribution(contribution) {
        return this.postData(`contributions`, contribution);
      }

      loadProjectContributions(projectUuid, status = 'validate') {
        return this.load(`contributions/projects/${encodeURIComponent(projectUuid)}/status/${status}`);
      }

      loadTaskContributions(taskUuid, validate = 'validate') {
        return this.load(`contributions/tasks/${encodeURIComponent(taskUuid)}/validate/${validate}`)
      }

      loadContribution(contributionUuid) {
        return this.load(`contributions/${encodeURIComponent(contributionUuid)}`);
      }

      validContributions(className,uuid, contributorName) {
        return this.load(`/contributions/contributor/${encodeURIComponent(contributorName)}/${encodeURIComponent(className)}/${encodeURIComponent(uuid)}/valid`);
      }

//---------------------function to manage alerts--------------------------------
      loadAlerts(projectUuid, all) {
        return this.load(`/alerts/task/${encodeURIComponent(projectUuid)}/template/${all}`);
      }

      loadAlert(alertUuid) {
        return this.load(`alerts/${encodeURIComponent(alertUuid)}/task`);
      }

      activateAlert(alertUuid) {
        return this.load(`alerts/${encodeURIComponent(alertUuid)}/activate`);
      }

      removeAlert(alertUuid) {
        return this.delete(`/alerts/${encodeURIComponent(alertUuid)}`);
      }

      saveAlert(taskUuid, data) {
        return this.postData(`/alerts/task/${encodeURIComponent(taskUuid)}`, data);
      }

//------------------------------------------------------------------------------
      validEmail(email, user, account, login) {
        return this.load(`/users/${encodeURIComponent(user)}/email/${encodeURIComponent(email)}`, account, login, login);
      }
//------------------------------users-------------------------------------------

      isAdmin(projectUuid) {
        return this.load(`users/project/${encodeURIComponent(projectUuid)}/admin`);
      }

      loadAdmin() {
        return this.load("users/admin");
      }

      getUsers() {
        return this.load(`/users`);
      }

      loadUrlContribute(accountLogin, projectUuid) {
        return this.load(`users/${encodeURIComponent(accountLogin)}/project/${encodeURIComponent(projectUuid)}`)
      }

      beginContributor(userName, projectUuid, email) {
        return this.load(`/users/${encodeURIComponent(userName)}/project/${encodeURIComponent(projectUuid)}/contributor/${encodeURIComponent(email)}`);
      }

      changePassword(password, oldPassword) {
        return this.postData('users/change', password , this.getAccountName(), this.getAccountLogin(), oldPassword );
      }

      reinitPassword(email, userName) {
        return this.load(`/users/${encodeURIComponent(userName)}/reinit/${encodeURIComponent(email)}`)
      }

//******************************************************************************
}



    let storage = new TBStorage();
    return storage;

});
