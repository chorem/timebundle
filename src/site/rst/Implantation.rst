Choix technique
===============

- Persistence via OrientDB
- API Rest
- Application WEB single page qui utilise l'API Rest
- framework riotJS


Persistence
===========

Chaque compte à sa propre base de données. Cela permet de répartir la charge
plus facilement, car il n'y a pas de base qui centralise toutes les informations.

La base est retrouvée grâce au nom du compte, celui-ci une fois créé ne peut
donc pas changer. Le but est de pouvoir faire un sous-domaine par compte et
ainsi profiter du DNS pour faire directement la répartition de charge.

Chaque compte a donc une base de données OrientDB local sur le système de fichier.

Sécurité
========

Authentification
----------------

Sauf pour la création d'un nouveau compte, le nom du compte doit toujours être
contenu dans url. Il permet de sélectionner la base de données de travail.

Si la demande est contient l'identifiant d'un contributeur, cet identifiant
est utililsé pour authentifier l'utilisateur.

Si la requête contient un entête d'authentification (Basic), il est utilisé
pour authentifier l'utilisateur.

Si la requête contient un cookie **tb-auth**, il est utilisé pour authentifier
l'utilisateur. Ce cookie est envoyé par les requêtes Rest. Il faut donc au moins
la 1ère fois utiliser une requête avec l'entête d'authentification. Ce cookie
est le contenu de l'authentification basic mais crypté par l'application.

Si aucune de ces authentifications ne fonctionne une authentifications
anonyme est créée, l'utilisateur peut donc accèder au compte et projets
uniquement publics.

Une fois l'utilisateur authentifier toutes les actions sur la base base de
données sont faites sous cet identifiant.

Autorisation
------------



Les autorisations sont gérés directement par la base de données.

Le créateur d'un compte est le super utilisateur général pour ce compte. Il peut
tout faire.

D'autres supers utilisateurs peuvent être créés.

Les chefs de projets peuvent gérer un ou plusieurs projets. Ils peuvent:
- modifier le projet
- créer/modifier/supprimer des tâches
- créer/activer/désactiver des contributeurs
- valider/blacklister des contributions
- creer/declencher des alertes emails

Les contributeurs peuvent
- envoyer des contributions sur les tâches auquels ils ont droit
- supprimer des contributions précédement envoyées

Un utilsateur peut être créé et n'avoir aucun des rôles d'écrit ci-dessus.
Cet utilisateur permet simplement de faire le lien entre les différents projets
auquels il contribue. Ce lien est fait sur les différentes adresse mail de
l'utilisateur (et qui ont été vérifié) et les emails des contributeurs.

Un utilisateur peut alors retrouver l'ensemble de projet sur lequel il a le
droit de contribuer pour un compte données plus facilement.
